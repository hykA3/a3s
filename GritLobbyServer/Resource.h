//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GritLobbyServer.rc
//

#define IDS_APP_TITLE			103

#define IDR_MAINFRAME			128
#define IDD_GRITLOBBYSERVER_DIALOG	102
#define IDD_ABOUTBOX			103
#define IDM_ABOUT				104
#define IDM_EXIT				105
#define IDI_GRITLOBBYSERVER			107
#define IDI_SMALL				108
#define IDC_GRITLOBBYSERVER			109
#define IDC_MYICON				2
#ifndef IDC_STATIC
#define IDC_STATIC				-1
#endif

#define BTN_START				2001
#define BTN_STOP				2002
#define EDIT_LOG				3001
// 다음은 새 개체에 사용할 기본값입니다.
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS

#define _APS_NO_MFC					130
#define _APS_NEXT_RESOURCE_VALUE	129
#define _APS_NEXT_COMMAND_VALUE		32771
#define _APS_NEXT_CONTROL_VALUE		1000
#define _APS_NEXT_SYMED_VALUE		110
#endif
#endif
