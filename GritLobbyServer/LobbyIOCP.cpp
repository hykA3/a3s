#include "stdafx.h"
#include "LobbyIOCP.h"
#include "Packet.h"

#include "../GritLibrary/NetworkRequestIntoJson.h"
#include "../GritLibrary/Queue.h"
#include "../GritLibrary/Connection.h"

CLobbyIOCP::CLobbyIOCP()
{
	if (CIOCPServer::_instance == NULL)
	{
		CIOCPServer::_instance = this;
	}

	_serverName = "LobbyServer";

	Initialize();

	_loginServerSocket = INVALID_SOCKET;
	_loginServerConnection = NULL;
}


CLobbyIOCP::~CLobbyIOCP()
{
	for (int i = 0; i < 100; i++)
	{
		_funcProcess[i].funcProcessPacket = NULL;
	}

	SAFE_DELETE(_loginServerConnection);
	closesocket(_loginServerSocket);
}

bool CLobbyIOCP::StartServer(std::string serverName, std::string ip, int port)
{
	char outStr[1024] = { 0 };

	_serverName = serverName;

	InitConfig	initConfig;

	initConfig._processPacketCount = 1000;
	
	initConfig._sendBufferCount = 8;
	initConfig._recvBufferCount = 2;

	initConfig._sendBufferSize = 5120; //1kb
	initConfig._recvBufferSize = 5120;

	initConfig._serverPort = 8080;
	initConfig._maxConnectionCount = 100;

	initConfig._serverWorkerThreadCount = 4;
	initConfig._serverProcessThreadCount = 4;

	if (aria::CIOCPServer::StartServer(initConfig) == false)
	{
		return false;
	}

	initConfig._serverPort = port;
	if (ConnectLoginServer(ip, port) == false)

	{
		return false;
	}

	return true;
}

bool CLobbyIOCP::OnConnect(aria::CConnection* connection)
{
	if (connection == _loginServerConnection)
	{
		aria::CNetworkRequestIntoJson json;
		json.AddParam("serverName", _serverName.c_str());
		json.SetRequestType(ConnectLoginServerType);

		std::string jsonString = json.GetJsonString();

		int size = sizeof(PacketBase) + jsonString.length();

		char* buffer = connection->PrepareSendPacket(size + 1);

		if (buffer == NULL)
		{
			return false;
		}

		buffer[size] = '\0';

		PacketBase* base = (PacketBase*)buffer;

		base->_packetLength = size + 1;
		base->_packetType = ConnectLoginServerType;


		CopyMemory(buffer + sizeof(PacketBase), jsonString.c_str(), jsonString.length());

		connection->SendPost(size + 1);

		GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, "LobbyIOCPServer::OnConnect(), Connection ��Ŷ ����");
	}
	return true;
}

bool CLobbyIOCP::OnAccept(aria::CConnection* connection)
{
	SOCKET socket = connection->GetSocket();

	std::string temp = "LobbyIOCPServer::OnAccept(), socketId : " + IntToString(socket);
	GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, temp.c_str());

	return true;
}

bool CLobbyIOCP::OnRecv(aria::CConnection* connection, DWORD size, char* recvedMsg)
{
	return true;
}

bool CLobbyIOCP::OnRecvImmediately(aria::CConnection* connection, DWORD size, char* recvedMsg)
{
	unsigned short type;
	CopyMemory(&type, recvedMsg + 4, 2);

	if (type >= 0 && type < 100 && NULL != _funcProcess[type].funcProcessPacket)
	{
		aria::CNetworkResponseIntoJson json(connection);
		json.SetRequestType(type);

		aria::CJsonObject* resultObject = new aria::CJsonObject();
		json.AddParam("result", resultObject, false);

		rapidjson::Document document;

		if (document.Parse<0>(recvedMsg + sizeof(PacketBase)).HasParseError())
		{
			return true;
		}

		_funcProcess[type].funcProcessPacket(&json, &document);
		return true;
	}

	return false;
}

void CLobbyIOCP::OnClose(aria::CConnection* connection)
{
	SOCKET socket = connection->GetSocket();

	std::string temp = "LobbyIOCPServer::OnClose(), socketId : " + IntToString(socket);
	GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, temp.c_str());
}

bool CLobbyIOCP::OnSystemMsg(aria::CConnection* connection, DWORD msgType, LPARAM lParam)
{
	return true;
}

bool CLobbyIOCP::ConnectLoginServer(std::string ip, int port)
{
	SAFE_DELETE(_loginServerConnection);

	_loginServerConnection = new aria::CConnection();

	if (_loginServerConnection)
	{
		InitConfig initConfig;

		initConfig._sendBufferCount = 2;
		initConfig._recvBufferCount = 2;

		initConfig._sendBufferSize = 10485760;
		initConfig._recvBufferSize = 10485760;

		initConfig._serverPort = 9091;
		initConfig._socketListener = GetListenSocket();

		if (_loginServerConnection->CreateConnection(initConfig, false) == false)
		{
			return false;
		}

		if (_loginServerConnection->BindConnectSock(ip.c_str(), port, _workerIOCPHandle) == false)
		{
			return false;
		}
	}

	return true;
}

bool CLobbyIOCP::Initialize()
{
	SettingProcessFunc();
	return true;
}

void CLobbyIOCP::Destroy()
{

}

void CLobbyIOCP::SettingProcessFunc()
{

}
