/*
	file : CLobbyIOCP.h
	creator : kwon sin hyeok ( aria2060@gmail.com )
	modify : 2017.04.03
	note : GritLobbyServer 의 메인 루프
*/

#pragma once

#include "../GritLibrary/IOCPServer.h"
#include "../GritLibrary/Rapidjson/document.h"

namespace aria
{
	class CConnection;
	class CNetworkResponseIntoJson;
}

class CLobbyIOCP : public aria::CIOCPServer
{
public:
	CLobbyIOCP();
	virtual ~CLobbyIOCP();

	virtual bool StartServer(std::string serverName, std::string ip, int port);
	virtual bool OnConnect(aria::CConnection* connection);
	virtual bool OnAccept(aria::CConnection* connection);
	virtual bool OnRecv(aria::CConnection* connection, DWORD size, char* recvedMsg);
	virtual bool OnRecvImmediately(aria::CConnection* connection, DWORD size, char* recvedMsg);
	virtual void OnClose(aria::CConnection* connection);
	virtual bool OnSystemMsg(aria::CConnection* connection, DWORD msgType, LPARAM lParam);

public:
	bool		Initialize();
	void		Destroy();
	void		SettingProcessFunc();

public:
	union FuncProcess
	{
		void(*funcProcessPacket)(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data);

		FuncProcess()
		{
			funcProcessPacket = NULL;
		}
	};

protected:
	bool		ConnectLoginServer(std::string ip, int port);

	std::string			_serverName;

	//loginserver 전용 접속 소켓
	SOCKET				_loginServerSocket;
	aria::CConnection*	_loginServerConnection;

	FuncProcess			_funcProcess[100];
	std::string			_logFileName;
};

