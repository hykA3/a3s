/*
	file : Packet.h
	creator : kwon sin hyeok ( aria2060@gmail.com )
	modify : 2017.04.03
	note : packet type define
*/
#pragma pack(push,1)

#define  MAX_CHATTING_MESSAGE_LENGTH	100

enum PacketType
{
	LoginPacketType = 11,
	LoginSuccessType = 12,
	LoginFailType = 13,

	LogoutPacketType = 14,

	CreateRoomPacketType = 31,

	ConnectLoginServerType = 41,

	UploadDataTablePacketType = 51,

	ChattingPacketType = 82,

	ChacterState = 21,
};

struct PacketBase
{
	unsigned int _packetLength;
	unsigned short _packetType;
};

