#pragma once

enum OperationType
{
	//Worker IOCP operation
	OP_SEND,
	OP_RECV,
	OP_ACCEPT,

	OP_CONNECT,
	//Process IOCP operation
	OP_CLOSE,
	OP_RECV_PACKET, //순서성 패킷 처리
	OP_SYSTEM
};

typedef struct _OVERLAPPED_EX
{
	WSAOVERLAPPED _overlapped;
	WSABUF _wsaBuf;
	int	_totalByte;
	DWORD _remain;
	char* _socketMsg;
	OperationType _operationType;
	void* _connection;

	_OVERLAPPED_EX(void* connection)
	{
		ZeroMemory(&_wsaBuf, sizeof(WSABUF));
		_totalByte = 0;
		_remain = 0;
		_socketMsg = NULL;
		_connection = connection;
	}
}OVERLAPPED_EX, *LPOVERLAPPED_EX;