#pragma once

#include "define.h"

class CUDP
{
public:
	CUDP();
	~CUDP();

	bool	ServerStart();
	void	ServerStop();

	bool	Initialize();

	bool	CreateSocket();
	bool	CreateWorkerIOCP();
	bool	CreateWorkerThread();
	bool	CreateBindSocket();

	void	WorkerThread();

	void	RecvPost();
	void	SendPost();

protected:
	SOCKET		_socket;
	SOCKADDR_IN	_socketAddr;

	SOCKET		_clientSocket;
	SOCKADDR_IN	_clientSockAddr;
	
	HANDLE	_workerIOCPHandle;
	
	HANDLE	_workerThreadHandle;
	bool	_workerThreadRun;
	int		_workerThreadCount;
};

