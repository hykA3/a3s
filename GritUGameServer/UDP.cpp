#include "stdafx.h"
#include "UDP.h"
#include <process.h>

unsigned int WINAPI CallWorkerThread(void* object)
{
	CUDP* udpServer = (CUDP*)object;
	udpServer->WorkerThread();
	return 1;
}

CUDP::CUDP()
{
}


CUDP::~CUDP()
{
	WSACleanup();
}

bool CUDP::ServerStart()
{
	if (!Initialize())
	{
		return false;
	}

	if (!CreateSocket())
	{
		return false;
	}

	if (!CreateWorkerIOCP())
	{
		return false;
	}
	
	if (!CreateWorkerThread())
	{
		return false;
	}

	if (!CreateBindSocket())
	{
		return false;
	}
	return true;
}

void CUDP::ServerStop()
{
	if (_workerIOCPHandle)
	{
		_workerThreadRun = false;
		PostQueuedCompletionStatus(_workerIOCPHandle, 0, 0, NULL);
		Sleep(1000);

		CloseHandle(_workerIOCPHandle);
		_workerIOCPHandle = NULL;
	}
	if (_workerThreadHandle != INVALID_HANDLE_VALUE)
	{
		CloseHandle(_workerThreadHandle);
		_workerThreadHandle = INVALID_HANDLE_VALUE;
	}

	if (_socket != INVALID_SOCKET)
	{
		closesocket(_socket);
		_socket = INVALID_SOCKET;
	}
}

bool CUDP::Initialize()
{
	_workerThreadHandle = INVALID_HANDLE_VALUE;
	_workerIOCPHandle = INVALID_HANDLE_VALUE;

	WSADATA	wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
	{
		return false;
	}

	return true;
}

bool CUDP::CreateSocket()
{	
	_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (_socket == INVALID_SOCKET)
	{
		return false;
	}

	return true;
}

bool CUDP::CreateWorkerIOCP()
{
	_workerIOCPHandle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
	if (_workerIOCPHandle == NULL)
	{
		return false;
	}

	return true;
}

bool CUDP::CreateWorkerThread()
{
	HANDLE	thread = INVALID_HANDLE_VALUE;
	UINT	threadId = -1;

	thread = (HANDLE)_beginthreadex(NULL, 0, &CallWorkerThread, this, 0, &threadId);
	if (thread == NULL)
	{
		return false;
	}

	_workerThreadHandle = thread;
	ResumeThread(thread);

	_workerThreadRun = true;

	return true;
}

bool CUDP::CreateBindSocket()
{
	ZeroMemory(&_socketAddr, sizeof(_socketAddr));
	_socketAddr.sin_family = AF_INET;
	_socketAddr.sin_port = htons(9092);
	_socketAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);

	int retval = bind(_socket, (SOCKADDR*)&_socketAddr, sizeof(_socketAddr));
	if (retval == SOCKET_ERROR)
	{
		return false;
	}

	HANDLE iocpHandle = CreateIoCompletionPort((HANDLE)_socket, 
		_workerIOCPHandle, (DWORD)_socket, 0);

	if (iocpHandle == NULL || _workerIOCPHandle != iocpHandle)
	{
		return false;
	}

	return true;
}

void CUDP::WorkerThread()
{
	int				retval;
	bool			bRet;
	DWORD			dwBytesTransfer, CompletionKey;
	LPOVERLAPPED	lpOverlapp;	

	while (_workerThreadRun)
	{
		bool success = GetQueuedCompletionStatus(_workerIOCPHandle, 
			&dwBytesTransfer, 
			&CompletionKey, 
			&lpOverlapp, INFINITE);
		
		if (success == false)
		{
			continue;
		}
// 		if (lpOverlapp == NULL)
// 		{
// 			continue;
// 		}

		LPOVERLAPPED_EX overlappedEX = (LPOVERLAPPED_EX)lpOverlapp;
		if (overlappedEX == NULL)
		{
			continue;
		}

		switch (overlappedEX->_operationType)
		{
		case OP_ACCEPT:
			break;

		case OP_RECV:
			break;

		case OP_SEND:
			break;

		default:
			break;
		}

		Sleep(10);
	}
}

void CUDP::RecvPost()
{

}

void CUDP::SendPost()
{

}

