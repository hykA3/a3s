//
// file : SqlConnector.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.07
//

#pragma once

#include <map>
#include <vector>
#include <mysql.h>

namespace aria
{
	class CSqlConnector
	{
	public:
		CSqlConnector(void);
		virtual ~CSqlConnector(void);

		bool		Initialize();
		void		Destroy();

		//���� �Լ�
		int			SendQuery(const char* query, std::vector < std::map<std::string, std::string>>& result, bool autoRelease = true, int stack = 0);

		inline bool	IsPending() { return _isPending; }
		inline void	PreReservation() { _isPending = false; }
		inline void	ReleaseReservation() { _isPending = true; }

	protected:
		MYSQL*		_sqlConnect;
		MYSQL*		_sqlRealConnect;

		bool		_isPending;
	};
}

#define SAFE_MYSQL_CLOSE( mysql )\
	{\
		if(mysql)\
		{\
			mysql_close(mysql);\
			mysql = NULL;\
		}\
	}



