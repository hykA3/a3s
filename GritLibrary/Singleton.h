#pragma once
//
// file : Singleton.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#pragma once

namespace aria
{
	template <typename T>
	class CSingleton
	{
	protected:
		CSingleton()
		{
		}
		~CSingleton()
		{
		}

	public:
		static T* GetInstance(bool create = true)
		{
			if (_instance == nullptr && create)
			{
				_instance = new T;
			}

			return _instance;
		}

		static void DestroyInstance()
		{
			if (_instance)
			{
				delete _instance;
			}

			_instance = nullptr;
		}

		virtual bool Initialize() = 0;
		virtual void Destroy() = 0;

	protected:
		static T		*_instance;
	};

	template<typename T>
	T* CSingleton<T>::_instance = nullptr;
}
