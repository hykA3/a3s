//
// file : TextSupport.cpp
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#include <iostream>
#include "Library.h"
#include <time.h>
#include "TextSupport.h"

std::string IntToString(int number)
{
	char buf[100];
	sprintf_s(buf, "%d", number);
	return std::string(buf);
}

int StringToInt(std::string str)
{
	return atoi(str.c_str());
}

long long StringToLong(std::string str)
{
	return _atoi64(str.c_str());
}

float StringToFloat(std::string str)
{
	return (float)atof(str.c_str());
}

std::string FloatToString(float number)
{
	char buf[100];
	sprintf_s(buf, "%f", number);
	return std::string(buf);
}


void TenLog(const char* str, ...)
{
	char* szBuf = new char[1024];

	va_list ap;
	va_start(ap, str);
	vsnprintf(szBuf, 1024, str, ap);
	szBuf[1024 - 1] = 0;
	va_end(ap);

	OutputDebugStringA(szBuf);
	OutputDebugStringA("\n");

	delete[] szBuf;
}

unsigned char ParseHexChar(char ch)
{
	if (ch >= 'a')
	{
		return ch - 'a' + 10;
	}
	else if (ch >= 'A')
	{
		return ch - 'A' + 10;
	}

	return ch - '0';
}

void ParseHexString(const char* string, unsigned char* value, int size)
{
	for (int i = 0; i < size; ++i)
	{
		value[i] = 0;

		if (*string)
		{
			value[i] = ParseHexChar(*string) << 4;
			++string;

			//dma ... 
			if (*string)
			{
				value[i] = value[i] | ParseHexChar(*string);
				++string;
			}
		}
	}
}

std::wstring CharToWsting(const char *cstr)
{
	std::string strSource = cstr;
	std::wstring wstrResult;

	if (strSource.empty())
		return wstrResult;

	int size = static_cast<int>(strSource.size());
	int wsize = MultiByteToWideChar(CP_ACP, 0, strSource.c_str(), size, 0, 0);

	WCHAR* pWBuffer = new WCHAR[wsize + 1];
	memset(pWBuffer, 0, sizeof(WCHAR)*(wsize + 1));

	MultiByteToWideChar(CP_ACP, 0, strSource.c_str(), size, pWBuffer, wsize);

	wstrResult.assign(pWBuffer);
	delete[] pWBuffer;

	return wstrResult;
}

int gettimeofday(struct timeval* val)
{
	if (val)
	{
		LARGE_INTEGER time, freq;

		QueryPerformanceFrequency(&freq);
		QueryPerformanceCounter(&time);

		val->tv_sec = (long)(time.QuadPart / freq.QuadPart);
		val->tv_usec = (long)(time.QuadPart * 1000000 / freq.QuadPart - val->tv_sec * 1000000);
	}
	return 0;
}

long long GetUnixTimeStamp()
{
	time_t unixtime;

	time(&unixtime);
	unixtime *= 1000;

	timeval now;
	gettimeofday(&now);

	unixtime += now.tv_usec / 1000;

	return unixtime;
}
