/*
	file : RedisConnectorManager.h
	creator : kwon sin hyeok ( aria2060@gmail.com )
	last modify : 17.02.21
*/

#pragma once

#include "Singleton.h"
#include "RedisConnector.h"

typedef std::map<std::string, std::string> CRedisRowResult;
typedef std::vector<CRedisRowResult> CRedisResult;


#define REDISMANAGER	CRedisConnectorManager::GetInstance()

namespace aria
{
	class CRedisConnectorManager : public CSingleton<CRedisConnectorManager>
	{
	public:
		CRedisConnectorManager();
		~CRedisConnectorManager();

		bool					Initialize();
		void					Destroy();

		int						NewRequest();

	protected:

		CRedisConnector*		_redis;


	};
}


