//
// file : TextSupport.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#pragma once

#include <string>

std::string IntToString(int number);
std::string FloatToString(float number);
int StringToInt(std::string str);
long long StringToLong(std::string str);
float StringToFloat(std::string str);

void TenLog(const char* str, ...);

unsigned char ParseHexChar(char ch);
void ParseHexString(const char* string, unsigned char* value, int size);
std::wstring CharToWsting(const char *str);

long long GetUnixTimeStamp();

