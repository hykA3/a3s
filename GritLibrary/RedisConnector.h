/*
	file : RedisConnector.h
	creator : kwon sin hyeok ( aria2060@gmail.com )
	last modify : 17.02.23
*/

#include <map>
#include <vector>
#include "hiredis/hiredis.h"

#pragma once
namespace aria
{
	class CRedisConnector
	{
	public:
		CRedisConnector();
		virtual ~CRedisConnector();

		bool			Initialize();
		void			Destroy();

		int				SendQuery(const char* query, std::vector < std::map<std::string, std::string>>& result, bool autoRelease = true, int stack = 0);

		inline bool		IsPending() { return _isPending; }
		inline void		PreReservation() { _isPending = false; }
		inline void		ReleaseReservation() { _isPending = true; }

	protected:
		redisContext*	_redisContext;
		redisReply*		_redisReply;

		bool			_isPending;
	};

}

