//
// file : SqlConnectorPool.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.07
//

#pragma once

#include "Singleton.h"
#include "Delegate.h"
#include "CriticalSection.h"
#include "Queue.h"

//두번째 인자는 요청 결과에 대한 성공 또는 실패 값 
//세번째 인자는 쿼리 수행 결과로 나온 필드이름과 값
typedef std::map<std::string, std::string> CSqlRowResult;
typedef aria::delegate2<bool, int, CSqlRowResult> CSqlRequestCallback;
typedef std::vector<CSqlRowResult> CSqlResult;

namespace aria
{
	class CSqlConnector;

	class CSqlRequest
	{
	public:
		CSqlRequest() : _callback(NULL) {}
		~CSqlRequest()
		{
			_callback = NULL;
		}

		std::string	_query;
		const CSqlRequestCallback* _callback;
	};


#define GLOBAL_SQL_CONNECTOR	aria::CSqlConnectorPool::GetInstance()
	class CSqlConnectorPool : public CSingleton<CSqlConnectorPool>
	{
	public:
		CSqlConnectorPool(void);
		virtual ~CSqlConnectorPool(void);

		bool						Initialize();
		void						Destroy();
		void						Update();

		int							NetworkRequest(const char* query, CSqlResult& result);

		CSqlConnector*				StartTransaction();
		void						EndTransaction(CSqlConnector* connector, bool isRollback);

		//비동기
		bool						AsyncNetworkRequest(const char* query, const CSqlRequestCallback* callback);

	public:
		CSqlConnector*				_sqlConnector;
		int							_connectorMaxCount;

		CSafeDeque<CSqlRequest*>	_requestQueue;
		CCriticalSection			_cs;

	};
};


