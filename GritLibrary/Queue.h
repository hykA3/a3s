//
// file : Queue.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#pragma once
#include "CriticalSection.h"
#include <map>
#include <vector>
#include <deque>

namespace aria
{
	//thread와 thread간 데이터 입출력을 안전하게
	template <class T>
	class  CSafeDeque
	{
	public:
		CSafeDeque()
		{
		}

		virtual ~CSafeDeque()
		{
		}

		unsigned int Size()
		{
			ScopedCriticalSectionLock lock(_cs);
			return _deque.size();
		}

		T Pop()
		{
			ScopedCriticalSectionLock lock(_cs);

			if (_deque.empty() == false)
			{
				T data = _deque.front();
				_deque.pop_front();

				return data;
			}

			return NULL;
		}

		T Head()
		{
			ScopedCriticalSectionLock lock(_cs);

			if (_deque.empty() == false)
			{
				T data = _deque.front();
				return data;
			}

			return NULL;
		}

		//deque 뒤에 넣는다.
		void Push(T data)
		{
			ScopedCriticalSectionLock lock(_cs);

			_deque.push_back(data);
		}

		//deque 앞에 넣는다.
		void PushFront(T data)
		{
			ScopedCriticalSectionLock lock(_cs);
			_deque.push_front(data);
		}

		void Erase(typename std::deque<T>::iterator itr)
		{
			ScopedCriticalSectionLock lock(_cs);

			_deque.erase(itr);
		}

		void Erase(T data)
		{
			ScopedCriticalSectionLock lock(_cs);

			for (std::deque<T>::iterator itr = _queue.begin(); itr != _queue.end(); itr++)
			{
				if (data == (*itr))
				{
					_queue.erase(itr);
					return;
				}
			}
		}

		bool Empty()
		{
			ScopedCriticalSectionLock lock(_cs);
			return _deque.empty();
		}
		
		std::deque<T>& GetDeque()
		{
			return _deque;
		}

		//배열의 인자로 참조 하는것은 스레드와 스레드간의 세이프 하지 않음.
		//Lock()을 호출 한 뒤에 쓰는 것을 매우 권장함..
		//그리고 반드시 UnLock()을 호출 할 것. 
		//그렇지 않으면 데드락에 빠짐
		T& operator[](size_t index)
		{
			return _deque[index];
		}

		void Lock()
		{
			_cs.Lock();
		}

		void Unlock()
		{
			_cs.Unlock();
		}

	protected:
		CCriticalSection _cs;
		std::deque<T> _deque;
	};

	template <typename T1, typename T2>
	class  SafeMap
	{
	public:
		SafeMap()
		{
		}

		virtual ~SafeMap()
		{
		}

		unsigned int Size()
		{
			ScopedCriticalSectionLock lock(_cs);
			return _map.size();
		}

		void Erase(typename std::map<T1, T2>::iterator itr)
		{
			ScopedCriticalSectionLock lock(_cs);

			_map.erase(itr);
		}

		void Erase(T1 data)
		{
			ScopedCriticalSectionLock lock(_cs);

			auto itr = _map.find(data);

			if (itr != _map.end())
			{
				_map.erase(itr);
			}
		}

		typename std::map<T1, T2>::iterator Find(T1 first)
		{
			ScopedCriticalSectionLock lock(_cs);

			return _map.find(first);
		}

		void Insert(T1 first, T2 second)
		{
			ScopedCriticalSectionLock lock(_cs);
			_map.insert(std::make_pair(first, second));
		}

		typename std::map<T1, T2>::iterator End()
		{
			ScopedCriticalSectionLock lock(_cs);
			return _map.end();
		}
		bool Empty()
		{
			ScopedCriticalSectionLock lock(_cs);
			return _map.empty();
		}

		std::map<T1, T2>& GetMap()
		{
			return _map;
		}

		void Lock()
		{
			_cs.Lock();
		}

		void Unlock()
		{
			_cs.Unlock();
		}
	protected:
		CCriticalSection _cs;
		std::map<typename T1, typename T2> _map;
	};
}