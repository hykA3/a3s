//
// file : Library.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#pragma once

#pragma warning( disable : 4005)

#include <iostream>

#include <winsock2.h>
#include <MSWSock.h>
#include <process.h>

#include <windows.h>

#include <map>
#include <vector>
#include <deque>

#include "define.h"
#include "TextSupport.h"
#include "Queue.h"
#include "Log.h"
