#include <iostream>
#include "Log.h"
#include <time.h>

namespace aria {
	CLog::CLog()
	{

	}

	CLog::~CLog()
	{
		Destroy();
	}

	bool CLog::Initialize()
	{
		return true;
	}

	void CLog::Destroy()
	{
		_logQueue.Lock();

		if (_logQueue.Empty() == false)
		{
			unsigned int size = _logQueue.Size();
			for (unsigned int i = 0; i < size; ++i)
			{
				delete _logQueue[i];
			}
			_logQueue.GetDeque().clear();
		}

		_logQueue.Unlock();
	}

	void CLog::Normal(LogInfoTypeNormal logType, const char* msg, ...)
	{
#ifndef OUTPUT_LOG_NORMAL
		return;
#endif
		char* szBuf = new char[1024];

		va_list ap;
		va_start(ap, msg);
		vsnprintf(szBuf, 1024, msg, ap);
		szBuf[1024 - 1] = 0;
		va_end(ap);

		LogInfo* logInfo = new LogInfo();
		time(&logInfo->_time);
		logInfo->_logType = logType;
		logInfo->_message = szBuf;

		if (logInfo)
		{
			InsertLog(logInfo);
		}

		OutputDebugStringA((std::string(szBuf) + "\n").c_str());
		delete[] szBuf;
	}

	void CLog::Error(LogInfoTypeError logType, std::string msg)
	{
		//에러는 무조건 로그를 남긴다.

		LogInfo* logInfo = new LogInfo();
		time(&logInfo->_time);
		logInfo->_logType = logType;
		logInfo->_message = msg;
		if (logInfo)
		{
			InsertLog(logInfo);
		}
		OutputDebugStringA(std::string(msg + "\n").c_str());
	}

	void CLog::InsertLog(LogInfo* logInfo)
	{
		_logQueue.Push(logInfo);
	}
};