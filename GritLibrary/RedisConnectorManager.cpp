/*
	file : RedisConnectorManager.h
	creator : kwon sin hyeok ( aria2060@gmail.com )
	last modify : 17.02.21
*/

#include "RedisConnectorManager.h"
#include "Log.h"

namespace aria
{
	CRedisConnectorManager::CRedisConnectorManager()
	{
		_redis = NULL;
	}


	CRedisConnectorManager::~CRedisConnectorManager()
	{
	}

	bool CRedisConnectorManager::Initialize()
	{
		_redis = new CRedisConnector();

		if (_redis == NULL)
		{
			return false;
		}

		if (_redis->Initialize() == false)
		{
			Destroy();
			return false;
		}

		return true;
	}

	void CRedisConnectorManager::Destroy()
	{
		if (_redis != NULL)
		{
			delete[] _redis;
		}
	}

}
