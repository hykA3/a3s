//
// file : CNetworkRequestIntoJson.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.14
//

#include <iostream>
#include "NetworkRequestIntoJson.h"
#include "TextSupport.h"
#include "Connection.h"

namespace aria
{
	CNetworkRequestIntoJson::CNetworkRequestIntoJson(void)
	{
	}

	CNetworkRequestIntoJson::~CNetworkRequestIntoJson(void)
	{
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, const char* value)
	{
		return AddParam(key, NULL, value);
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, int value)
	{
		return AddParam(key, NULL, value);
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, float value)
	{
		return AddParam(key, NULL, value);
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, long long value)
	{
		return AddParam(key, NULL, value);
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, bool value)
	{
		return AddParam(key, (char*)NULL, value);
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, CJsonObject* object, bool isArray)
	{
		return AddParam(key, NULL, object, isArray);
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, const char* parentPath, const char* value)
	{
		if (key == NULL || value == NULL)
			return false;

		CJsonObject* data = GetJsonObject(&_data, parentPath);

		if (data == NULL)
			return false;

		data->AddParam(key, value);

		return true;
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, const char* parentPath, int value)
	{
		if (key == NULL)
			return false;

		CJsonObject* data = GetJsonObject(&_data, parentPath);

		if (data == NULL)
			return false;

		data->AddParam(key, value);

		return true;
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, const char* parentPath, float value)
	{
		if (key == NULL)
			return false;

		CJsonObject* data = GetJsonObject(&_data, parentPath);

		if (data == NULL)
			return false;

		data->AddParam(key, value);

		return true;
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, const char* parentPath, bool value)
	{
		if (key == NULL)
			return false;

		CJsonObject* data = GetJsonObject(&_data, parentPath);

		if (data == NULL)
			return false;

		data->AddParam(key, value);

		return true;
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, const char* parentPath, CJsonObject* value, bool isArray)
	{
		if (key == NULL || value == NULL)
			return false;

		CJsonObject* data = GetJsonObject(&_data, parentPath);

		if (data == NULL)
			return false;

		data->AddParam(key, value, isArray);

		return true;
	}

	bool CNetworkRequestIntoJson::AddParam(const char* key, const char* parentPath, long long value)
	{
		if (key == NULL || value == NULL)
			return false;

		CJsonObject* data = GetJsonObject(&_data, parentPath);

		if (data == NULL)
			return false;

		data->AddParam(key, value);

		return true;
	}

	CJsonObject* CNetworkRequestIntoJson::GetJsonObject(CJsonObject* CJsonObject, const char* key)
	{
		if (CJsonObject == NULL)
			CJsonObject = &_data;

		if (key == NULL)
			return &_data;

		std::string path = key;

		if (std::size_t point = path.find("/") == std::string::npos)
		{
			auto itr = CJsonObject->_objectData.find(path);

			if (itr != CJsonObject->_objectData.end())
				return itr->second;
			else
			{
				aria::CJsonObject* newObject = new aria::CJsonObject();
				CJsonObject->_objectData.insert(std::make_pair(path, newObject));
				return newObject;
			}
		}
		else
		{
			auto itr = CJsonObject->_objectData.find(path.substr(0, point).c_str());

			if (itr != CJsonObject->_objectData.end())
				return GetJsonObject(itr->second, path.substr(point + 1, path.length()).c_str());
			else
			{
				aria::CJsonObject* newObject = new aria::CJsonObject();
				CJsonObject->_objectData.insert(std::make_pair(path.substr(point + 1, path.length()), newObject));
				return GetJsonObject(newObject, path.substr(point + 1, path.length()).c_str());
			}
		}

		return NULL;
	}

	std::string CNetworkRequestIntoJson::GetJsonString()
	{
		rapidjson::StringBuffer buffer;
		rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);

		_data.GenerateJson(writer);

		return buffer.GetString();
	}

	CNetworkResponseIntoJson::CNetworkResponseIntoJson(CConnection* connection)
	{
		_connection = connection;
	}

	CNetworkResponseIntoJson::~CNetworkResponseIntoJson(void)
	{
		_connection = NULL;
	}

	void CNetworkResponseIntoJson::SendPost()
	{
		std::string data = GetJsonString();

		int size = data.length() + sizeof(PacketBase);

		char* packet = _connection->PrepareSendPacket(size + 1);

		if (packet == NULL)
		{
			GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_HIGH, "SendBuffer�� ����");
			return;
		}

		packet[size] = '\0';

		PacketBase* base = (PacketBase*)packet;
		base->_length = size + 1;
		base->_type = _requestType;

		memcpy(packet + sizeof(PacketBase), data.c_str(), data.length());

		_connection->SendPost(size + 1);
	}

	void CNetworkResponseIntoJson::SendErrorPacket(std::string errorCode)
	{
		AddParam("errorCode", errorCode.c_str());
		AddParam("serverTime", GetUnixTimeStamp());

		SendPost();
	}

	void CNetworkResponseIntoJson::SendSuccessPacket()
	{
		AddParam("serverTime", GetUnixTimeStamp());

		SendPost();
	}

	CConnection* CNetworkResponseIntoJson::GetConnection()
	{
		return _connection;
	}

}