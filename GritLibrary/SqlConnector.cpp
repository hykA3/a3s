#include <iostream>
#include <WinSock2.h>
#include <Windows.h>
#include "SqlConnectorPool.h"
#include "SqlConnector.h"
#include "Log.h"
#include "errmsg.h"

namespace aria
{
	CSqlConnector::CSqlConnector(void) 
		: _sqlConnect(NULL), _sqlRealConnect(NULL)
	{
		_isPending = false;
	}


	CSqlConnector::~CSqlConnector(void)
	{
		Destroy();
		mysql_library_end();
	}

	void CSqlConnector::Destroy()
	{
		SAFE_DELETE_ARRAY(_sqlConnect);
	}

	bool CSqlConnector::Initialize()
	{
		if (_sqlConnect)
		{
			return false;
		}

		_sqlConnect = mysql_init(NULL);
		if (_sqlConnect == NULL)
		{
			std::string error = "Mysql Initialize 실패";
			error += mysql_error(_sqlConnect);

			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, (error));
			return false;
		}
		 
		//접속할 서버에 주소와 계정, 비번을 알아야 하는데..

		_sqlRealConnect = mysql_real_connect(_sqlConnect, "127.0.0.1", "Grit", "ariaLib", "aria", 3306, NULL, 0);  

		if (_sqlRealConnect == NULL)
		{
			std::string error = "Mysql Initialize 실패";
			error += mysql_error(_sqlConnect);

			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, (error));

			SAFE_MYSQL_CLOSE(_sqlConnect);
			return false;
		}

		mysql_query(_sqlConnect, "set session character_set_connection=euckr;");
		mysql_query(_sqlConnect, "set session character_set_results=euckr;");
		mysql_query(_sqlConnect, "set session character_set_client=euckr;");
		_isPending = true;

		std::string connect = "MysqlConnect : " + IntToString(_sqlConnect->net.fd);
		GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, connect.c_str());
		return true;
	}

	int CSqlConnector::SendQuery(const char* query, std::vector< std::map<std::string, std::string> > &result, bool autoRelease /*= true*/, int stack /*= 0*/)
	{
		if (_sqlConnect == NULL)
		{
			_isPending = true;
			return -1;
		}

		if (query == NULL)
		{
			_isPending = true;
			return -1;
		}

		if (int errorCode = mysql_query(_sqlConnect, query) != 0)
		{
			switch (errorCode)
			{
			case CR_COMMANDS_OUT_OF_SYNC:
			{
				TenLog("Mysql query error : %d, CR_COMMANDS_OUT_OF_SYNC", errorCode);
			}
			break;
			case CR_SERVER_GONE_ERROR:
			{
				TenLog("Mysql query error : %d, CR_SERVER_GONE_ERROR", errorCode);
			}
			break;
			case CR_SERVER_LOST:
			{
				TenLog("Mysql query error : %d, CR_SERVER_LOST", errorCode);
			}
			break;
			case CR_UNKNOWN_ERROR:
			{
				TenLog(query);
				TenLog("Mysql query error : %d, CR_UNKNOWN_ERROR", errorCode);
			}
			default:
			{
				TenLog(query);
				TenLog("Mysql query error : %d, CR_UNKNOWN_ERROR", errorCode);
			}
			break;
			}

			Destroy();
			Initialize();

			if (stack == 0)
			{
				errorCode = SendQuery(query, result, autoRelease, stack + 1);

				if (autoRelease)
					_isPending = true;
			}
			return -1;
		}

		MYSQL_RES* queryResult = mysql_store_result(_sqlConnect);

		if (queryResult)
		{
			int fieldCount = mysql_num_fields(queryResult);

			MYSQL_FIELD* fields = mysql_fetch_fields(queryResult);


			MYSQL_ROW row;
			while (row = mysql_fetch_row(queryResult))
			{
				std::map<std::string, std::string> rowResult;
				for (int i = 0; i < fieldCount; i++)
				{
					std::string name = fields[i].name;
					std::string value = row[i] == NULL ? "" : row[i];
					rowResult.insert(std::make_pair(name, value));
				}
				result.push_back(rowResult);
			}
			mysql_free_result(queryResult);
		}
		else
		{
			//결과값을 리턴하지 않는 쿼리를 요청 했을 때.
			if (mysql_field_count(_sqlConnect) == 0)
			{
				//몇개의 row가 영향을 받았는지 알려줌
				int count = mysql_affected_rows(_sqlConnect);

				if (autoRelease)
					_isPending = true;
				return count;
			}
			else
			{
				if (autoRelease)
					_isPending = true;
				GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_HIGH, std::string(mysql_error(_sqlConnect)));
				return -1;
			}
		}
		if (autoRelease)
			_isPending = true;
		return true;
	}
}

