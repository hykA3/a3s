//
// file : NetworkRequestIntoJson.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.14
//
#pragma once

#include "Delegate.h"
#include "Rapidjson/document.h"
#include "Rapidjson/writer.h"
#include "Rapidjson/stringbuffer.h"

typedef aria::delegate1<void, rapidjson::Value&> NetworkJsonResponseCallback;

namespace aria
{
	class CJsonObject
	{
	public:
		virtual ~CJsonObject()
		{
			_stringData.clear();
			_intData.clear();
			_boolData.clear();
			_longData.clear();

			if (_objectData.empty() == false)
			{
				auto itr = _objectData.begin();

				for (; itr != _objectData.end(); ++itr)
				{
					delete itr->second;
				}
			}

			if (_arrayData.empty() == false)
			{
				auto mapItr = _arrayData.begin();

				for (mapItr; mapItr != _arrayData.end(); ++mapItr)
				{
					auto itr = mapItr->second.begin();
					auto itrEnd = mapItr->second.end();

					for (; itr != itrEnd; ++itr)
						delete *itr;
				}
			}
		}

		void AddParam(const char* key, const char* value)
		{
			auto itr = _stringData.find(key);

			if (itr != _stringData.end())
				itr->second = value;
			else
				_stringData.insert(std::map< std::string, std::string>::value_type(key, value));
		}

		void AddParam(const char* key, int value)
		{
			auto itr = _intData.find(key);

			if (itr != _intData.end())
				itr->second = value;
			else
				_intData.insert(std::map< std::string, int>::value_type(key, value));
		}

		void AddParam(const char* key, float value)
		{
			auto itr = _floatData.find(key);

			if (itr != _floatData.end())
				itr->second = value;
			else
				_floatData.insert(std::map< std::string, float>::value_type(key, value));
		}

		void AddParam(const char* key, long long value)
		{
			auto itr = _longData.find(key);

			if (itr != _longData.end())
				itr->second = value;
			else
				_longData.insert(std::map< std::string, long long>::value_type(key, value));
		}

		void AddParam(const char* key, bool value)
		{
			auto itr = _boolData.find(key);

			if (itr != _boolData.end())
				itr->second = value;
			else
				_boolData.insert(std::map< std::string, bool>::value_type(key, value));
		}

		void AddParam(const char* key, CJsonObject* object, bool isArray)
		{
			if (isArray)
			{
				auto itr = _arrayData.find(key);

				if (itr != _arrayData.end())
				{
					itr->second.push_back(object);
				}
				else
				{
					std::vector<CJsonObject*> vec;
					vec.push_back(object);
					_arrayData.insert(std::map< std::string, std::vector<CJsonObject*> >::value_type(key, vec));
				}
			}
			else
			{
				auto itr = _objectData.find(key);

				if (itr != _objectData.end())
				{
					delete itr->second;
					itr->second = object;
				}
				else
				{
					_objectData.insert(std::map< std::string, CJsonObject*>::value_type(key, object));
				}
			}
		}

		void GenerateJson(rapidjson::Writer<rapidjson::StringBuffer>& writer)
		{
			writer.StartObject();
			{
				if (_stringData.empty() == false)
				{
					for (auto itr = _stringData.begin(); itr != _stringData.end(); ++itr)
					{
						writer.String(itr->first.c_str());
						writer.String(itr->second.c_str());
					}
				}

				if (_intData.empty() == false)
				{
					for (auto itr = _intData.begin(); itr != _intData.end(); ++itr)
					{
						writer.String(itr->first.c_str());
						writer.Int(itr->second);
					}
				}

				if (_floatData.empty() == false)
				{
					for (auto itr = _floatData.begin(); itr != _floatData.end(); ++itr)
					{
						writer.String(itr->first.c_str());
						writer.Double(itr->second);
					}
				}

				if (_longData.empty() == false)
				{
					for (auto itr = _longData.begin(); itr != _longData.end(); ++itr)
					{
						writer.String(itr->first.c_str());
						writer.Int64(itr->second);
					}
				}

				if (_boolData.empty() == false)
				{
					for (auto itr = _boolData.begin(); itr != _boolData.end(); ++itr)
					{
						writer.String(itr->first.c_str());
						writer.Bool(itr->second);
					}
				}

				if (_objectData.empty() == false)
				{
					for (auto itr = _objectData.begin(); itr != _objectData.end(); ++itr)
					{
						writer.String(itr->first.c_str());
						itr->second->GenerateJson(writer);
					}
				}

				if (_arrayData.empty() == false)
				{
					auto itr = _arrayData.begin();

					for (auto itr = _arrayData.begin(); itr != _arrayData.end(); ++itr)
					{
						writer.String(itr->first.c_str());

						writer.StartArray();
						{
							for (auto vecItr = itr->second.begin(); vecItr != itr->second.end(); ++vecItr)
								(*vecItr)->GenerateJson(writer);
						}
						writer.EndArray();
					}
				}
			}
			writer.EndObject();
		}

		std::map<std::string, std::string> _stringData;
		std::map<std::string, int> _intData;
		std::map<std::string, float> _floatData;
		std::map<std::string, long long> _longData;
		std::map<std::string, bool> _boolData;

		std::map<std::string, CJsonObject*> _objectData;
		std::map<std::string, std::vector<CJsonObject*> > _arrayData;
	};

	class CNetworkRequestIntoJson
	{
	public:
#pragma pack(push, 1)
		struct PacketBase
		{
			unsigned int _length;
			unsigned short _type;
		};
#pragma pack(pop)
		CNetworkRequestIntoJson(void);
		virtual ~CNetworkRequestIntoJson(void);

		bool AddParam(const char* key, const char* value);
		bool AddParam(const char* key, int value);
		bool AddParam(const char* key, float value);
		bool AddParam(const char* key, long long value);
		bool AddParam(const char* key, bool value);
		bool AddParam(const char* key, CJsonObject* object, bool isArray);

		bool AddParam(const char* key, const char* parentPath, const char* value);
		bool AddParam(const char* key, const char* parentPath, int value);
		bool AddParam(const char* key, const char* parentPath, float value);
		bool AddParam(const char* key, const char* parentPath, long long value);
		bool AddParam(const char* key, const char* parentPath, bool value);
		bool AddParam(const char* key, const char* parentPath, CJsonObject* object, bool isArray);


		CJsonObject* GetJsonObject(CJsonObject* jsonObject, const char* key);

		std::string GetJsonString();

		void SetRequestType(short type) { _requestType = type; }
		inline short GetRequestType() { return _requestType; }
	protected:
		short _requestType;

		CJsonObject _data;
	};

	class CConnection;
	class CNetworkResponseIntoJson : public CNetworkRequestIntoJson
	{
	public:
		CNetworkResponseIntoJson(CConnection* connection);
		virtual ~CNetworkResponseIntoJson(void);

		void SendPost();

		void SendErrorPacket(std::string errorCode);
		void SendSuccessPacket();

		CConnection* GetConnection();
		void SetConnection(CConnection* connection) { _connection = connection; }
	protected:
		CConnection* _connection;
		int _panding; //��������..
	};
}

