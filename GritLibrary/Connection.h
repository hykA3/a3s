//
// file : Connection.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#pragma once
#include "Library.h"
#include "RingBuffer.h"

namespace aria
{
	class  CConnection
	{
	public:

		CConnection();
		virtual ~CConnection();

		void InitializeConnection();

		bool CreateConnection(InitConfig& config, bool isAccept = true);
		virtual bool CloseConnection(bool init = true, bool force = false);


		bool BindAcceptExSock(bool socketType = true);
		bool BindConnectSock(const char* ip, int port, HANDLE& iocpHandle, bool socketType = true);

		SOCKET GetSocket() { return _connectionSocket; }
		void SetSocket(SOCKET socket) { _connectionSocket = socket; }

		inline int GetIndex() { return _index; }

		bool RecvPost(char* next, DWORD remain);
		bool SendPost(int sendSize);

		inline int IncrementAcceptIOReferenceCount() { return InterlockedIncrement((LPLONG)&_acceptIOReferenceCount); }
		inline int IncrementRecvIOReferenceCount() { return InterlockedIncrement((LPLONG)&_recvIOReferenceCount); }
		inline int IncrementSendIOReferenceCount() { return InterlockedIncrement((LPLONG)&_sendIOReferenceCount); }

		inline int DecrementAcceptIOReferenceCount() { return (_acceptIOReferenceCount ? InterlockedDecrement((LPLONG)&_acceptIOReferenceCount) : 0); }
		inline int DecrementRecvIOReferenceCount() { return (_recvIOReferenceCount ? InterlockedDecrement((LPLONG)&_recvIOReferenceCount) : 0); }
		inline int DecrementSendIOReferenceCount() { return (_sendIOReferenceCount ? InterlockedDecrement((LPLONG)&_sendIOReferenceCount) : 0); }

		inline int GetAcceptIOReferenceCount() { return _acceptIOReferenceCount; }
		inline int GetRecvIOReferenceCount() { return _recvIOReferenceCount; }
		inline int GetSendIOReferenceCount() { return _sendIOReferenceCount; }

		char* PrepareSendPacket(int length);
		bool ReleaseSendPacket(LPOVERLAPPED_EX sendOverlappedEx);

		inline void  SetConnectionIP(char* Ip) { memcpy(_connectionIP, Ip, MAX_IP_LENGTH); }
		char* GetConnectionIP() { return (char*)_connectionIP; }


		bool 	BindIOCP(HANDLE& iocpHandle);

	public:
		int				_index;
		bool			_isAcceptSocket;

		bool			_isConnect;
		bool			_isClosed;

		char			_addressBuffer[1024];
		BYTE			_connectionIP[MAX_IP_LENGTH];

		CRingBuffer		_sendRingBuffer;
		CRingBuffer		_recvRingBuffer;

		SOCKET			_connectionSocket;
		SOCKET			_serverListenSocket;

		HANDLE			_iocpHandle;


		int				_recvBufferSize;
		int				_sendBufferSize;

		//현재 Overlapped I/O 전송 작업을 하고 있는지 여부
		BOOL			_isSend;

		//Overlapped I/O 요청을 위한 변수
		OVERLAPPED_EX*		_recvOverlappedEx;
		OVERLAPPED_EX*		_sendOverlappedEx;

		//Overlapped i/o작업을 요청한 개수
		DWORD		_sendIOReferenceCount;
		DWORD		_recvIOReferenceCount;
		DWORD		_acceptIOReferenceCount;

		CCriticalSection _cs;
	};
}



