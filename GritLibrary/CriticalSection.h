//
// file : CriticalSection.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#pragma once

#include <Windows.h>

namespace aria
{
	//CriticalSection 관리
	class CCriticalSection
	{
	public:
		CCriticalSection()
		{
			InitializeCriticalSection(&_cs);
		}
		~CCriticalSection()
		{
			DeleteCriticalSection(&_cs);
		}

		inline void Lock()
		{
			EnterCriticalSection(&_cs);
		}

		inline void Unlock()
		{
			LeaveCriticalSection(&_cs);
		}
		
	private:
		CRITICAL_SECTION	_cs;
	};

	//지역변수로 TScopedLock을 생성하면서 Lock
	//지역변수가 사라질때 unlock 자동호출
	template<class CriticalSectionType>
	class TCriticalSectionScopedLock
	{
	public:
		inline TCriticalSectionScopedLock(CriticalSectionType& cs) : _cs(&cs)
		{
			_cs->Lock();
		}

		inline ~TCriticalSectionScopedLock()
		{
			_cs->Unlock();
		}
	
		TCriticalSectionScopedLock	&operator=(const TCriticalSectionScopedLock &rhs);
	private:
		CriticalSectionType*	_cs;
	};
	typedef TCriticalSectionScopedLock<CCriticalSection> ScopedCriticalSectionLock;
}

