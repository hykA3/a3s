//
// file : define.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#pragma once

#include "Library.h"

#define	MAX_RINGBUFFER_SIZE			1024 * 100

#define SAFE_DELETE(x)				if(x!=NULL){delete x; x = NULL;}
#define	SAFE_DELETE_ARRAY(x)		if(x!=NULL){delete[] x; x = NULL;}

#define MAX_WORKER_THREAD			10
#define MAX_PROCESS_THREAD			10

#define PACKET_HEADER_SIZE_LENGTH	4 //패킷 헤더중, 패킷의 크기를 나타내는 공간의 사이즈..
#define MAX_IP_LENGTH				20


enum OperationType
{
	//Worker IOCP operation
	OP_SEND,
	OP_RECV,
	OP_ACCEPT,

	OP_CONNECT,
	//Process IOCP operation
	OP_CLOSE,
	OP_RECV_PACKET, //순서성 패킷 처리
	OP_SYSTEM
};

typedef struct _InitConfig
{
	int _index;
	SOCKET _socketListener;

	int _processPacketCount;
	int _maxConnectionCount;

	//RecvBuffer 초기화 사이즈.. 
	int _recvBufferCount;
	int _recvBufferSize;

	int _sendBufferCount;
	int _sendBufferSize;

	unsigned short _serverPort;
	unsigned short _serverWorkerThreadCount;
	unsigned short _serverProcessThreadCount;

	_InitConfig()
	{
		ZeroMemory(this, sizeof(InitConfig));
	}
} InitConfig;

typedef struct _OVERLAPPED_EX
{
	WSAOVERLAPPED _overlapped;
	WSABUF _wsaBuf;
	int	_totalByte;
	DWORD _remain;
	char* _socketMsg;
	OperationType _operationType;
	void* _connection;

	_OVERLAPPED_EX(void* connection)
	{
		ZeroMemory(&_wsaBuf, sizeof(WSABUF));
		_totalByte = 0;
		_remain = 0;
		_socketMsg = NULL;
		_connection = connection;
	}
}OVERLAPPED_EX, *LPOVERLAPPED_EX;

//ProcessThread에서 처리할 패킷 정의..
//시스템메시지로 넘어옴..
typedef struct _PROCESS_PACKET
{
	OperationType _operationType;
	WPARAM _wParam;
	LPARAM _lParam;

	_PROCESS_PACKET()
	{
		Initialize();
	}
	void Initialize()
	{
		ZeroMemory(this, sizeof(PROCESS_PACKET));
	}
}PROCESS_PACKET;


//Access
#define DEFINE_GET_ACCESS(type, func, var ) \
public: \
	type Get##func() { return var; } 

#define DEFINE_GET_R_ACCESS(type, func, var ) \
public: \
	type &Get##func() { return var; } 

#define DEFINE_SET_ACCESS(type, func, var ) \
public: \
	virtual void Set##func(type data) { var = data; }

#define DEFINE_SET_R_ACCESS(type, func, var ) \
public: \
	virtual void Set##func(type &data) { var = data; }

#define DEFINE_GET_SET_ACCES(type, func, var) \
	DEFINE_GET_ACCESS(type, func, var) \
	DEFINE_SET_ACCESS(type, func, var) 

#define DEFINE_GET_R_SET_ACCES(type, func, var) \
	DEFINE_GET_R_ACCESS(type, func, var) \
	DEFINE_SET_ACCESS(type, func, var) 

#define DEFINE_GET_SET_R_ACCES(type, func, var) \
	DEFINE_GET_ACCESS(type, func, var) \
	DEFINE_SET_R_ACCESS(type, func, var) 

#define DEFINE_GET_R_SET_R_ACCES(type, func, var) \
	DEFINE_GET_R_ACCESS(type, func, var) \
	DEFINE_SET_R_ACCESS(type, func, var) 

#define DEFINE_GET_ACCESS_WITH_VAR(type, func, var) \
	DEFINE_GET_ACCESS(type, func, var) \
protected: \
	type var; 

#define DEFINE_GET_R_ACCESS_WITH_VAR(type, func, var) \
	DEFINE_GET_R_ACCESS(type, func, var) \
protected: \
	type var; 

#define DEFINE_SET_ACCESS_WITH_VAR(type, func, var) \
	DEFINE_SET_ACCESS(type, func, var) \
protected: \
	type var; 

#define DEFINE_GET_SET_ACCESS_WITH_VAR(type, func, var) \
	DEFINE_GET_SET_ACCES(type, func, var) \
protected: \
	type var; 

#define DEFINE_GET_R_SET_R_ACCESS_WITH_VAR(type, func, var) \
	DEFINE_GET_R_SET_R_ACCES(type, func, var) \
protected: \
	type var; 