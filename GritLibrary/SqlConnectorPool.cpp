//
// file : SqlConnectorPool.cpp
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.08
//

#include <iostream>
#include "define.h"
#include "Library.h"
#include "SqlConnectorPool.h"
#include "SqlConnector.h"
#include <memory>

namespace aria
{
	CSqlConnectorPool::CSqlConnectorPool(void) 
	{
		_sqlConnector = NULL;
		_connectorMaxCount = 0;
	}


	CSqlConnectorPool::~CSqlConnectorPool(void)
	{
		Destroy();
	}

	void CSqlConnectorPool::Destroy()
	{
		SAFE_DELETE_ARRAY(_sqlConnector);

		if (_requestQueue.Empty() == false)
		{
			_requestQueue.Lock();
			int size = _requestQueue.Size();
			for (int i = 0; i < size; ++i)
			{
				delete	_requestQueue[i];
			}

			_requestQueue.Unlock();
		}
	}

	bool CSqlConnectorPool::Initialize()
	{
		_connectorMaxCount = 3;
		if (_connectorMaxCount <= 0)
		{
			_connectorMaxCount = 0;

			GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, "SqlConnector Count is 0");
			return false;
		}

		Destroy();

		_sqlConnector = new CSqlConnector[_connectorMaxCount];
		if (_sqlConnector == NULL)
		{
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, "SqlConnector alloc failed");
			return false;
		}

		for (int i = 0; i < _connectorMaxCount; ++i)
		{
			if (_sqlConnector[i].Initialize() == false)
			{
				Destroy();
				return false;
			}
		}

		return true;
	}


	int CSqlConnectorPool::NetworkRequest(const char* query, CSqlResult& result)
	{
		if (query == NULL || _sqlConnector == NULL)
			return false;

		_cs.Lock();

		while (true)
		{
			for (int i = 0; i < _connectorMaxCount; ++i)
			{
				if (_sqlConnector[i].IsPending())
				{
					_sqlConnector[i].PreReservation();

					_cs.Unlock();

					return _sqlConnector[i].SendQuery(query, result);
				}
			}
		}

		return false;
	}

	bool CSqlConnectorPool::AsyncNetworkRequest(const char* query, const CSqlRequestCallback* callback)
	{
		if (query == NULL || callback == NULL)
		{
			return false;
		}

		CSqlRequest* request = new CSqlRequest();

		request->_query = query;
		request->_callback = callback;

		_requestQueue.Push(request);
		return true;
	}

	void CSqlConnectorPool::Update()
	{
		if (_requestQueue.Empty() == true)
			return;

		{

		}
	}

	CSqlConnector* CSqlConnectorPool::StartTransaction()
	{
		_cs.Lock();

		while (true)
		{
			for (int i = 0; i < _connectorMaxCount; ++i)
			{
				if (_sqlConnector[i].IsPending())
				{
					_sqlConnector[i].PreReservation();

					_cs.Unlock();

					CSqlResult result;
					if (_sqlConnector[i].SendQuery("BEGIN", result, false) < 0)
					{
						return NULL;
					}
					return &_sqlConnector[i];
				}
			}
		}

		return NULL;
	}

	void CSqlConnectorPool::EndTransaction(CSqlConnector* connector, bool isRollback)
	{
		if (connector == NULL)
			return;

		CSqlResult result;
		if (isRollback)
		{
			connector->SendQuery("ROLLBACK", result);
		}
		else
		{
			connector->SendQuery("COMMIT", result);
		}
	}
}

