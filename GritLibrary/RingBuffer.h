//
// file : RingBuffer.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#pragma once

#include "Library.h"
#include "CriticalSection.h"

namespace aria
{
	class CRingBuffer
	{
	public:
		CRingBuffer();
		~CRingBuffer();

		//링버퍼 할당
		bool			Create(int bufferSize = MAX_RINGBUFFER_SIZE);
		//초기화
		bool			Initialize();

		//내부 버퍼의 현재 포인터를 전진
		char*			ForwardMark(int forwardLength);
		char*			ForwardMark(int forwardLength, int nextLength, DWORD remainLength);

		//사용된 내부 버퍼 해제
		void			ReleaseBuffer(int releaseSize);

		//내부 버퍼 데이터를 읽어서 반환
		char*			GetBuffer(int readSize, int* outputReadSize);

		//SendPost에서만 불리는 함수..서로 다른 스레드를 쓰는 상황에서 
		//제대로된 사이즈를 계산 할수 없기 떄문..
		void			SetUsedBufferSize(int usedBufferSize);

		//사용된 내부 버퍼 크기 반환
		int				GetUsedBufferSize() { return _usedBufferSize; }

		//해당하는 내부 버퍼의 포인터를 반환
		inline	char*	GetBeginMark() { return _beginMark; }
		inline	char*	GetCurrentMark() { return _currentMark; }
		inline	char*	GetEndMark() { return _endMark; }

		//할당된 버퍼 크기를 반환한다.
		inline	int		GetBufferSize() { return _totalBufferSize; }
		

		//누적 버퍼 사용양 반환
		int				GetAllUsedBufferSize() { return _allUsedBufferSize; }

		

	protected:
		char*				_ringBuffer;			//실제 데이터를 저장하는 버퍼 포인터

		char*				_beginMark;				//버퍼의 처음부분을 가리키고 있는 포인터
		char*				_currentMark;			//버퍼의 현재까지 사용된 부분을 가리키는 포인터
		char*				_endMark;				//버퍼의 마지막부분을 가리키고 있는 포인터

		char*				_gettedBufferMark;		//현재까지 데이터를 읽은 버퍼 포인터
		char*				_lastMoveMark;			//recycle되기 전에 마지막 포인터
							
		int					_totalBufferSize;			//내부 버퍼의 총 크기
		int					_usedBufferSize;		//현재 사용된 부분을 가리키는 포인터
		UINT				_allUsedBufferSize;		//총 처리된 데이터양

		CCriticalSection	_cs;					//동기화 객체

	private:
		CRingBuffer(const CRingBuffer &rhs);
		CRingBuffer &operator = (const CRingBuffer &rhs);
	};

}
