//
// file : Log.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.07
//

#pragma once

#include "Library.h"
#include "Queue.h"
#include "Singleton.h"

enum LogSaveType 
{
	LOG_SAVE_TYPE_FILE				=	0x00000000,
	LOG_SAVE_TYPE_SQL				=	0x00000001,
	LOG_SAVE_TYPE_NOSQL				=	0x00000002
};

enum LogInfoTypeNormal
{
	LOG_INFO_TYPE_NORMAL			=	0x00000001,
	LOG_INFO_TYPE_HIGH				=	0x00000002,
	LOG_INFO_TYPE_CRITICAL			=	0x00000004
};

enum LogInfoTypeError
{
	LOG_INFO_TYPE_ERROR_NORMAL		=	0x00000008,
	LOG_INFO_TYPE_ERROR_HIGH		=	0x0000000F,
	LOG_INFO_TYPE_ERROR_CRITICAL	=	0x00000010
};

struct LogInfo
{
	time_t			_time;
	DWORD			_logType;
	std::string		_message;
};

namespace aria
{
#define GLOBAL_LOG_HELPER	aria::CLog::GetInstance()

	class CLog : public CSingleton<CLog>
	{
	public:
		CLog();
		virtual ~CLog();

		bool	Initialize();
		void	Destroy();

		void	Normal(LogInfoTypeNormal logType, const char* msg, ...);
		void	Error(LogInfoTypeError logType, std::string msg);
		void	InsertLog(LogInfo* logInfo);

		CSafeDeque<LogInfo*>*	GetLog() { return &_logQueue; }

	private:
		CSafeDeque<LogInfo*>	_logQueue;
	};
};


