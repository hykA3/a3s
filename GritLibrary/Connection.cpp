//
// file : Connection.cpp
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#include <iostream>
#include "Connection.h"
#include "Log.h"
#include "IOCPServer.h"
#include <ws2tcpip.h>

namespace aria
{
	CConnection::CConnection()
	{
		_serverListenSocket = INVALID_SOCKET;
		_connectionSocket = INVALID_SOCKET;

		_recvOverlappedEx = NULL;
		_sendOverlappedEx = NULL;

		_isAcceptSocket = true;

		memset(&_addressBuffer, 0, sizeof(_addressBuffer));

		InitializeConnection();
	}

	CConnection::~CConnection()
	{
		CloseConnection(false);

		SAFE_DELETE_ARRAY(_recvOverlappedEx);
		SAFE_DELETE_ARRAY(_sendOverlappedEx);

		_serverListenSocket = INVALID_SOCKET;
		_connectionSocket = INVALID_SOCKET;
	}

	void CConnection::InitializeConnection()
	{
		ZeroMemory(_connectionIP, MAX_IP_LENGTH);

		_connectionSocket = INVALID_SOCKET;

		_isConnect = false;
		_isClosed = false;
		_isSend = true;

		_sendIOReferenceCount = 0;
		_recvIOReferenceCount = 0;
		_acceptIOReferenceCount = 0;


		_recvRingBuffer.Initialize();
		_sendRingBuffer.Initialize();
	}

	bool CConnection::CreateConnection(InitConfig& config, bool isAccept /* = true */)
	{
		_isAcceptSocket = isAccept;

		_index = config._index;
		_serverListenSocket = config._socketListener;

		_recvOverlappedEx = new OVERLAPPED_EX(this);
		_sendOverlappedEx = new OVERLAPPED_EX(this);

		_recvRingBuffer.Create(config._recvBufferSize * config._recvBufferCount);
		_sendRingBuffer.Create(config._sendBufferSize * config._sendBufferCount);

		_recvBufferSize = config._recvBufferSize;
		_sendBufferSize = config._sendBufferSize;

		if (isAccept)
		{
			return BindAcceptExSock();
		}

		return true;
	}

	bool CConnection::CloseConnection(bool init /*= true*/, bool force /*= false*/)
	{
		ScopedCriticalSectionLock lock(_cs);


		struct linger li = { 0, 0 };
		if (force)
		{
			li.l_onoff = 1;
		}

		if (_isConnect == true)
		{
			GLOBAL_IOCP_SERVER->OnClose(this);
		}

		shutdown(_connectionSocket, SD_BOTH);
		setsockopt(_connectionSocket, SOL_SOCKET, SO_LINGER, (char *)&li, sizeof(li));
		closesocket(_connectionSocket);

		_connectionSocket = INVALID_SOCKET;
		if (_recvOverlappedEx != NULL)
		{
			_recvOverlappedEx->_remain = 0;
			_recvOverlappedEx->_totalByte = 0;
		}

		if (_sendOverlappedEx != NULL)
		{
			_sendOverlappedEx->_remain = 0;
			_sendOverlappedEx->_totalByte = 0;
		}

		//connection을 다시 초기화 시켜준다.
		if (init)
		{
			InitializeConnection();

			if (_isAcceptSocket)
				BindAcceptExSock();
		}
		return true;
	}

	bool CConnection::BindAcceptExSock(bool socketType /*= true*/)
	{
		if (_serverListenSocket == 0)
		{
			return true;
		}			

		DWORD bytes = 0;

		memset(&_recvOverlappedEx->_overlapped, 0, sizeof(OVERLAPPED));

		_recvOverlappedEx->_wsaBuf.buf = _addressBuffer;
		_recvOverlappedEx->_socketMsg = &_recvOverlappedEx->_wsaBuf.buf[0];
		_recvOverlappedEx->_wsaBuf.len = _recvBufferSize;
		_recvOverlappedEx->_operationType = OP_ACCEPT;
		_recvOverlappedEx->_connection = this;

		if (socketType == true)
		{
			_connectionSocket = WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_IP, NULL, 0, WSA_FLAG_OVERLAPPED);
		}
		else
		{
			_connectionSocket = WSASocketW(AF_INET, SOCK_DGRAM, IPPROTO_IP, NULL, 0, WSA_FLAG_OVERLAPPED);
		}		

		if (_connectionSocket == INVALID_SOCKET)
		{
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "Connection::BindAcceptExSock(), WSASocket 에러");
			return false;
		}

		IncrementAcceptIOReferenceCount();


		BOOL ret = AcceptEx(
			_serverListenSocket,
			_connectionSocket,
			_recvOverlappedEx->_wsaBuf.buf,
			0,
			sizeof(SOCKADDR_IN) + 16,
			sizeof(SOCKADDR_IN) + 16,
			&bytes,
			(LPOVERLAPPED)_recvOverlappedEx);

		int error = WSAGetLastError();
		if (ret == FALSE && error != WSA_IO_PENDING)
		{
			DecrementAcceptIOReferenceCount();

			std::string temp = "Connection::BindAcceptExSock(), AcceptEx 에러 : " + IntToString(error);
			//GlobalLogHelper->Error(LOG_INFO_TYPE_ERROR_NORMAL, temp);

			return false;
		}

		return true;
	}

	bool CConnection::BindConnectSock(const char* ip, int port, HANDLE& iocpHandle, bool socketType /*= true*/)
	{
		if (ip == NULL)
			return false;

		if (_serverListenSocket == 0)
			return true;

		DWORD bytes = 0;

		memset(&_recvOverlappedEx->_overlapped, 0, sizeof(OVERLAPPED));

		_recvOverlappedEx->_wsaBuf.buf = _addressBuffer;
		_recvOverlappedEx->_socketMsg = &_recvOverlappedEx->_wsaBuf.buf[0];
		_recvOverlappedEx->_wsaBuf.len = _recvBufferSize;
		_recvOverlappedEx->_operationType = OP_CONNECT;
		_recvOverlappedEx->_connection = this;

		if (socketType == true)
		{
			_connectionSocket = WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_IP, NULL, 0, WSA_FLAG_OVERLAPPED);
		}
		else
		{
			_connectionSocket = WSASocketW(AF_INET, SOCK_DGRAM, IPPROTO_IP, NULL, 0, WSA_FLAG_OVERLAPPED);
		}

		if (_connectionSocket == INVALID_SOCKET)
		{
			//GlobalLogHelper->Error(LOG_INFO_TYPE_ERROR_NORMAL, "Connection::BindAcceptExSock(), WSASocket 에러");
			return false;
		}

		SOCKADDR_IN addr;

		memset(&addr, 0, sizeof(addr));

		addr.sin_family = AF_INET;
		int result = bind(_connectionSocket, (sockaddr*)&addr, sizeof(addr));

		//ConnectionEX 함수를 얻어와야함
		DWORD byte = 0;
		LPFN_CONNECTEX ConnectionEx = NULL;
		GUID GuidConnectEx = WSAID_CONNECTEX;
		// Overlap의 Connection을 위한 함수포인터인 lpfnConnectEx을 얻어온다.
		result = ::WSAIoctl(
			_connectionSocket,
			SIO_GET_EXTENSION_FUNCTION_POINTER,
			&GuidConnectEx,
			sizeof(GuidConnectEx),
			&ConnectionEx,
			sizeof(ConnectionEx),
			&byte,
			NULL,
			NULL
		);

		if (result == SOCKET_ERROR)
		{
			CloseConnection(false);

			return false;
		}

		if (BindIOCP(iocpHandle) == false)
		{
			CloseConnection(false);

			return false;
		}


		addr.sin_family = AF_INET;
		addr.sin_port = htons(port);
		inet_pton(AF_INET, ip, &(addr.sin_addr));

		//result = WSAConnect( _connectionSocket, (sockaddr*)& addr, sizeof(addr), NULL, NULL, NULL, NULL);

		DWORD sendByte = 0;
		result = ConnectionEx(_connectionSocket, (sockaddr*)& addr, sizeof(addr), NULL, 0, NULL, (LPOVERLAPPED)_recvOverlappedEx);

		if (result == false && WSAGetLastError() != WSA_IO_PENDING)
		{
			int error = WSAGetLastError();
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "TenIOCPServer::ConnectLoginServer(), login server connect 실패");
			return false;
		}

		return true;
	}

	char* CConnection::PrepareSendPacket(int length)
	{
		if (_isConnect == false)
			return NULL;

		char* buffer = _sendRingBuffer.ForwardMark(length);

		if (buffer == NULL)
		{
			GLOBAL_IOCP_SERVER->CloseConnection(this);
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, "Connection::PrepareSendPacket(), SendRingBuffer Overflow");
			return NULL;
		}

		ZeroMemory(buffer, length);
		CopyMemory(buffer, &length, PACKET_HEADER_SIZE_LENGTH);

		return buffer;
	}

	bool CConnection::ReleaseSendPacket(LPOVERLAPPED_EX sendOverlappedEx)
	{
		if (sendOverlappedEx == NULL)
			return false;

		_sendRingBuffer.ReleaseBuffer(_sendOverlappedEx->_wsaBuf.len);
		sendOverlappedEx = NULL;
		return true;
	}

	bool CConnection::BindIOCP(HANDLE& iocpHandle)
	{
		HANDLE tempIOCPHandle;
		ScopedCriticalSectionLock lock(_cs);


		tempIOCPHandle = CreateIoCompletionPort(
			(HANDLE)_connectionSocket,
			iocpHandle,
			reinterpret_cast<DWORD>(this),
			0);

		if (NULL == tempIOCPHandle || iocpHandle != tempIOCPHandle)
		{
			int error = WSAGetLastError();
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "Connection::BindIOCP(), CreateIoCompletionPort 실패");
			return false;
		}
		_iocpHandle = iocpHandle;
		return true;
	}

	bool CConnection::RecvPost(char* next, DWORD remain)
	{
		int ret = 0;
		DWORD flag = 0;
		DWORD recvNumBytes = 0;

		if (_isConnect == false || _recvOverlappedEx == NULL)
			return false;

		_recvOverlappedEx->_operationType = OP_RECV;
		_recvOverlappedEx->_remain = remain;

		int moveMark = remain - (_recvRingBuffer.GetCurrentMark() - next);
		_recvOverlappedEx->_wsaBuf.len = _recvBufferSize;
		_recvOverlappedEx->_wsaBuf.buf = _recvRingBuffer.ForwardMark(moveMark, _recvBufferSize, remain);

		if (_recvOverlappedEx->_wsaBuf.buf == NULL)
		{
			GLOBAL_IOCP_SERVER->CloseConnection(this);
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "Connection::RecvPost(), _recvRingBuffer ForwardMark 실패");
			return false;
		}
		_recvOverlappedEx->_socketMsg = _recvOverlappedEx->_wsaBuf.buf - remain;

		memset(&_recvOverlappedEx->_overlapped, 0, sizeof(OVERLAPPED));

		IncrementRecvIOReferenceCount();

		int recvResult = WSARecv(
			_connectionSocket,
			&_recvOverlappedEx->_wsaBuf,
			1,
			&recvNumBytes,
			&flag,
			&_recvOverlappedEx->_overlapped,
			NULL);

		if (recvResult == SOCKET_ERROR && WSAGetLastError() != WSA_IO_PENDING)
		{
			DecrementRecvIOReferenceCount();
			GLOBAL_IOCP_SERVER->CloseConnection(this);
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "Connection::RecvPost(), WSARecv 실패");
			return false;
		}
		return true;
	}


	bool CConnection::SendPost(int sendSize)
	{
		DWORD bytes;

		if (sendSize > 0)
			_sendRingBuffer.SetUsedBufferSize(sendSize);

		if (InterlockedCompareExchange((LPLONG)&_isSend, FALSE, TRUE) == TRUE)
		{
			int readSize;
			char* buffer = _sendRingBuffer.GetBuffer(_sendBufferSize, &readSize);
			if (buffer == NULL)
			{
				InterlockedExchange((LPLONG)&_isSend, TRUE);
				return false;
			}
			_sendOverlappedEx->_remain = 0;
			_sendOverlappedEx->_operationType = OP_SEND;
			_sendOverlappedEx->_totalByte = readSize;
			ZeroMemory(&_sendOverlappedEx->_overlapped, sizeof(OVERLAPPED));
			_sendOverlappedEx->_wsaBuf.len = readSize;
			_sendOverlappedEx->_wsaBuf.buf = buffer;
			_sendOverlappedEx->_connection = this;

			IncrementSendIOReferenceCount();

			int ret = WSASend(
				_connectionSocket,
				&_sendOverlappedEx->_wsaBuf,
				1,
				&bytes,
				0,
				&_sendOverlappedEx->_overlapped,
				NULL);
			if (ret == SOCKET_ERROR && WSAGetLastError() != WSA_IO_PENDING)
			{
				DecrementSendIOReferenceCount();
				if (GLOBAL_IOCP_SERVER->CloseConnection(this) == true)
				{
					this->CloseConnection(true);
				}
				GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "Connection::RecvPost(), WSASend 실패");
				InterlockedExchange((LPLONG)&_isSend, FALSE);
				return false;
			}
			//GlobalLogHelper->Normal(LOG_INFO_TYPE_INFO_NORMAL, "%d에게 %d 바이트 전송", _connectionSocket, sendSize );
		}
		return true;
	}
}
