//
// file : Delegate.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#pragma once

#include "Delegate/delegate.h"

namespace aria
{
	//delegate 편하게 쓰기 위한..  정의..


	//리턴 타입만 존재하고, 함수의 인자는 없는 경우...
	template< typename ReturnType >
	class delegate0 : public fd::delegate0<ReturnType>
	{
	public:
		delegate0(void* arg = NULL) {}

		delegate0(const fd::delegate0<ReturnType>& other) : fd::delegate0<ReturnType>(other)
		{
		}
		delegate0(const delegate0<ReturnType>& other) : fd::delegate0<ReturnType>(other)
		{
		}

		delegate0<ReturnType>* Clone() const
		{
			return new delegate0<ReturnType>(*this);
		}
		const delegate0* operator&() const { return this; }
	};

	template< typename ReturnType >
	delegate0<ReturnType> ConvertDelegate(const fd::delegate0<ReturnType>& other)
	{
		return delegate0<ReturnType>(other, target);
	}

	//리턴 타입과 함수의 매개변수가 한개 존재 하는경우..
	template< typename ReturnType, typename Type1 >
	class delegate1 : public fd::delegate1<typename ReturnType, typename Type1>
	{
	public:
		delegate1(void* arg = NULL) {}

		delegate1(const fd::delegate1<ReturnType, Type1>& other) : fd::delegate1<ReturnType, Type1>(other)
		{
		}

		delegate1(const delegate1<ReturnType, Type1>& other) : fd::delegate1<ReturnType, Type1>(other)
		{
		}

		delegate1<ReturnType, Type1>* Clone() const
		{
			return new delegate1<ReturnType, Type1>(*this);
		}
		const delegate1* operator&() const { return this; }
	};

	template< typename ReturnType, typename Type1  >
	delegate1<ReturnType, Type1> ConvertDelegate(const fd::delegate1<ReturnType, Type1>& other)
	{
		return delegate1<ReturnType, Type1>(other);
	}

	//리턴 타입과 함수의 매개변수가 두개 존재 하는경우..
	template< typename ReturnType, typename Type1, typename Type2 >
	class delegate2 : public fd::delegate2<typename ReturnType, typename Type1, typename Type2>
	{
	public:
		delegate2(void* arg = NULL) {}

		delegate2(const fd::delegate2<ReturnType, Type1, Type2>& other) : fd::delegate2<ReturnType, Type1, Type2>(other)
		{
		}

		delegate2(const delegate2<ReturnType, Type1, Type2>& other) : fd::delegate2<ReturnType, Type1, Type2>(other)
		{
		}

		delegate2<ReturnType, Type1, Type2>* Clone() const
		{
			return new delegate1<ReturnType, Type1, Type2>(*this);
		}
		const delegate2* operator&() const { return this; }
	};

	template< typename ReturnType, typename Type1, typename Type2  >
	delegate2<ReturnType, Type1, Type2> ConvertDelegate(const fd::delegate2<ReturnType, Type1, Type2>& other)
	{
		return delegate2<ReturnType, Type1, Type2>(other);
	}


#define MAKE_DELEGATE( fn, target )						ConvertDelegate( fd::make_delegate( fn, target ) )
#define MAKE_DELEGATE_REFCOUNTED( fn, target )			ConvertDelegate( fd::make_delegate( fn, target ), dynamic_cast<inno::RefCounted*>(target) )

#define REGISTER_CALLBACK(a)							(&MAKE_DELEGATE(&a, this))
#define REGISTER_CALLBACK_WITH_TARGET(a, target)		(&MAKE_DELEGATE(&a, target))
}
