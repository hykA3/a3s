//
// file : IOCPServer.cpp
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.08
//
#include <iostream>
#include "IOCPServer.h"
#include "Log.h"
#include "Connection.h"
#include "SqlConnectorPool.h"
#include "RedisConnector.h"
#include <WS2tcpip.h>

namespace aria
{
	CIOCPServer* CIOCPServer::_instance = NULL;
	unsigned int WINAPI CallWorkerThread(void* object)
	{
		CIOCPServer* server = (CIOCPServer*)object;
		server->WorkerThread();
		return 1;
	}

	unsigned int WINAPI CallProcessThread(void* object)
	{
		CIOCPServer* server = (CIOCPServer*)object;
		server->ProcessThread();
		return 1;
	}

	CIOCPServer::CIOCPServer()
	{
		_privateKey = 0;
		_workerThreadRun = true;
		_processThreadRun = true;

		_processThreadCount = 0;
		_workerThreadCount = 0;

		_processPacket = NULL;

		_isServerRun = false;
	}


	CIOCPServer::~CIOCPServer()
	{
		_instance = NULL;

		SAFE_DELETE_ARRAY(_processPacket);

		WSACleanup();

		CLog::DestroyInstance();
		CSqlConnectorPool::DestroyInstance();
	}


	bool CIOCPServer::StartServer(InitConfig& config, bool socketType)
	{
		_serverPort = config._serverPort;
		_workerThreadCount = config._serverWorkerThreadCount;
		_processThreadCount = config._serverProcessThreadCount;

		if (InitializeSocket() == false)
		{
			return false;
		}

		if (CreateWorkerIOCP() == false)
		{
			return false;
		}

		if (CreateProcessIOCP() == false)
		{
			return false;
		}

		if (CreateWorkerThread() == false)
		{
			return false;
		}

		if (CreateProcessThread() == false)
		{
			return false;
		}

		if (CreateListenSocket(socketType) == false)
		{
			return false;
		}

		config._socketListener = GetListenSocket();

		SAFE_DELETE_ARRAY(_processPacket);

		_processPacket = new PROCESS_PACKET[config._processPacketCount];
		_processPacketCount = config._processPacketCount;

		GLOBAL_SQL_CONNECTOR->Initialize();

		_isServerRun = true;
		return true;
	}



	bool CIOCPServer::StopServer()
	{
		GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, "Server Down Start");

		//모든 쓰레드를 중단시키고 중단이 완료 될 때 까지..기다린다
		if (_workerIOCPHandle)
		{
			_workerThreadRun = false;
			for (DWORD i = 0; i < _workerThreadCount; ++i)
			{
				PostQueuedCompletionStatus(_workerIOCPHandle, 0, 0, NULL);
			}
			Sleep(1000); //스레드가 끝나길 기다려야 하는데.. 왜 CloseHandle을 시켰을까?

			CloseHandle(_workerIOCPHandle);
			_workerIOCPHandle = NULL;
		}

		if (_processIOCPHandle)
		{
			_processThreadRun = false;
			for (DWORD i = 0; i < _processThreadCount; ++i)
			{
				PostQueuedCompletionStatus(_processIOCPHandle, 0, 0, NULL);
			}
			Sleep(1000); //스레드가 끝나길 기다려야 하는데.. 왜 CloseHandle을 시켰을까?

			CloseHandle(_processIOCPHandle);
			_processIOCPHandle = NULL;
		}

		//worker thread 핸들을 닫는다.
		for (unsigned int i = 0; i < _workerThreadCount; ++i)
		{
			if (_workerThreadHandle[i] != INVALID_HANDLE_VALUE)
				CloseHandle(_workerThreadHandle[i]);
			_workerThreadHandle[i] = INVALID_HANDLE_VALUE;
		}

		//process thread 핸들을 닫는다.
		for (unsigned int i = 0; i < _processThreadCount; ++i)
		{
			if (_processThreadHandle[i] != INVALID_HANDLE_VALUE)
				CloseHandle(_processThreadHandle[i]);
			_processThreadHandle[i] = INVALID_HANDLE_VALUE;
		}

		if (_serverListenSocket != INVALID_SOCKET)
		{
			closesocket(_serverListenSocket);
			_serverListenSocket = INVALID_SOCKET;
		}
		GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, "Server Down");

		_isServerRun = false;
		return true;
	}

	bool CIOCPServer::CloseConnection(CConnection* connection)
	{
		//각 요청에 대한 레퍼런스 카운트가 남이 있다면.. 
		//소켓은 일단 닫아버리고, 이 요청들이 모두 처리 될 때까지 iocp에서 completion 될때까지 기다려야한다
		if (connection->GetAcceptIOReferenceCount() != 0 ||
			connection->GetRecvIOReferenceCount() != 0 ||
			connection->GetSendIOReferenceCount() != 0)
		{
			shutdown(connection->GetSocket(), SD_BOTH);
			closesocket(connection->GetSocket());

			connection->SetSocket(INVALID_SOCKET);

			return true;
		}

		if (InterlockedCompareExchange((LPLONG)&connection->_isClosed, TRUE, FALSE) == FALSE)
		{
			PROCESS_PACKET* processPacket(GetProcessPacket(OP_CLOSE, NULL, NULL));

			if (processPacket == NULL)
				return false;

			if (PostQueuedCompletionStatus(_processIOCPHandle, 0, (ULONG_PTR)connection, (LPOVERLAPPED)processPacket) == 0)
			{
				ClearProcessPacket(processPacket);
				GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "IOCPServer::CloseConnection() 실패");

				connection->CloseConnection(true);
			}
		}

		return true;
	}

	bool CIOCPServer::InitializeSocket()
	{
		for (unsigned short i = 0; i < _workerThreadCount; ++i)
		{
			_workerThreadHandle[i] = INVALID_HANDLE_VALUE;
		}


		_workerIOCPHandle = INVALID_HANDLE_VALUE;
		_processIOCPHandle = INVALID_HANDLE_VALUE;

		_serverListenSocket = INVALID_SOCKET;

		WSADATA		wsaData;

		if (WSAStartup(MAKEWORD(2, 2), &wsaData))
		{
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, "WSA Initialize Failed");
			return false;
		}

		return true;
	}

	bool CIOCPServer::CreateWorkerIOCP()
	{
		_workerIOCPHandle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 1);

		if (_workerIOCPHandle == NULL)
		{
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, "workerIOCPHandle 실패");
			return false;
		}

		return true;
	}

	bool CIOCPServer::CreateProcessIOCP()
	{
		_processIOCPHandle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 1);

		if (_processIOCPHandle == NULL)
		{
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, "processIOCPHandle 실패");
			return false;
		}
		return true;
	}

	bool CIOCPServer::CreateWorkerThread()
	{
		HANDLE	thread = INVALID_HANDLE_VALUE;
		UINT	threadId = -1;

		for (DWORD count = 0; count < _workerThreadCount; ++count)
		{
			thread = (HANDLE)_beginthreadex(NULL, 0, &CallWorkerThread,
				this, CREATE_SUSPENDED, &threadId);

			if (thread == NULL)
			{
				GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, "IOCPServer::CreateWorkerThread(), thread 생성 실패");
				return false;
			}

			_workerThreadHandle[count] = thread;
			ResumeThread(thread);
		}

		_workerThreadRun = true;

		return true;
	}

	bool CIOCPServer::CreateProcessThread()
	{
		HANDLE	thread = INVALID_HANDLE_VALUE;
		UINT	threadId = -1;

		for (DWORD count = 0; count < _processThreadCount; count++)
		{
			thread = (HANDLE)_beginthreadex(NULL, 0, &CallProcessThread,
				this, CREATE_SUSPENDED, &threadId);

			if (thread == NULL)
			{
				GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, "IOCPServer::CreateProcessThread(), thread 생성 실패");
				return false;
			}

			_processThreadHandle[count] = thread;
			ResumeThread(thread);
			SetThreadPriority(thread, THREAD_PRIORITY_TIME_CRITICAL);
		}

		_processThreadRun = true;

		return true;
	}

	bool CIOCPServer::CreateListenSocket(bool socketType)
	{
		SOCKADDR_IN addr;

		if (socketType == true)
		{
			_serverListenSocket = WSASocketW(AF_INET, SOCK_STREAM, IPPROTO_IP, NULL, 0, WSA_FLAG_OVERLAPPED);
		}
		else
		{
			_serverListenSocket = WSASocketW(AF_INET, SOCK_DGRAM, IPPROTO_IP, NULL, 0, WSA_FLAG_OVERLAPPED);
		}
		
		if (_serverListenSocket == INVALID_SOCKET)
		{
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "IOCPServer::CreateListenSocket(), 리슨 소켓 생성 실패..");

			return false;
		}

		addr.sin_family = AF_INET;
		addr.sin_port = htons(_serverPort);
		addr.sin_addr.S_un.S_addr = htonl(INADDR_ANY); //아무 아이피나 받겟다.

		int result = bind(_serverListenSocket, (sockaddr*)& addr, sizeof(addr));

		if (result == SOCKET_ERROR)
		{
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "IOCPServer::CreateListenSocket(), 바인드 실패");
			return false;
		}

		if (socketType == true)
		{
			result = listen(_serverListenSocket, 50);

			if (result == SOCKET_ERROR)
			{
				GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "IOCPServer::CreateListenSocket(), 리슨 실패");
				return false;
			}
		}	

		HANDLE iocpHandle = CreateIoCompletionPort((HANDLE)_serverListenSocket, _workerIOCPHandle, (DWORD)0, 0);

		if (iocpHandle == NULL || _workerIOCPHandle != iocpHandle)
		{
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "IOCPServer::CreateListenSocket(), CreateIoCompletionPort 실패");
			return false;
		}

		return true;
	}

	void CIOCPServer::DoAccept(LPOVERLAPPED_EX overlappedEx)
	{
		SOCKADDR* localSockAddr = NULL;
		SOCKADDR* remoteSockAddr = NULL;

		int localSockAddrLength = 0;
		int remoteSockAddrLength = 0;

		CConnection* connection = (CConnection*)overlappedEx->_connection;

		if (connection == NULL)
			return;

		connection->DecrementAcceptIOReferenceCount();

		//remote address를 알아낸다.
		GetAcceptExSockaddrs(
			connection->_addressBuffer,
			0,
			sizeof(SOCKADDR_IN) + 16,
			sizeof(SOCKADDR_IN) + 16,
			&localSockAddr,
			&localSockAddrLength,
			&remoteSockAddr,
			&remoteSockAddrLength);

		if (remoteSockAddrLength != 0)
		{
			std::string ip = inet_ntop(AF_INET, &((SOCKADDR_IN*)remoteSockAddr)->sin_addr, connection->_addressBuffer, sizeof(connection->_addressBuffer));

			if (ip.compare("192.168.0.1") == 0)
			{
				connection->SetConnectionIP(LOCAL_IP);
			}
			else
				connection->SetConnectionIP((char*)ip.c_str());
		}
		else
		{
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "IOCPServer::DoAccept(), GetAcceptExSockaddrs 에러");
			return;
		}

		//소켓생성및 IOCP와 바인드
		if (connection->BindIOCP(_workerIOCPHandle) == false)
		{
			CloseConnection(connection);
			return;
		}

		connection->_isConnect = true;

		if (connection->RecvPost(connection->_recvRingBuffer.GetBeginMark(), 0) == false)
		{
			CloseConnection(connection);
			return;
		}

		OnAccept(connection);
	}

	void CIOCPServer::DoConnect(LPOVERLAPPED_EX overlappedEx)
	{
		CConnection* connection = (CConnection*)overlappedEx->_connection;

		if (connection == NULL)
			return;

		connection->_isConnect = true;

		if (connection->RecvPost(connection->_recvRingBuffer.GetBeginMark(), 0) == false)
		{
			CloseConnection(connection);
			return;
		}

		OnConnect(connection);
	}

	void CIOCPServer::DoRecv(LPOVERLAPPED_EX overlappedEx, DWORD size)
	{
		CConnection* connection = (CConnection*)overlappedEx->_connection;

		if (connection == NULL)
			return;

		connection->DecrementRecvIOReferenceCount();

		int msgSize = 0, remain = 0;
		char* current = NULL;
		char* next = NULL;

		remain = overlappedEx->_remain;
		overlappedEx->_wsaBuf.buf = overlappedEx->_socketMsg;
		overlappedEx->_remain += size;

		if (overlappedEx->_remain >= PACKET_HEADER_SIZE_LENGTH)
		{
			CopyMemory(&msgSize, &(overlappedEx->_wsaBuf.buf[0]), PACKET_HEADER_SIZE_LENGTH);
		}
		else
			msgSize = 0;
	
		//종료 처리
		if (msgSize == 0)
		{
			if (CloseConnection(connection) == true)
			{
				connection->CloseConnection(true);
				return;
			};
		}

		//잘못된 패킷 처리..
		else if (msgSize <= 0 || msgSize > connection->_recvRingBuffer.GetBufferSize())
		{
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "잘못된 패킷....");
			CloseConnection(connection);
			return;
		}

		overlappedEx->_totalByte = msgSize;

		if (overlappedEx->_remain < (DWORD)msgSize)
		{
			remain = overlappedEx->_remain;
			next = overlappedEx->_wsaBuf.buf;
		}
		else //하나 이상의 패킷의 데이터를 모두 받으면..
		{
			current = &overlappedEx->_wsaBuf.buf[0];
			int currentSize = msgSize;

			remain = overlappedEx->_remain;

			if (ProcessPacket(connection, current, currentSize) == false)
				return;


			remain -= currentSize;
			next = current + currentSize;

			while (true)
			{
				if (remain >= PACKET_HEADER_SIZE_LENGTH)
				{
					CopyMemory(&msgSize, next, PACKET_HEADER_SIZE_LENGTH);
					currentSize = msgSize;

					//arrive wrong packet..
					if (msgSize <= 0 || msgSize > connection->_recvRingBuffer.GetBufferSize())
					{
						GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "잘못된 패킷....");
						CloseConnection(connection);
						return;
					}
					overlappedEx->_totalByte = currentSize;
					if (remain >= currentSize)
					{
						if (ProcessPacket(connection, next, currentSize) == false)
							return;
						remain -= currentSize;
						next += currentSize;
					}
					else
						break;
				}
				else
					break;
			}
		}
		connection->RecvPost(next, remain);
	}

	void CIOCPServer::DoSend(LPOVERLAPPED_EX overlappedEx, DWORD size)
	{
		CConnection* connection = (CConnection*)overlappedEx->_connection;

		if (connection == NULL)
			return;

		connection->DecrementSendIOReferenceCount();

		overlappedEx->_remain += size;

		if ((DWORD)overlappedEx->_totalByte > overlappedEx->_remain)
		{
			DWORD flag = 0;
			DWORD sendNumBytes = 0;

			overlappedEx->_wsaBuf.buf += size;
			overlappedEx->_wsaBuf.len += size;

			memset(&overlappedEx->_overlapped, 0, sizeof(OVERLAPPED));
			connection->DecrementSendIOReferenceCount();

			int ret = WSASend(connection->GetSocket(),
				&(overlappedEx->_wsaBuf),
				1,
				&sendNumBytes,
				flag,
				&(overlappedEx)->_overlapped,
				NULL);

			if (ret == SOCKET_ERROR && (WSAGetLastError() != ERROR_IO_PENDING))
			{
				connection->DecrementSendIOReferenceCount();
				GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "IOCPServer::DoSend(), WSASend() error");

				CloseConnection(connection);
				return;
			}
		}
		else
		{
			connection->_sendRingBuffer.ReleaseBuffer(overlappedEx->_totalByte);
			InterlockedExchange((LPLONG)&connection->_isSend, TRUE);
			connection->SendPost(0);
		}
	}

	void CIOCPServer::WorkerThread()
	{
		BOOL success = FALSE;
		LPOVERLAPPED overlapped = NULL;

		CConnection* connection = NULL;
		LPOVERLAPPED_EX	overlappedEx = NULL;
		DWORD size = 0;


		//GlobalLogHelper->Normal(LOG_INFO_TYPE_INFO_NORMAL, "워커 스레드 시작");
		while (_workerThreadRun)
		{
			size = 0;
			overlapped = NULL;
			success = GetQueuedCompletionStatus(
				_workerIOCPHandle,
				&size,
				(PULONG_PTR)&connection,
				&overlapped,
				INFINITE);


			if (success == FALSE)
			{
				//GlobalLogHelper->Normal(LOG_INFO_TYPE_INFO_NORMAL, "워커 스레드 작업 시작");
				if (overlapped == NULL && connection == NULL)
				{
					GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "IOCPServer::WorkerThread(), GetQueuedCompletionStatus() error");
					continue;
				}

				LPOVERLAPPED_EX overlappedEx = (LPOVERLAPPED_EX)overlapped;
				connection = (CConnection*)overlappedEx->_connection;


				if (connection == NULL)
					continue;

				if (overlappedEx->_operationType == OP_ACCEPT)
					connection->DecrementAcceptIOReferenceCount();
				else if (overlappedEx->_operationType == OP_RECV)
					connection->DecrementRecvIOReferenceCount();
				else if (overlappedEx->_operationType == OP_SEND)
					connection->DecrementSendIOReferenceCount();

				CloseConnection(connection);
				continue;
			}

			LPOVERLAPPED_EX overlappedEx = (LPOVERLAPPED_EX)overlapped;

			if (overlappedEx == NULL)
				continue;

			switch (overlappedEx->_operationType)
			{
			case OP_ACCEPT:
			{
				DoAccept(overlappedEx);
			}
			break;
			case OP_RECV:
			{
				DoRecv(overlappedEx, size);
			}
			break; 
			case OP_SEND:
			{
				DoSend(overlappedEx, size);
			}
			break;
			case OP_CONNECT:
			{
				DoConnect(overlappedEx);
			}
			break;
			}
			Sleep(10);
		}	
	}

	void CIOCPServer::ProcessThread()
	{
		BOOL success = FALSE;
		int ret = 0;
		PROCESS_PACKET* processPacket = NULL;
		LPOVERLAPPED overlapped = NULL;
		CConnection* connection = NULL;
		LPOVERLAPPED_EX overlappedEx = NULL;
		DWORD size = 0;

		while (_processThreadRun)
		{
			size = 0;
			success = GetQueuedCompletionStatus(
				_processIOCPHandle,
				&size,
				(PULONG_PTR)&connection,
				(LPOVERLAPPED*)&processPacket,
				INFINITE);

			//쓰레드 종료
			if (success == TRUE && connection == NULL)
				break;

			switch (processPacket->_operationType)
			{
			case OP_CLOSE:
			{
				connection->CloseConnection(true);
			}
			break;
			case OP_RECV_PACKET:
			{
				if (processPacket->_lParam == NULL)
					continue;

				OnRecv(connection, size, (char*)processPacket->_lParam);
				connection->_recvRingBuffer.ReleaseBuffer(size);
			}
			break;
			case OP_SYSTEM:
			{
				OnSystemMsg(connection, (DWORD)processPacket->_lParam, processPacket->_wParam);
			}
			break;
			}

			ClearProcessPacket(processPacket);
			Sleep(10);
		}		
	}

	bool CIOCPServer::ProcessPacket(CConnection* connection, char* current, DWORD currentSize)
	{
		int useBuffserSize = connection->_recvRingBuffer.GetUsedBufferSize();

		if (!OnRecvImmediately(connection, currentSize, current))
		{
			PROCESS_PACKET* processPacket = GetProcessPacket(OP_RECV_PACKET, (LPARAM)current, NULL);

			if (processPacket == NULL)
				return false;

			if (PostQueuedCompletionStatus(_processIOCPHandle, currentSize, (ULONG_PTR)connection, (LPOVERLAPPED)processPacket))
			{
				ClearProcessPacket(processPacket);

				DWORD error = GetLastError();
				GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "IOCPServer::ProcessPacket(), PostQueuedCompletionStatus 에러");
			}
			return false;
		}
		else
		{
			connection->_recvRingBuffer.ReleaseBuffer(currentSize);
		}
		return true;
	}

	PROCESS_PACKET* CIOCPServer::GetProcessPacket(OperationType operationType, LPARAM lParam, WPARAM wParam)
	{
		DWORD processPacketCount = InterlockedDecrement((LPLONG)&_processPacketCount);

		if ((int)processPacketCount == (int)-1)
		{
			InterlockedIncrement((LPLONG)&processPacketCount);
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_HIGH, "IOCPServer::GetProcessPacket(), packet empty");
			return NULL;
		}

		PROCESS_PACKET* processPacket = &_processPacket[_processPacketCount];

		processPacket->_operationType = operationType;
		processPacket->_lParam = lParam;
		processPacket->_wParam = wParam;

		return processPacket;
	}

	void CIOCPServer::ClearProcessPacket(PROCESS_PACKET* processPacket)
	{
		processPacket->Initialize();
		InterlockedIncrement((LPLONG)&_processPacketCount);
	}



}


