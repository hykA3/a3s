//
// file : IOCPServer.cpp
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.07
//

#pragma once
#include "Library.h"

#define LOCAL_IP		"127.0.0.1"
#define EXTERNAL_IP		"220.69.74.115"

namespace aria
{
#define GLOBAL_IOCP_SERVER	aria::CIOCPServer::GetInstance()

	class CConnection;
	class CIOCPServer
	{
	protected:
		static CIOCPServer*	_instance;

	public:
		static CIOCPServer* GetInstance() { return CIOCPServer::_instance; }

		CIOCPServer();
		virtual	~CIOCPServer();

		virtual bool StartServer(InitConfig& config, bool socketType = true);
		virtual bool StopServer();


		bool CloseConnection(CConnection* connection);


		SOCKET GetListenSocket() { return _serverListenSocket; }
		//initialize();
		bool InitializeSocket();
		bool CreateWorkerIOCP();
		bool CreateProcessIOCP();
		bool CreateWorkerThread();
		bool CreateProcessThread();
		bool CreateListenSocket(bool socketType);


		//접속을 허가 한다.
		void DoAccept(LPOVERLAPPED_EX overlappedEx);
		void DoConnect(LPOVERLAPPED_EX overlappedEx);
		void DoRecv(LPOVERLAPPED_EX overlappedEx, DWORD size);
		void DoSend(LPOVERLAPPED_EX overlappedEx, DWORD size);

		virtual bool OnConnect(CConnection* connection) = 0;

		virtual bool OnAccept(CConnection* connection) = 0;
		//순서성 있는 패킷들이 처리되어야 할때...
		virtual bool OnRecv(CConnection* connection, DWORD size, char* recvedMsg) = 0;
		//순서성 없는 패킷들이 처리될때..
		virtual bool OnRecvImmediately(CConnection* connection, DWORD size, char* recvedMsg) = 0;

		virtual	void OnClose(CConnection* connection) = 0;
		//서버에서 ProcessThread가 아닌 다른 쓰레드에서 발생시킨 
		//메시지가 순서 성있게 처리되야 한다면 이 함수를 사용.
		virtual bool OnSystemMsg(CConnection* connection, DWORD msgType, LPARAM lParam) = 0;


		//thread function
		void WorkerThread();
		void ProcessThread();

		//packet 관련
		bool ProcessPacket(CConnection* connection, char* current, DWORD currentSize);
		PROCESS_PACKET* GetProcessPacket(OperationType operationType, LPARAM lParam, WPARAM wParam);
		void ClearProcessPacket(PROCESS_PACKET* processPacket);

		DWORD GeneratePrivateKey() { return ++_privateKey; }
	protected:
		DWORD _privateKey;

		unsigned short				_serverPort;
		std::string					_serverIP;

		DEFINE_GET_ACCESS_WITH_VAR(bool, ServerRun, _isServerRun);

		SOCKET						_serverListenSocket;

		//thread 관련..
		bool						_workerThreadRun;
		bool						_processThreadRun;

		unsigned short				_workerThreadCount;
		unsigned short				_processThreadCount;

		HANDLE						_workerIOCPHandle;
		HANDLE						_processIOCPHandle;

		HANDLE						_workerThreadHandle[MAX_WORKER_THREAD];
		HANDLE						_processThreadHandle[MAX_PROCESS_THREAD];

		PROCESS_PACKET*				_processPacket;
		DWORD						_processPacketCount; //최대 처리 패킷의 개수.

	};
}


