//
// file : RingBuffer.cpp
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.03
//

#include <iostream>
#include "RingBuffer.h"
#include "CriticalSection.h"

namespace aria
{
	CRingBuffer::CRingBuffer()
	{
		_ringBuffer = NULL;

		_beginMark = NULL;
		_currentMark = NULL;
		_endMark = NULL;

		_gettedBufferMark = NULL;
		_lastMoveMark;

		_totalBufferSize = 0;
		_usedBufferSize = 0;
		_allUsedBufferSize = 0;
	}


	CRingBuffer::~CRingBuffer()
	{
		SAFE_DELETE_ARRAY(_beginMark);
	}


	bool CRingBuffer::Create(int bufferSize /*= MAX_RINGBUFFER_SIZE*/)
	{
		ScopedCriticalSectionLock lock(_cs);
		if (NULL != _beginMark)
		{
			delete[] _beginMark;
		}
		_beginMark = new char[bufferSize];

		if (NULL == _beginMark)
		{
			return false;
		}

		ZeroMemory(_beginMark, bufferSize);
		_endMark = _beginMark + bufferSize - 1;
		_totalBufferSize = bufferSize;

		Initialize();
		return true;
	}

	bool CRingBuffer::Initialize()
	{
		ScopedCriticalSectionLock lock(_cs);

		_usedBufferSize = 0;
		_allUsedBufferSize = 0;

		_currentMark = _beginMark;
		_gettedBufferMark = _beginMark;
		_lastMoveMark = _endMark;

		return true;
	}

	char* CRingBuffer::ForwardMark(int forwardLength)
	{
		char* preCurrentMark = NULL;
		ScopedCriticalSectionLock lock(_cs);
		{
			//링버퍼 오버플로 체크
			if (_usedBufferSize + forwardLength > _totalBufferSize)
			{
				return NULL;
			}
				
			if ((_endMark - _currentMark) >= forwardLength)
			{
				preCurrentMark = _currentMark;
				_currentMark += forwardLength;
			}
			else
			{
				//순환 되기 전 마지막 좌표를 저장
				_lastMoveMark = _currentMark;
				_currentMark = _beginMark + forwardLength;
				preCurrentMark = _beginMark;
			}

		}
		return preCurrentMark;
	}


	char* CRingBuffer::ForwardMark(int forwardLength, int nextLength, DWORD remainLength)
	{
		ScopedCriticalSectionLock lock(_cs);

		//링버퍼 오버플로 체크
		if (_usedBufferSize + forwardLength + nextLength > _totalBufferSize)
			return NULL;

		if ((_endMark - _currentMark) > (nextLength + forwardLength))
		{
			_currentMark += forwardLength;
		}
		else
		{
			//순환되기 전 마지막 좌표를 저장
			_lastMoveMark = _currentMark;

			CopyMemory(_beginMark, _currentMark - (remainLength - forwardLength), remainLength);
			_currentMark = _beginMark + remainLength;
		}

		return _currentMark;
	}


	void CRingBuffer::ReleaseBuffer(int releaseSize)
	{
		ScopedCriticalSectionLock lock(_cs);

		_usedBufferSize -= releaseSize;
	}

	char* CRingBuffer::GetBuffer(int readSize, int* outputReadSize)
	{
		char* ret = NULL;

		ScopedCriticalSectionLock lock(_cs);
		//마지막까지 다 읽었다면 그 읽어들일 버퍼의 포인터는 맨앞으로 옮긴다.
		if (_lastMoveMark == _gettedBufferMark)
		{
			_gettedBufferMark = _beginMark;
			_lastMoveMark = _endMark;
		}

		//현재 버퍼에 있는 size가 읽어들일 size보다 크다면
		if (_usedBufferSize > readSize)
		{
			//링버퍼의 끝인지 판단.
			if ((_lastMoveMark - _gettedBufferMark) >= readSize)
			{
				*outputReadSize = readSize;
				ret = _gettedBufferMark;
				_gettedBufferMark += readSize;
			}
			else
			{
				*outputReadSize = (int)(_lastMoveMark - _gettedBufferMark);
				ret = _gettedBufferMark;
				_gettedBufferMark += *outputReadSize;
			}
		}
		else if (_usedBufferSize > 0)
		{
			//링버퍼의 끝인지 판단.
			if ((_lastMoveMark - _gettedBufferMark) >= _usedBufferSize)
			{
				*outputReadSize = _usedBufferSize;
				ret = _gettedBufferMark;
				_gettedBufferMark += _usedBufferSize;
			}
			else
			{
				*outputReadSize = (int)(_lastMoveMark - _gettedBufferMark);
				ret = _gettedBufferMark;
				_gettedBufferMark += *outputReadSize;
			}
		}

		return ret;
	}

	void CRingBuffer::SetUsedBufferSize(int usedBufferSize)
	{
		ScopedCriticalSectionLock lock(_cs);

		_usedBufferSize += usedBufferSize;
		_allUsedBufferSize += usedBufferSize;
	}

}

