﻿/** @file dgutil.h
 *
 *  @brief  utility classes for delegate implementation
 *
 *  @author JaeWook Choi
 *  @version 1.22
 *
 * This software is provided "as is" without express or implied warranty, and with
 * no claim as to its suitability for any purpose.
 *
**/
#if !defined(__DGUTIL_H__INCLUDED__)
#define __DGUTIL_H__INCLUDED__

namespace fd
{

namespace util
{

//
// for FD_STATIC_ASSERT()
//
// restricting constant template arguments
// ( http://cpptips.hyperformix.com/cpptips/restr_templ_args )
//
template<bool> struct t_assert;
template<> struct t_assert<true>  { typedef bool assertion; };
template<> struct t_assert<false> { };

// ================================================================================

//
// A gentle introduction to Template Metaprogramming with C++ By moliate
// ( http://www.codeproject.com/cpp/crc_meta.asp )
//

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

template<bool, typename Then, typename Else> struct If;
template<typename Then, typename Else> struct If<true, Then, Else> { typedef Then Result; };
template<typename Then, typename Else> struct If<false, Then, Else> { typedef Else Result; };

#else // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

template <bool t_condition, typename Then, typename Else>
struct If
{
  template<bool> struct selector;
  template<> struct selector<true> { typedef Then Result; };
  template<> struct selector<false> { typedef Else Result; };

  typedef typename selector<t_condition>::Result Result;

};  // template <bool t_condition, typename Then, typename Else> If

#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

// ================================================================================

template<typename T, typename U>
struct Is_pointer
{
  static U testee_;
  static int tester_(...);        // false condition
  static char tester_(T *);       // true condition, typename 'U' matches with typename 'T *' or 'T-derived *'

  enum { value = (sizeof(char) == sizeof(tester_(testee_))) };

};  // template<typename T, typename U> struct Is_pointer

template<typename T, typename U, typename Then, typename Else>
struct If_pointer
{
  // typename 'U' matches with typename 'T *', 'T const *', 'T-derived *' or 'T-derived const *'
  enum { Condition = Is_pointer<T, U>::value || Is_pointer<T const, U>::value };

  typedef typename If< Condition, Then, Else >::Result Result;

};  // template<typename T, typename U, typename Then, typename Else> struct If_pointer

// ================================================================================

template<typename T, typename U>
struct Is_reference
{
  static U testee_;
  static int tester_(...);        // false condition
  static char tester_(T &);       // true condition, typename 'U' matches with typename 'T &' or 'T-derived &'

  enum { value = (sizeof(char) == sizeof(tester_(testee_))) };

};  // template<typename T, typename U> struct Is_reference

template<typename T, typename U, typename Then, typename Else>
struct If_reference
{
  // typename 'U' matches with typename 'T &', 'T const &', 'T-derived &' or 'T-derived const &'
  enum { Condition = Is_reference<T, U>::value || Is_reference<T const, U>::value };

  typedef typename If< Condition, Then, Else >::Result Result;

};  // template<typename T, typename U, typename Then, typename Else> struct If_reference

// ================================================================================

template<typename T, typename U>
struct Is_pointer_or_reference
{
  // value is true, if typename 'U' matches with T' or 'T-derived' 's pointer or reference
  enum { value = Is_pointer<T, U>::value || Is_reference<T, U>::value };

};  // template<typename T, typename U> struct Is_pointer_or_reference

// ================================================================================

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

template<typename T, typename U> struct Is_same_type { enum { value = false }; };
template<typename T> struct Is_same_type<T, T> { enum { value = true }; };

#else // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

template<typename T, typename U>
struct Is_same_type
{
  template<typename> struct selector { enum { value = false }; };
  template<> struct selector<T> { enum { value = true }; };

  enum { value = selector<U>::value };

};  // template<typename T, typename U> struct Is_same_type

#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

// ================================================================================

template<typename T> struct Is_integral
{
  static T testee_;

  // false conditions
  static int tester_(...);
  static int tester_(void);
  static int tester_(float);
  static int tester_(double);
  static int tester_(long double);

  // true conditions
  static unsigned char tester_(signed char);
  static unsigned char tester_(signed short);
  static unsigned char tester_(signed int);
  static unsigned char tester_(signed long);
  static unsigned char tester_(unsigned char);
  static unsigned char tester_(unsigned short);
  static unsigned char tester_(unsigned int);
  static unsigned char tester_(unsigned long);

  enum { value = (sizeof(unsigned char) == sizeof(tester_(testee_))) };

};  // template<typename T> struct Is_integral

// ================================================================================

template<typename T> struct Is_floating
{
  static T testee_;

  // false conditions
  static int tester_(...);
  static int tester_(void);
  static int tester_(signed char);
  static int tester_(signed short);
  static int tester_(signed int);
  static int tester_(signed long);
  static int tester_(unsigned char);
  static int tester_(unsigned short);
  static int tester_(unsigned int);
  static int tester_(unsigned long);

  // true conditions
  static unsigned char tester_(float);
  static unsigned char tester_(double);
  static unsigned char tester_(long double);

  enum { value = (sizeof(unsigned char) == sizeof(tester_(testee_))) };

};  // template<typename T> struct Is_floating

// ================================================================================

template<typename T1, typename T2>
struct tied
{
  T1 & t1_; T2 & t2_;
  tied(T1 & t1, T2 & t2) : t1_(t1), t2_(t2) { }
  void operator = (std::pair<T1, T2> & pr) { t1_ = pr.first; t2_ = pr.second; }

};  // template<typename T1, typename T2> struct tied

template<typename T1, typename T2> inline
tied<T1, T2> tie(T1 & t1, T2 & t2)
{
  return tied<T1, T2>(t1, t2);
}

// ================================================================================

} // namespace util

} // namespace fd

#endif  // #if !defined(__DGUTIL_H__INCLUDED__)
