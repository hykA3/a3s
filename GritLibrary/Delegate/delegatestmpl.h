﻿/** @file delegatestmpl.h
 *
 *  @brief  multi-casting version of delegate (fd::delegates and fd::delegatesN)
 *
 *  @author JaeWook Choi
 *  @version 1.22
 *
 * This software is provided "as is" without express or implied warranty, and with
 * no claim as to its suitability for any purpose.
 *
 **/

// Note: this header is a header template and must NOT have multiple-inclusion
// protection.

#include "delegatestmpl.hpp"

namespace fd
{
  
#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

template<typename R FD_COMMA FD_TPARMS(N), typename Combiner, typename Allocator, size_t t_countof_pvoid, typename DGList>
class FD_JOIN(delegates,N); // forward reference

// ====================================================================================================
//
// ====================================================================================================
//
// Preferred syntax
//
// class delegates<R (T1, T2, ..., Tn), Combiner, Allocator, t_countof_pvoid, DGList>
//
// ====================================================================================================
//
// ====================================================================================================
template<typename R FD_COMMA FD_TPARMS(N), typename Combiner, typename Allocator, size_t t_countof_pvoid, typename DGList>
class delegates<R (FD_TARGS(N)), Combiner, Allocator, t_countof_pvoid, DGList>
  : public detail::safe_bool<delegates<R (FD_TARGS(N)), Combiner, Allocator, t_countof_pvoid, DGList> >
{
  typedef delegates<R (FD_TARGS(N)), Combiner, Allocator, t_countof_pvoid, DGList> thisClass;

  // --------------------------------------------------------------------------------

private:
  // data members

  typedef detail::FD_JOIN(delegatesImpl,N)<R FD_COMMA FD_TARGS(N), Combiner, Allocator, t_countof_pvoid, DGList> implClass;
  implClass impl_;

  typedef typename implClass::clear_type clear_type;

  // --------------------------------------------------------------------------------

public:
  // typedef's
  typedef typename implClass::size_type       size_type;
  typedef typename implClass::result_type     result_type;
  typedef typename implClass::delegate_type   delegate_type;
  typedef typename implClass::combiner_type   combiner_type;
  typedef typename implClass::list_type       list_type;

#if N == 1
  // std::unary_function compatible
  typedef T1 argument_type;
#endif  // #if N == 1

#if N == 2
  // std::binary_function compatible
  typedef T1 first_argument_type;
  typedef T2 second_argument_type;
#endif  // #if N == 2

  // --------------------------------------------------------------------------------

  struct friend__
  {
    template<typename Combiner2> inline
      static implClass & impl_of_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> & friend_delegates)
    {
      return friend_delegates.impl_;
    }

    template<typename Combiner2> inline
      static implClass const & impl_of_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & friend_delegates)
    {
      return friend_delegates.impl_;
    }

  };  // friend__
  friend struct friend__;

  // ====================================================================================================
  // c'tors & d'tor
  // ====================================================================================================
public:
  
#if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  delegates()
    : impl_()
  {
  }

  template<typename Combiner2>
  explicit delegates(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
  uintptr_t hint = 0)
    : impl_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint)
  {
  }

  template<typename Combiner2>
  explicit delegates(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
  uintptr_t hint = 0)
    : impl_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint)
  {
  }

  explicit delegates(clear_type const *)
    : impl_()
  {
  }

  explicit delegates(delegate_type const & dg, uintptr_t hint = 0)
    : impl_(dg, hint)
  {
  }

#else // #if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  delegates()
    : impl_()
  {
  }

  template<typename Combiner2>
  explicit delegates(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
    : impl_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other))
  {
  }

  template<typename Combiner2>
  explicit delegates(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
    : impl_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other))
  {
  }

  explicit delegates(clear_type const *)
    : impl_()
  {
  }

  explicit delegates(delegate_type const & dg)
    : impl_(dg)
  {
  }

#endif  // #if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)


  // ----------------------------------------------------------------------

  template<typename Combiner2>
  inline void swap(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> & other)
  {
    impl_.swap_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }

  template<typename Combiner2>
  inline void swap(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> & other)
  {
    impl_.swap_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }

#if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  inline void clear(uintptr_t hint = 0)
  {
    impl_.clear_(hint);
  }

  inline size_type size(uintptr_t hint = 0) const
  {
    return impl_.size_(hint);
  }

  inline bool empty(uintptr_t hint = 0) const
  {
    return impl_.empty_(hint);
  }

#else // #if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  inline void clear()
  {
    impl_.clear_();
  }

  inline size_type size() const
  {
    return impl_.size_();
  }

  inline bool empty() const
  {
    return impl_.empty_();
  }

#endif  // #if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  // ----------------------------------------------------------------------
  // assignment operators

  template<typename Combiner2>
  inline thisClass & operator = (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    if(&this->impl_ == reinterpret_cast<implClass const *>(&delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other)))
      return *this;

    impl_.assign_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }

  template<typename Combiner2>
    thisClass & operator = (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    if(&this->impl_ == reinterpret_cast<implClass const *>(&FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other)))
      return *this;

    impl_.assign_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }

  inline thisClass & operator = (delegate_type const & dg)
  {
    impl_.assign_(dg);

    return *this;
  }

  inline thisClass & operator = (clear_type const *)
  {
    impl_.reset_();

    return *this;
  }

  // ----------------------------------------------------------------------

  // add delegate into the delegate list
  inline thisClass & operator += (delegate_type const & dg)
  {
    impl_.add_(dg);

    return *this;
  }

  // remove delegate from the delegate list
  inline thisClass & operator -= (delegate_type const & dg)
  {
    impl_.remove_(dg);

    return *this;
  }

  // ----------------------------------------------------------------------

  // add delegate into the delegate list
  inline thisClass operator + (delegate_type const & dg)
  {
    thisClass dgs(*this);

    dgs.impl_.add_(dg);

    return dgs;
  }

  // remove delegate from the delegate list
  inline thisClass operator - (delegate_type const & dg)
  {
    thisClass dgs(*this);

    dgs.impl_.remove_(dg);

    return dgs;
  }

  // ----------------------------------------------------------------------

  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass & operator += (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    impl_.add_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass & operator -= (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    impl_.remove_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }

  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass & operator += (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    impl_.add_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass & operator -= (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    impl_.remove_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }

  // ----------------------------------------------------------------------

  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass operator + (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    thisClass dgs(*this);

    dgs.impl_.add_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return dgs;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass operator - (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    thisClass dgs(*this);

    dgs.impl_.remove_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return dgs;
  }

  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass operator + (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    thisClass dgs(*this);

    dgs.impl_.add_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return dgs;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass operator - (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    thisClass dgs(*this);

    dgs.impl_.remove_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return dgs;
  }

  // ----------------------------------------------------------------------
  // equality comparison

  template<typename Combiner2>
    inline bool operator == (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other) const
  {
    return impl_.equal_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }

  template<typename Combiner2>
    inline bool operator != (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other) const
  {
    return !impl_.equal_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }

  template<typename Combiner2>
    inline bool operator == (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other) const
  {
    return impl_.equal_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }

  template<typename Combiner2>
    inline bool operator != (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other) const
  {
    return !impl_.equal_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }

  // ----------------------------------------------------------------------
  // to allow to compare to 0

  inline bool operator == (clear_type const *) const
  {
    return impl_.empty_();
  }

  inline bool operator != (clear_type const *) const
  {
    return !impl_.empty_();
  }

  // ----------------------------------------------------------------------

  inline result_type operator () (FD_FPARMS(N)) const
  {
    return impl_.invoke_(FD_FARGS(N));
  }

  // ----------------------------------------------------------------------

#if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  // ----------------------------------------------------------------------
  // assignment with hint

  template<typename Combiner2>
  inline thisClass & assign(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
  uintptr_t hint = 0)
  {
    if(&this->impl_ == reinterpret_cast<implClass const *>(&delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other)))
      return *this;

    impl_.assign_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }

  template<typename Combiner2>
    thisClass & assign(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    if(&this->impl_ == reinterpret_cast<implClass const *>(&FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other)))
      return *this;

    impl_.assign_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }

  inline thisClass & assign(delegate_type const & dg, uintptr_t hint = 0)
  {
    impl_.assign_(dg, hint);

    return *this;
  }

  // ----------------------------------------------------------------------

  // add delegate into the delegate list
  inline thisClass & add(delegate_type const & dg, uintptr_t hint = 0)
  {
    impl_.add_(dg, hint);

    return *this;
  }

  // remove delegate from the delegate list
  inline thisClass & remove(delegate_type const & dg, uintptr_t hint = 0)
  {
    impl_.remove_(dg, hint);

    return *this;
  }

  // ----------------------------------------------------------------------

  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass & add(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    impl_.add_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass & remove(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    impl_.remove_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }

  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass & add(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    impl_.add_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass & remove(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    impl_.remove_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }

  // ----------------------------------------------------------------------
  // equality comparison with hint

  template<typename Combiner2>
    inline bool equal(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0) const
  {
    return impl_.equal_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);
  }

  template<typename Combiner2>
    inline bool equal(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0) const
  {
    return impl_.equal_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);
  }

  // ----------------------------------------------------------------------

  // allow to iterate through delegate list (read-only)
  inline std::pair<typename list_type::const_iterator, typename list_type::const_iterator> list(uintptr_t hint = 0) const
  {
    return impl_.list_(hint);
  }

  // ----------------------------------------------------------------------

  inline result_type invoke(FD_FPARMS(N) FD_COMMA uintptr_t hint = 0) const
  {
    return impl_.invoke_(FD_FARGS(N) FD_COMMA hint);
  }

#endif  // #if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

};

#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

// ====================================================================================================
//
// ====================================================================================================
//
// Portable syntax
//
// class delegatesN<R, T1, T2, ..., Tn, Combiner, Allocator, t_countof_pvoid, DGList>
//
// ====================================================================================================
//
// ====================================================================================================
template<
typename R FD_COMMA FD_TPARMS(N),
typename Combiner = DefaultCombiner,
typename Allocator = DefaultAllocator,
size_t t_countof_pvoid = DefaultCountOfVoid,
typename DGList = DefaultDGList
> class FD_JOIN(delegates,N)
  : public detail::safe_bool<FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner, Allocator, t_countof_pvoid, DGList> >
{
  typedef FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner, Allocator, t_countof_pvoid, DGList> thisClass;
  // ====================================================================================================
  // data members
  // ====================================================================================================
private:

  typedef detail::FD_JOIN(delegatesImpl,N)<R FD_COMMA FD_TARGS(N), Combiner, Allocator, t_countof_pvoid, DGList> implClass;
  implClass impl_;

  typedef typename implClass::clear_type clear_type;

public:
  // typedef's
  typedef typename implClass::size_type       size_type;
  typedef typename implClass::result_type     result_type;
  typedef typename implClass::delegate_type   delegate_type;
  typedef typename implClass::combiner_type   combiner_type;
  typedef typename implClass::list_type       list_type;

#if N == 1
  // std::unary_function compatible
  typedef T1 argument_type;
#endif  // #if N == 1

#if N == 2
  // std::binary_function compatible
  typedef T1 first_argument_type;
  typedef T2 second_argument_type;
#endif  // #if N == 2

  struct friend__
  {
    template<typename Combiner2> inline
      static implClass & impl_of_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> & friend_delegates)
    {
      return friend_delegates.impl_;
    }
    template<typename Combiner2> inline
      static implClass const & impl_of_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & friend_delegates)
    {
      return friend_delegates.impl_;
    }

  };  // friend__
  friend struct friend__;

  // ====================================================================================================
  // c'tors & d'tor
  // ====================================================================================================
public:
  
#if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  FD_JOIN(delegates,N)()
    : impl_()
  {
  }

  template<typename Combiner2>
    explicit FD_JOIN(delegates,N)(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
    : impl_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint)
  {
  }

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)
  template<typename Combiner2>
    explicit FD_JOIN(delegates,N)(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
    : impl_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint)
  {
  }
#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

  explicit FD_JOIN(delegates,N)(clear_type const *)
    : impl_()
  {
  }

  explicit FD_JOIN(delegates,N)(delegate_type const & dg, uintptr_t hint = 0)
    : impl_(dg, hint)
  {
  }

#else // #if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  FD_JOIN(delegates,N)()
    : impl_()
  {
  }

  template<typename Combiner2>
    explicit FD_JOIN(delegates,N)(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
    : impl_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other))
  {
  }

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)
  template<typename Combiner2>
    explicit FD_JOIN(delegates,N)(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
    : impl_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other))
  {
  }
#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

  explicit FD_JOIN(delegates,N)(clear_type const *)
    : impl_()
  {
  }

  explicit FD_JOIN(delegates,N)(delegate_type const & dg)
    : impl_(dg)
  {
  }

#endif  // #if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  // ----------------------------------------------------------------------

  template<typename Combiner2>
    inline void swap(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> & other)
  {
    impl_.swap_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)
  template<typename Combiner2>
    inline void swap(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> & other)
  {
    impl_.swap_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }
#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

#if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  inline void clear(uintptr_t hint = 0)
  {
    impl_.clear_(hint);
  }

  inline size_type size(uintptr_t hint = 0) const
  {
    return impl_.size_(hint);
  }

  inline bool empty(uintptr_t hint = 0) const
  {
    return impl_.empty_(hint);
  }

#else // #if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  inline void clear()
  {
    impl_.clear_();
  }

  inline size_type size() const
  {
    return impl_.size_();
  }

  inline bool empty() const
  {
    return impl_.empty_();
  }

#endif  // #if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  // ----------------------------------------------------------------------
  // assignment operators

  template<typename Combiner2>
    thisClass & operator = (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    if(&this->impl_ == reinterpret_cast<implClass const *>(&FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other)))
      return *this;

    impl_.assign_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)
  template<typename Combiner2>
    inline thisClass & operator = (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    if(&this->impl_ == reinterpret_cast<implClass const *>(&delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other)))
      return *this;

    impl_.assign_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }
#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

  inline thisClass & operator = (delegate_type const & dg)
  {
    impl_.assign_(dg);

    return *this;
  }

  inline thisClass & operator = (clear_type const *)
  {
    impl_.reset_();

    return *this;
  }

  // ----------------------------------------------------------------------

  // add delegate into the delegate list
  inline thisClass & operator += (delegate_type const & dg)
  {
    impl_.add_(dg);

    return *this;
  }

  // remove delegate from the delegate list
  inline thisClass & operator -= (delegate_type const & dg)
  {
    impl_.remove_(dg);

    return *this;
  }

  // ----------------------------------------------------------------------

  // add delegate into the delegate list
  inline thisClass operator + (delegate_type const & dg)
  {
    thisClass dgs(*this);

    dgs.impl_.add_(dg);

    return dgs;
  }

  // remove delegate from the delegate list
  inline thisClass operator - (delegate_type const & dg)
  {
    thisClass dgs(*this);

    dgs.impl_.remove_(dg);

    return dgs;
  }

  // ----------------------------------------------------------------------

  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass & operator += (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    impl_.add_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass & operator -= (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    impl_.remove_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)
  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass & operator += (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    impl_.add_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass & operator -= (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    impl_.remove_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return *this;
  }
#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

  // ----------------------------------------------------------------------

  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass operator + (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    thisClass dgs(*this);

    dgs.impl_.add_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return dgs;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass operator - (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    thisClass dgs(*this);

    dgs.impl_.remove_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return dgs;
  }

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)
  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass operator + (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    thisClass dgs(*this);

    dgs.impl_.add_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return dgs;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass operator - (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other)
  {
    thisClass dgs(*this);

    impl_.remove_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));

    return dgs;
  }
#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

  // ----------------------------------------------------------------------
  // equality comparison

  template<typename Combiner2>
    inline bool operator == (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other) const
  {
    return impl_.equal_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }

  template<typename Combiner2>
    inline bool operator != (FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other) const
  {
    return !impl_.equal_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)
  template<typename Combiner2>
    inline bool operator == (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other) const
  {
    return impl_.equal_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }

  template<typename Combiner2>
    inline bool operator != (delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other) const
  {
    return !impl_.equal_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other));
  }
#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

  // ----------------------------------------------------------------------

  // to allow to compare to 0
  inline bool operator == (clear_type const *) const
  {
    return impl_.empty_();
  }

  inline bool operator != (clear_type const *) const
  {
    return !impl_.empty_();
  }

  // ----------------------------------------------------------------------

  inline result_type operator () (FD_FPARMS(N)) const
  {
    return impl_.invoke_(FD_FARGS(N));
  }

  // ----------------------------------------------------------------------

#if !defined(FD_DELEGATES_DISABLE_MEM_FN_HINT)

  // ----------------------------------------------------------------------
  // add/remove delegate with hint

  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass add(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    impl_.add_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass remove(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    impl_.remove_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)
  // add delegates into the delegate list
  template<typename Combiner2>
    inline thisClass & add(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    impl_.add_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }

  // remove delegates from the delegate list
  template<typename Combiner2>
    inline thisClass & remove(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    impl_.remove_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }
#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)


  // ----------------------------------------------------------------------
  // assignment with hint

  template<typename Combiner2>
    thisClass & assign(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    if(&this->impl_ == reinterpret_cast<implClass const *>(&FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other)))
      return *this;

    impl_.assign_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)
  template<typename Combiner2>
    inline thisClass & assign(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    if(&this->impl_ == reinterpret_cast<implClass const *>(&delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other)))
      return *this;

    impl_.assign_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);

    return *this;
  }
#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

  inline thisClass & assign(delegate_type const & dg, uintptr_t hint = 0)
  {
    impl_.assign_(dg, hint);

    return *this;
  }

  // ----------------------------------------------------------------------

  // add delegate into the delegate list
  inline thisClass & add(delegate_type const & dg, uintptr_t hint = 0)
  {
    impl_.add_(dg, hint);

    return *this;
  }

  // remove delegate from the delegate list
  inline thisClass & remove(delegate_type const & dg, uintptr_t hint = 0)
  {
    impl_.remove_(dg, hint);

    return *this;
  }

  // ----------------------------------------------------------------------
  // equality comparison with hint

  template<typename Combiner2>
    inline bool equal(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0) const
  {
    return impl_.equal_(FD_JOIN(delegates,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);
  }

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)
  template<typename Combiner2>
    inline bool equal(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0) const
  {
    return impl_.equal_(delegates<R (FD_TARGS(N)), Combiner2, Allocator, t_countof_pvoid, DGList>::friend__::impl_of_(other), hint);
  }
#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

  // ----------------------------------------------------------------------

  // allow to iterate through delegate list (read-only)
  inline std::pair<typename list_type::const_iterator, typename list_type::const_iterator> list(uintptr_t hint = 0) const
  {
    return impl_.list_(hint);
  }

  // ----------------------------------------------------------------------

  inline result_type invoke(FD_FPARMS(N) FD_COMMA uintptr_t hint = 0) const
  {
    return impl_.invoke_(FD_FARGS(N) FD_COMMA hint);
  }

  // ----------------------------------------------------------------------

#endif

};

} // namespace fd
