/** @file combiner.hpp
 *
 *  @brief  delegates' return values can be returned back to the caller of the delegates
 *          through a combiner. The combiner is a mechanism that can take the results of calling delegates
 *          (there many be no results or a hundred; we don't know until the program runs) and coalesces them
 *          into a single result to be returned to the caller.
 *          The single result is often a simple function of the results of the delegate calls:
 *
 *            the result of the last delegate call, (last_value<T>)
 *            the maximum value returned by any slot (maximum<T>), or
 *            a container of all of the results (aggregate_values<T>)
 *
 *         are some possibilities.
 *
 *  @author JaeWook Choi
 *  @version 1.22
 *
 *  the following code are excerpted from boost.signal documentation
 *  (http://boost.org/doc/html/signals/tutorial.html#id2731908)
 *
 *
**/
#if !defined(__COMBINER_HPP__INCLUDED__)
#define __COMBINER_HPP__INCLUDED__

namespace fd
{

namespace util
{

// --------------------------------------------------------------------------------

template<typename T>
struct last_value
{
  typedef T result_type;

  template<typename InputIterator>
    T operator()(InputIterator first, InputIterator last) const
  {
    // If there are no slots to call, just return the
    // default-constructed value
    if (first == last)
      return T();

    return *--last;
  }

};  // template<typename T> struct last_value

// --------------------------------------------------------------------------------

template<typename T>
struct maximum
{
  typedef T result_type;

  template<typename InputIterator>
    T operator()(InputIterator first, InputIterator last) const
  {
    // If there are no slots to call, just return the
    // default-constructed value
    if (first == last)
      return T();

    T max_value = *first++;
    while (first != last) {
      if (max_value < *first)
        max_value = *first;
      ++first;
    }

    return max_value;
  }

};  // template<typename T> struct maximum

// --------------------------------------------------------------------------------

template<typename Container>
struct aggregate_values
{
  typedef Container result_type;

  template<typename InputIterator>
    Container operator()(InputIterator first, InputIterator last) const
  {
    return Container(first, last);
  }

};  // template<typename Container> struct aggregate_values

} // namespace util

} // namespace fd

#endif  // #if !defined(__COMBINER_HPP__INCLUDED__)
