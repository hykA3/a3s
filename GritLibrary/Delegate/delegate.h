﻿/** @file delegate.h
 *
 *  @brief  delegate class
 *
 *  @author JaeWook Choi
 *  @version 1.22
 *
 * This software is provided "as is" without express or implied warranty, and with
 * no claim as to its suitability for any purpose.
 *
 */

// ================================================================================
// References
// ================================================================================
//
// 1. CALLBACKS IN C++ USING TEMPLATE FUNCTORS by Rich Hickey
// ( http://www.tutok.sk/fastgl/callback.html )
// - summarizes existing callback methods and their weaknesses then describes a flexible, powerful and
//   easy-to-use callback technique based on template functors. ('1994)
//
// 2. Callbacks in C++
// ( http://bpeers.com/articles/callback/ )
// - The article based on Rich Hickey's article to illustrate the concept and techniques used to implement callbacks
//
// 3. Member Function Pointers and the Fastest Possible C++ Delegates by Don Clugston
// ( http://codeproject.com/cpp/delegate.asp )
// - A comprehensive tutorial on member function pointers, and an implementation of
//   delegates that generates only two ASM opcodes!
//
// 4. The Impossibly Fast C++ Delegates by Sergey Ryazanov
// ( http://www.codeproject.com/cpp/ImpossiblyFastCppDelegate.asp )
// - A implementation of a delegate library which can work faster than "the Fastest
//   Possible C++ Delegates" and is completely compatible with the C++ Standard.
//
// 5. Yet Another Generalized Functors Implementation in C++ By Aleksei Trunov
// ( http://www.codeproject.com/cpp/genfunctors.asp )
// - An article on generalized functors implementation in C++. Generalized functor requirements,
//   existing implementation problems and disadvantages are considered. Several new ideas and problem
//   solutions together with the compete implementation are suggested.
//
// ================================================================================

#if !defined(__DELEGATE_H__INCLUDED__)
#define __DELEGATE_H__INCLUDED__

#if !defined(FD_EXTRACT_SIMPLIFIED)

#ifdef __APPLE__
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif
#include <algorithm>
#include <string>
#include <stdexcept>
#endif  // #if !defined(FD_EXTRACT_SIMPLIFIED)

// --------------------------------------------------
#if !defined(FD_EXTRACT_SIMPLIFIED)
#include "config.hpp"
#else
#include "..\..\..\include\config.hpp"
#endif
// --------------------------------------------------

// ================================================================================

#if defined(_MSC_VER)

#pragma warning(disable:4522) // warning C4522: '' : multiple assignment operators specified

#if _MSC_VER >= 1300
#define FD_MS_VC
#else // #if _MSC_VER >= 1300
#define FD_MS_VC6
#pragma warning(disable:4786) // identifier was truncated to '255' characters in the debug information
#pragma warning(disable:4503) // decorated name length exceeded, name was truncated
#endif  // #if _MSC_VER >= 1300

#endif  // #if defined(_MSC_VER)

// ================================================================================

#if defined(__GNUC__)
#define FD_GNUC
#endif  // #if defined(__GNUC__)

// ================================================================================

// ================================================================================
// Calling conventions
// ================================================================================
#if defined(FD_MS_VC) || defined(FD_MS_VC6)
//
// http://blogs.msdn.com/oldnewthing/archive/2004/01/02/47184.aspx
// http://blogs.msdn.com/oldnewthing/archive/2004/01/08/48616.aspx
// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/vccore98/html/_core_results_of_calling_example.asp
//
#define FD_STDCALL_   __stdcall
#define FD_FASTCALL_  __fastcall
#define FD_CDECL_     __cdecl
#define FD_PASCAL_

#undef FD_FN_ENABLE_PASCAL

#endif  // #if defined(FD_MS_VC) || defined(FD_MS_VC6)

#if defined(FD_GNUC)
//
// todo: I couldn't figure out how to use calling convention for G++ at the moment
//
// http://gcc.gnu.org/onlinedocs/gcc/Function-Attributes.html#Function-Attributes
//

//#define FD_STDCALL_   __attribute__ ((stdcall))
//#define FD_FASTCALL_  __attribute__ ((fastcall))
//#define FD_CDECL_     __attribute__ ((cdecl))
//#define FD_PASCAL_

#define FD_STDCALL_
#define FD_FASTCALL_
#define FD_CDECL_
#define FD_PASCAL_

#undef FD_MEM_FN_ENABLE_STDCALL
#undef FD_MEM_FN_ENABLE_FASTCALL
#undef FD_MEM_FN_ENABLE_CDECL
#undef FD_FN_ENABLE_STDCALL
#undef FD_FN_ENABLE_FASTCALL
#undef FD_FN_ENABLE_PASCAL

#endif  // #if defined(FD_GNUC)

// ================================================================================
// Platform specific workarounds
// ================================================================================

// VC71+
#if defined(FD_MS_VC)
#endif  // #if defined(FD_MS_VC)

// VC6
#if defined(FD_MS_VC6)
#define FD_NEED_SHADOW_TEMPLATE
#define FD_NO_VOID_RETURN
#define FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION
#define FD_CANT_REBIND_TYPE
#endif  // #if defined(FD_MS_VC6)

// DEV-C++ 4.9.9.2 (Mingw/gcc 3.4.2))
#if defined(FD_GNUC)
#define FD_NO_FULL_SPECIALIZATION_IN_NESTED_CLASS
#define FD_CANT_FUNCTOR_TYPE_CHECK_RELAXATION
#endif  // #if defined(FD_GNUC)

#if defined(FD_NO_FULL_SPECIALIZATION_IN_NESTED_CLASS)
//
// g++: Full VS Partial Specialization of Nested Templates
// ( http://lists.debian.org/debian-gcc/2004/09/msg00015.html )
//
#define FD_NESTED_PARTIAL_TPARM_MAIN(N)     , typename dummy##N = bool
#define FD_NESTED_PARTIAL_TPARM_PARTIAL(N)  typename dummy##N
#define FD_NESTED_PARTIAL_TARG(N)           , dummy##N

#else // #if defined(FD_NO_FULL_SPECIALIZATION_IN_NESTED_CLASS)
#define FD_NESTED_PARTIAL_TPARM_MAIN(N)
#define FD_NESTED_PARTIAL_TPARM_PARTIAL(N)
#define FD_NESTED_PARTIAL_TARG(N)

#endif  // #if defined(FD_NO_FULL_SPECIALIZATION_IN_NESTED_CLASS)

#if defined(FD_NEED_SHADOW_TEMPLATE)
#define FD_SHADOW_TEMPLATE(T)  template<typename T>
#else // #if defined(FD_NEED_SHADOW_TEMPLATE)
#define FD_SHADOW_TEMPLATE(T)
#endif  // #if defined(FD_NEED_SHADOW_TEMPLATE)

// ================================================================================

#if !defined(FD_EXTRACT_SIMPLIFIED)

#include "dgutil.h"
#include "delegate.hpp"
#include "get_pointer.hpp"
#include "allocator.hpp"
#if !defined(FD_DISABLE_DELEGATES)
#include "combiner.hpp"
#include "dglist.hpp"
#endif  // #if !defined(FD_DISABLE_DELEGATES)

#else // #if !defined(FD_EXTRACT_SIMPLIFIED)

#include "..\..\..\include\dgutil.h"
#include "..\..\..\include\delegate.hpp"
#include "..\..\..\include\get_pointer.hpp"
#include "..\..\..\include\allocator.hpp"
#if !defined(FD_DISABLE_DELEGATES)
#include "..\..\..\include\combiner.hpp"
#include "..\..\..\include\dglist.hpp"
#endif  // #if !defined(FD_DISABLE_DELEGATES)

#endif  // #if !defined(FD_EXTRACT_SIMPLIFIED)

namespace fd
{
  using util::tie;

//
// The bad_member_function_call exception class is thrown when a delegate object is invoked
//
class bad_member_function_call : public std::runtime_error
{
public:
  bad_member_function_call() : std::runtime_error("member function call on no object") {}

};  // class bad_member_function_call : public std::runtime_error

class bad_function_call : public std::runtime_error
{
public:
  bad_function_call() : std::runtime_error("call to empty delegate") {}

};  // class bad_function_call : public std::runtime_error

// ================================================================================

enum { DefaultCountOfVoid = FD_BUF_SIZE_IN_COUNTOF_PVOID };
typedef FD_DEFAULT_ALLOCATOR    DefaultAllocator;

#if !defined(FD_DISABLE_DELEGATES)
typedef util::last_value<void>  DefaultCombiner;
typedef util::dglist<void>      DefaultDGList;
#endif  // #if !defined(FD_DISABLE_DELEGATES)

// ================================================================================

template<
typename T,
typename Allocator = DefaultAllocator,
size_t t_countof_pvoid = DefaultCountOfVoid
> class delegate; // no body

#if !defined(FD_DISABLE_DELEGATES)

template<
typename T,
typename Combiner = DefaultCombiner,
typename Allocator = DefaultAllocator,
size_t t_countof_pvoid = DefaultCountOfVoid,
typename DGList = DefaultDGList
> class delegates; // no body

#endif  // #if !defined(FD_DISABLE_DELEGATES)

} // namespace fd

// ================================================================================
// Specializations
// ================================================================================

#if !defined(FD_EXTRACT_SIMPLIFIED)

// --------------------------------------------------
#define FD_COMMA
// --------------------------------------------------
#define FD_COMMA_SP
// --------------------------------------------------

#define N 0
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

// --------------------------------------------------
#undef FD_COMMA
#define FD_COMMA  ,
// --------------------------------------------------

#define N 1
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

// --------------------------------------------------
#undef FD_COMMA_SP
#define FD_COMMA_SP  ,
// --------------------------------------------------

#define N 2
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 3
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 4
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 5
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 6
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 7
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 8
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 9
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 10
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 11
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 12
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 13
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 14
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 15
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 16
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 17
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 18
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 19
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

#define N 20
#if FD_MAX_NUM_PARAMS >= N
#include "delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "delegatestmpl.h"
#endif
#include "make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "bindtmpl.hpp"
#endif
#endif
#undef N

// --------------------------------------------------
#undef FD_COMMA
// --------------------------------------------------
#undef FD_COMMA_SP
// --------------------------------------------------

#else // #if !defined(FD_EXTRACT_SIMPLIFIED)

#include "..\..\..\include\delegatetmpl.h"
#if !defined(FD_DISABLE_DELEGATES)
#include "..\..\..\include\delegatestmpl.h"
#endif
#include "..\..\..\include\make_delegatetmpl.hpp"
#if defined(FD_ENABLE_BIND_HELPER_FUNCTION)
#include "..\..\..\include\bindtmpl.hpp"
#endif

#endif  // #if !defined(FD_EXTRACT_SIMPLIFIED)

#endif  // #if !defined(__DELEGATE_H__INCLUDED__)

