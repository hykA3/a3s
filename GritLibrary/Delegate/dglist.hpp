/** @file dglist.hpp
 *
 *  @brief  delegate list policy implementations
 *
 *  @author JaeWook Choi
 *  @version 1.22
 *
 * This software is provided "as is" without express or implied warranty, and with
 * no claim as to its suitability for any purpose.
 *
 **/
#if !defined(__DGLIST_HPP__INCLUDED__)
#define __DGLIST_HPP__INCLUDED__

#if !defined(FD_DISABLE_DELEGATES)

#if !defined(FD_EXTRACT_SIMPLIFIED)

#if !defined(FD_MS_VC6)
#include <list>
#include <map>
#include <functional>

#include <stddef.h>

#else // #if !defined(FD_MS_VC6)
#pragma warning(push)

#include <yvals.h>              // warning numbers get enabled in yvals.h 

#pragma warning(disable: 4018)  // signed/unsigned mismatch
#pragma warning(disable: 4100)  // unreferenced formal parameter
#pragma warning(disable: 4245)  // conversion from 'type1' to 'type2', 
// signed/unsigned mismatch
#pragma warning(disable: 4284)  // return type for 'identifier::operator ->' 
// is not a UDT or reference 
// to a UDT. Will produce errors if applied using 
// infix notation
#pragma warning(disable: 4512)  // 'class' : assignment operator could not be generated
#pragma warning(disable: 4663)  // C++ language change: to explicitly specialize 
// class template 'vector'
#pragma warning(disable: 4710)  // 'function' : function not inlined

// BUG: C4786 Warning Is Not Disabled with #pragma Warning
// STATUS: Microsoft has confirmed this to be a bug in the Microsoft product. 
// This warning can be ignored. This occured only in the <map> container.

#include <list>
#include <map>
#include <functional>

#if !defined(_W64)
#if !defined(__midl) && (defined(_X86_) || defined(_M_IX86)) && _MSC_VER >= 1300
#define _W64 __w64
#else
#define _W64
#endif
#endif

#ifndef _UINTPTR_T_DEFINED
#ifdef  _WIN64
typedef unsigned __int64    uintptr_t;
#else
typedef _W64 unsigned int   uintptr_t;
#endif
#define _UINTPTR_T_DEFINED
#endif

#pragma warning(pop)
#endif  // #if !defined(FD_MS_VC6)

#endif  // #if !defined(FD_EXTRACT_SIMPLIFIED)

namespace fd
{

namespace util
{

//
// --------------------------------------------------------------------------------
// public interface (typedef's)
// --------------------------------------------------------------------------------
// value_type
// iterator
// const_iterator
// size_type;
//
// --------------------------------------------------------------------------------
// public member functions
// --------------------------------------------------------------------------------
// const_iterator begin(uintptr_t hint) const;
// const_iterator end(uintptr_t hint) const;
// size_type size(uintptr_t hint) const;
// void clear(uintptr_t hint);
// void swap(thisClass & other);
// iterator find(T const & val, uintptr_t hint);
// void insert(T const & val, uintptr_t hint);
// size_type erase(T const & val, uintptr_t hint);
// void insert(thisClass const & other, uintptr_t hint);
// size_type erase(thisClass const & other, uintptr_t hint);
// bool equal(thisClass const & other, uintptr_t hint);
//
//
// (remark) hint parameter is completely ignored in class dglist and class fast_lookup_dglist
//

template<typename T, typename Allocator = std::allocator<T> >
class dglist
{
  typedef dglist thisClass;

  typedef std::list<T, Allocator> listClass;
  listClass dglist_;

  // typedef's
public:
  typedef typename listClass::value_type      value_type;
  typedef typename listClass::iterator        iterator;
  typedef typename listClass::const_iterator  const_iterator;
  typedef typename listClass::size_type       size_type;

public:
  inline const_iterator begin(uintptr_t hint = 0) const { return dglist_.begin(); }
  inline const_iterator end(uintptr_t hint = 0) const { return dglist_.end(); }
  inline size_type size(uintptr_t hint = 0) const { return dglist_.size(); }
  inline void clear(uintptr_t hint = 0) { dglist_.clear(); }
  inline void swap(thisClass & other) { dglist_.swap(other.dglist_); }

  inline iterator find(T const & val, uintptr_t /*hint*/)
  {
    return std::find_if(dglist_.begin(), dglist_.end(), std::bind1st(std::equal_to<T>(), val) );
  }

  inline void insert(T const & val, uintptr_t /*hint*/)
  {
    iterator itl_f = std::find_if(dglist_.begin(), dglist_.end(), std::bind1st(std::equal_to<T>(), val) );
    if(dglist_.end() != itl_f)
    { // found
      return;
    }
    else
    { // not found
      dglist_.push_back(val);
    }
  }

  inline size_type erase(T const & val, uintptr_t /*hint*/)
  {
    iterator it_f = std::find_if(dglist_.begin(), dglist_.end(), std::bind1st(std::equal_to<T>(), val) );
    if(dglist_.end() != it_f)
    { // found
      dglist_.erase(it_f);
      return 1;
    }
    else
    { // not found
      return 0;
    }
  }

  inline void insert(thisClass const & other, uintptr_t /*hint*/)
  {
    iterator it_e = dglist_.end();
    const_iterator it2 = other.dglist_.begin(), it2_e = other.dglist_.end();
    while(it2 != it2_e)
    {
      iterator it_f = std::find_if(dglist_.begin(), dglist_.end(), std::bind1st(std::equal_to<T>(), *it2));
      if(it_f == it_e)
      { // not found
        dglist_.push_back(*it2);
      }
      ++it2;
    }
  }

  inline size_type erase(thisClass const & other, uintptr_t /*hint*/)
  {
    size_type sz_erase = 0;
    iterator it_e = dglist_.end();
    const_iterator it2 = other.dglist_.begin(), it2_e = other.dglist_.end();
    while(it2 != it2_e)
    {
      iterator it_f = std::find_if(dglist_.begin(), dglist_.end(), std::bind1st(std::equal_to<T>(), *it2));
      if(it_f != it_e)
      { // found
        dglist_.erase(it_f);
        ++sz_erase;
      }
      ++it2;
    }

    return sz_erase;
  }

  inline bool equal(thisClass const & other, uintptr_t /*hint*/) const
  {
    if(dglist_.size() == other.dglist_.size())
    {
      return std::equal(dglist_.begin(), dglist_.end(), other.dglist_.begin());
    }
    else
    {
      return false;
    }
  }

};  // template<typename T, typename Allocator = std::allocator<T> > class list

// ================================================================================

template<typename T, typename Allocator = std::allocator<T> >
class fast_lookup_dglist
{
  typedef fast_lookup_dglist thisClass;
  typedef std::list<T, Allocator> listClass;
  listClass dglist_;

#if !defined(FD_CANT_REBIND_TYPE)
  typedef typename Allocator::template rebind<std::pair<const T, typename listClass::iterator> >::other AllocMap_;
#else // #if !defined(FD_CANT_REBIND_TYPE)
  typedef std::allocator<std::pair<const T, typename listClass::iterator> > AllocMap_;
#endif  // #if !defined(FD_CANT_REBIND_TYPE)

  typedef std::map<T, typename listClass::iterator, std::less<T>, AllocMap_> fastLookup;
  fastLookup lookup_;

  // typedef's
public:
  typedef typename listClass::value_type      value_type;
  typedef typename listClass::iterator        iterator;
  typedef typename listClass::const_iterator  const_iterator;
  typedef typename listClass::size_type       size_type;

public:
  inline const_iterator begin(uintptr_t hint = 0) const { return dglist_.begin(); }
  inline const_iterator end(uintptr_t hint = 0) const { return dglist_.end(); }
  inline size_type size(uintptr_t hint = 0) const { return dglist_.size(); }

  inline void clear(uintptr_t hint = 0)
  {
    dglist_.clear();
    lookup_.clear();
  }

  inline void swap(thisClass & other)
  {
    dglist_.swap(other.dglist_);
    lookup_.swap(other.lookup_);
  }

  inline const_iterator find(T const & val, uintptr_t /*hint*/) const
  {
    typename fastLookup::const_iterator itm_f = lookup_.find(val);
    if(lookup_.end() != itm_f)
    { // found
      return itm_f->second;
    }
    else
    { // not found
      return dglist_.end();
    }
  }

  inline void insert(T const & val, uintptr_t /*hint*/)
  {
    typename fastLookup::iterator itm_f = lookup_.find(val);
    if(lookup_.end() == itm_f)
    { // not found
      dglist_.push_back(val);
      lookup_.insert(std::make_pair(val, --dglist_.end()));
    }
  }

  inline size_type erase(T const & val, uintptr_t /*hint*/)
  {
    typename fastLookup::iterator itm_f = lookup_.find(val);
    if(lookup_.end() != itm_f)
    { // found
      iterator itl_f = itm_f->second;
      dglist_.erase(itl_f);
      lookup_.erase(itm_f);
      return 1;
    }
    else
    { // not found
      return 0;
    }
  }

  inline void insert(thisClass const & other, uintptr_t /*hint*/)
  {
    typename fastLookup::iterator itm_e = lookup_.end();
    const_iterator itl2 = other.dglist_.begin(), itl2_e = other.dglist_.end();
    while(itl2 != itl2_e)
    {
      typename fastLookup::iterator itm_f = lookup_.find(*itl2);
      if(itm_f == itm_e)
      { // not found
        iterator itl_f = itm_f->second;
        dglist_.push_back(*itl2);
        lookup_.insert(std::make_pair(*itl2, --dglist_.end()));
      }

      ++itl2;
    }
  }

  inline size_type erase(thisClass const & other, uintptr_t /*hint*/)
  {
    size_type sz_erase = 0;
    typename fastLookup::iterator itm_e = lookup_.end();
    const_iterator itl2 = other.dglist_.begin(), itl2_e = other.dglist_.end();
    while(itl2 != itl2_e)
    {
      typename fastLookup::iterator itm_f = lookup_.find(*itl2);
      if(itm_f != itm_e)
      { // found
        iterator itl_f = itm_f->second;
        dglist_.erase(itl_f);
        lookup_.erase(itm_f);
        ++sz_erase;
      }
      ++itl2;
    }

    return sz_erase;
  }

  inline bool equal(thisClass const & other, uintptr_t /*hint*/) const
  {
    if(dglist_.size() == other.dglist_.size())
    {
      return std::equal(dglist_.begin(), dglist_.end(), other.dglist_.begin());
    }
    else
    {
      return false;
    }
  }

};  // template<typename T, typename Allocator = std::allocator<T> > class fast_lookup_dglist

} // namespace util

} // namespace fd

#endif  // #if !defined(FD_DISABLE_DELEGATES)

#endif  // #if !defined(__DGLIST_HPP__INCLUDED__)
