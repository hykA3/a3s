/** @file delegatestmpl.hpp
 *
 *  @brief  multi-casting version of delegate (fd::delegates and fd::delegatesN)
 *
 *  @author JaeWook Choi
 *  @version 1.22
 *
 * This software is provided "as is" without express or implied warranty, and with
 * no claim as to its suitability for any purpose.
 *
 **/

// Note: this header is a header template and must NOT have multiple-inclusion
// protection.

#if !defined(FD_EXTRACT_SIMPLIFIED)

#if !defined(FD_MS_VC6)
#include <vector>

#include <stddef.h>

#else
#pragma warning(push)

#include <yvals.h>              // warning numbers get enabled in yvals.h 

#pragma warning(disable: 4018)  // signed/unsigned mismatch
#pragma warning(disable: 4100)  // unreferenced formal parameter
#pragma warning(disable: 4245)  // conversion from 'type1' to 'type2', 
                                // signed/unsigned mismatch
#pragma warning(disable: 4663)  // C++ language change: to explicitly specialize 
                                // class template 'vector'
#pragma warning(disable: 4702)  // unreachable code
#pragma warning(disable: 4710)  // 'function' : function not inlined
#pragma warning(disable: 4786)  // identifier was truncated to 'number' characters
                                // in the debug information
#include <vector>

#if !defined(_W64)
#if !defined(__midl) && (defined(_X86_) || defined(_M_IX86)) && _MSC_VER >= 1300
#define _W64 __w64
#else
#define _W64
#endif
#endif

#ifndef _UINTPTR_T_DEFINED
#ifdef  _WIN64
typedef unsigned __int64    uintptr_t;
#else
typedef _W64 unsigned int   uintptr_t;
#endif
#define _UINTPTR_T_DEFINED
#endif

#pragma warning(pop)
#endif

#endif  // #if !defined(FD_EXTRACT_SIMPLIFIED)

namespace fd
{

namespace detail
{

template<typename R FD_COMMA FD_TPARMS(N), typename Combiner, typename Allocator, size_t t_countof_pvoid, typename DGList>
struct FD_JOIN(delegatesImpl,N)
{
  typedef FD_JOIN(delegatesImpl,N)<R FD_COMMA FD_TARGS(N), Combiner, Allocator, t_countof_pvoid, DGList> thisClass;

  // --------------------------------------------------------------------------------

  template<typename T FD_NESTED_PARTIAL_TPARM_MAIN(0)>
    struct select_combiner_
  {
    typedef Combiner type;
  };

  template<FD_NESTED_PARTIAL_TPARM_PARTIAL(0)>
    struct select_combiner_<util::last_value<void> FD_NESTED_PARTIAL_TARG(0)>
  {
    typedef util::last_value<R> type;
  };

  typedef typename select_combiner_<Combiner>::type combiner_type;

  // --------------------------------------------------------------------------------

  // DefaultVoid - a workaround for 'void' templates in VC6.
#if defined(FD_NO_VOID_RETURN)
  typedef typename detail::VoidToDefaultVoid<typename combiner_type::result_type>::type result_type;
#else
  typedef typename combiner_type::result_type result_type;
#endif

  // --------------------------------------------------------------------------------

  typedef FD_JOIN(delegate, N)<R FD_COMMA FD_TARGS(N), Allocator, t_countof_pvoid> delegate_type;

  // --------------------------------------------------------------------------------

  template<typename T FD_NESTED_PARTIAL_TPARM_MAIN(0)>
    struct select_dglist_
  {
    typedef DGList type;
  };

  template<FD_NESTED_PARTIAL_TPARM_PARTIAL(0)>
    struct select_dglist_<util::dglist<void> FD_NESTED_PARTIAL_TARG(0)>
  {
    typedef util::dglist<delegate_type> type;
  };

  typedef typename select_dglist_<DGList>::type list_type;

  // --------------------------------------------------------------------------------

  typedef typename list_type::size_type size_type;

  // --------------------------------------------------------------------------------

  struct clear_type;

  // --------------------------------------------------------------------------------

  // data members

  list_type dglist_;

public:
  // c'tors
  FD_JOIN(delegatesImpl,N)()
    : dglist_()
  {
  }

  template<typename Combiner2>
    explicit FD_JOIN(delegatesImpl,N)(FD_JOIN(delegatesImpl,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
    : dglist_()
  {
    dglist_.insert(other.dglist_, hint);
  }
  
  explicit FD_JOIN(delegatesImpl,N)(delegate_type const & dg,
    uintptr_t hint = 0)
    : dglist_()
  {
    dglist_.insert(dg, hint);
  }
  
  // ----------------------------------------------------------------------

  template<typename Combiner2> inline
    void swap_(FD_JOIN(delegatesImpl,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> & other)
  {
    dglist_.swap(other.dglist_);
  }
  inline void reset_() { thisClass().swap_(*this); }
  inline void clear_(uintptr_t hint = 0) { dglist_.clear(hint); }
  inline size_type size_(uintptr_t hint = 0) const { return dglist_.size(hint); }
  inline bool empty_(uintptr_t hint = 0) const { return 0 == dglist_.size(hint); }

  // ----------------------------------------------------------------------

  // assignment operators
  template<typename Combiner2> inline
    void assign_(FD_JOIN(delegatesImpl,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    thisClass(other, hint).swap_(*this);
  }

  inline void assign_(delegate_type const & dg, uintptr_t hint = 0)
  {
    dglist_.clear(hint);
    dglist_.insert(dg, hint);
  }

  inline void assign_(clear_type const *, uintptr_t hint = 0)
  {
    dglist_.clear(hint);
  }

  // ----------------------------------------------------------------------

  // add delegate into the delegate list
  inline void add_(delegate_type const & dg, uintptr_t hint = 0)
  {
    dglist_.insert(dg, hint);
  }

  // remove delegate from the delegate list
  inline void remove_(delegate_type const & dg, uintptr_t hint = 0)
  {
    dglist_.erase(dg, hint);
  }

  // ----------------------------------------------------------------------

  // add delegates into the delegate list
  template<typename Combiner2> inline
    void add_(FD_JOIN(delegatesImpl,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    dglist_.insert(other.dglist_, hint);
  }

  // remove delegates from the delegate list
  template<typename Combiner2> inline
    void remove_(FD_JOIN(delegatesImpl,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0)
  {
    dglist_.erase(other.dglist_, hint);
  }

  // ----------------------------------------------------------------------

  // compare delegates with delegate in the list
  template<typename Combiner2> inline
    bool equal_(FD_JOIN(delegatesImpl,N)<R FD_COMMA FD_TARGS(N), Combiner2, Allocator, t_countof_pvoid, DGList> const & other,
    uintptr_t hint = 0) const
  {
    enum { is_same_combiner = util::Is_same_type<Combiner, Combiner2>::value };
    if(!is_same_combiner)
      return false;

    return dglist_.equal(other.dglist_, hint);
  }

  // ----------------------------------------------------------------------

  // allow to iterate through delegate list (read-only)
  inline std::pair<typename list_type::const_iterator, typename list_type::const_iterator> list_(uintptr_t hint = 0) const
  {
    return std::make_pair(dglist_.begin(hint), dglist_.end(hint));
  }

  // ----------------------------------------------------------------------

  typedef typename delegate_type::result_type dg_result_type;

  struct dg_invoker_t
  {
    typedef FD_JOIN(delegate, N)<R FD_COMMA FD_TARGS(N), Allocator, t_countof_pvoid> delegate_type;

#if N == 0
    dg_invoker_t() { }
#else // #if N == 0 
    FD_DECLMV(N);
    dg_invoker_t(FD_FPARMS(N)) : FD_CTORINIT(N) { }
#endif  // #if N == 0
    inline dg_result_type operator () (delegate_type const & dg) const { return dg(FD_FARGS2(N)); }

  };  // struct dg_invoker_t

  // ----------------------------------------------------------------------

  template<typename DGReturn FD_NESTED_PARTIAL_TPARM_MAIN(0)>
  struct func_call_ret_spec_
  {
    inline static result_type invoke_(thisClass const & dgs FD_COMMA FD_FPARMS(N), uintptr_t hint = 0)
    {
      std::vector<DGReturn> v_dgresult(dgs.dglist_.size(hint));
      // to transform results of multi-casting into std::vector
      std::transform(dgs.dglist_.begin(hint), dgs.dglist_.end(hint), v_dgresult.begin(), dg_invoker_t(FD_FARGS(N)));
      // to return the result of combiner
      return combiner_type()(v_dgresult.begin(), v_dgresult.end());
    }

  };

#if !defined(FD_NO_VOID_RETURN)
  template<FD_NESTED_PARTIAL_TPARM_PARTIAL(0)>
  struct func_call_ret_spec_<void FD_NESTED_PARTIAL_TARG(0)>
  {
    inline static result_type invoke_(thisClass const & dgs FD_COMMA FD_FPARMS(N), uintptr_t hint = 0)
    {
      std::for_each(dgs.dglist_.begin(hint), dgs.dglist_.end(hint), dg_invoker_t(FD_FARGS(N)));
      return result_type(); // return default-constructed value
    }

  };
#else // #if !defined(FD_NO_VOID_RETURN)
  template<FD_NESTED_PARTIAL_TPARM_PARTIAL(0)>
  struct func_call_ret_spec_<void const * FD_NESTED_PARTIAL_TARG(0)>
  {
    inline static result_type invoke_(thisClass const & dgs FD_COMMA FD_FPARMS(N), uintptr_t hint = 0)
    {
      std::for_each(dgs.dglist_.begin(hint), dgs.dglist_.end(hint), dg_invoker_t(FD_FARGS(N)));
      return result_type(); // return default-constructed value
    }

  };
#endif  // #if !defined(FD_NO_VOID_RETURN)

  inline result_type invoke_(FD_FPARMS(N)) const
  {
    return func_call_ret_spec_<dg_result_type>::invoke_(*this FD_COMMA FD_FARGS(N));
  }

};  // template<typename R FD_COMMA FD_TPARMS(N), typename Combiner, typename DGList> struct FD_JOIN(delegatesImpl,N)

// ================================================================================

} // namespace detail

} // namespace fd
