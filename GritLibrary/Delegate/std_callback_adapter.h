﻿/** @file std_callback_adapter.h
 *
 *  @brief  make delegate to be used as a standard C-style callback function argument in WIN32 API
 *          support from 1 parameter callback up to 9 parameters callback
 *
 *  @author JaeWook Choi
 *  @version 1.22
 *
 *  @reference
 *    1. Use member functions for C-style callbacks and threads - a general solution by Daniel Lohmann
 *      (http://www.codeproject.com/win32/callback_adapter.asp)
 *
 * ================================================================================
 * WIN32 API which require callback as an input parameter
 * ================================================================================
 * Remark) unless otherwise specified, calling convention for the following callback is __stdcall
 *
 * 1) API which require synchronous callback
 *
 * - EnumXXX() APIs
 *
 * EnumWindows(), EnumChildWindows(), EnumThreadWindow(), EnumDesktopWindows(), EnumDesktops(), EnumWindowStations(),
 * EnumResourceNames(), EnumResourceLanguages(), EnumThreadWindows(), EnumFonts(), EnumFontFamiliesEx(), EnumMetaFile(),
 * EnumEnhMetaFile(), EnumICMProfiles(), EnumObjects(), EnumerateLoadedModules(), EnumDisplayMonitors(), EnumResourceTypes(),
 * EnumSystemCodePages(), EnumSystemLocales(), EnumDateFormats(), EnumDateFormatsEx(), EnumTimeFormats(), EnumCalendarInfo(),
 * EnumObjects(), EnumProps(ex)()
 *
 * - DLGPROC (Modal)
 *
 * DialogBox(), DialogBoxIndirect(),  DialogBoxIndirectParam(), DialogBoxParam()
 *
 * --------------------------------------------------------------------------------
 *
 * 2) API which require asynchronous callback
 *
 * 2-a) single invocation
 *
 * - CreateThread() [LPTHREAD_START_ROUTINE], QueueUserWorkItem() [LPTHREAD_START_ROUTINE],
 *   CreateRemoteThread() [LPTHREAD_START_ROUTINE], CreateFiber(Ex)() [LPFIBER_START_ROUTINE]
 * - QueueUserAPC() [PAPCFUNC], SetWaitableTimer() [PTIMERAPCROUTINE], ReadFileEx() [LPOVERLAPPED_COMPLETION_ROUTINE],
 *   WriteFileEx() [LPOVERLAPPED_COMPLETION_ROUTINE], ReadDirectoryChangesW() [LPOVERLAPPED_COMPLETION_ROUTINE]
 * - SendMessageCallback() [SENDASYNCPROC]
 *
 * - AfxBeginThread() [AFX_THREADPROC, MFC, __cdecl]
 *
 * - _beginthread() [CRT, __cdecl], _beginthreadex() [CRT]
 *
 * 2-b) multiple invocations
 *
 * - SetWindowHook(Ex)() [HOOKPROC]
 * - SetTimer() [TIMERPROC]
 * - SetWinEventHook() [WINEVENTPROC]
 * - GrayString() [GRAYSTRINGPROC], SetAbortProc() [ABORTPROC]
 * - ReadEncryptedFileRaw() [PFE_EXPORT_FUNC], WriteEncryptedFileRaw() [PFE_IMPORT_FUNC]
 * - EM_SETWORDBREAKPROC [EDITWORDBREAKPROC]
 * - MessageBoxIndirect() [MSGBOXCALLBACK]
 *
 * 2-c) WNDPROC
 *
 * - RegisterWindow(Ex)(), SetWindowLong(GWL_WNDPROC), SetWindowLongPtr(GWLP_WNDPROC),
 *   SetClassLong(GCL_WNDPROC), SetClassLongPtr(GCLP_WNDPROC)
 *
 * 2-d) DLGPROC (Modeless)
 *
 * - CreateDialog(), CreateDialogIndirect(), CreateDialogIndirectParam(), CreateDialogParam(),
 *   SetWindowLong(DWL_DLGPROC), SetWindowLongPtr(DWLP_DLGPROC)
 *
 * ================================================================================
 *
 * These APIs listed above will work well with this callback adapter class and helper functions
 * provided below, but there might be more APIs than  what is listed above and as long as
 * the size of the first input argument of callback is same as the size of 'void *', the adapter
 * class should work fine.
 *
**/
#if !defined(__STD_CALLBACK_ADAPTER_H__INCLUDED__)
#define __STD_CALLBACK_ADAPTER_H__INCLUDED__

#if !defined(WIN32)
#error std_callback_adapter_impl.h requires WIN32 project only
#endif  // #if !defined(WIN32)

#if !defined(__DELEGATE_H__INCLUDED__)
#error std_callback_adapter_impl.h requires delegate.h to be included first
#endif  // #if !defined(__DELEGATE_H__INCLUDED__)

#include <windows.h>

#if defined(_MSC_VER) && _MSC_VER > 1200

#include <map>
#include <memory>

#else // #if defined(_MSC_VER) && _MSC_VER > 1200

#pragma warning(push)

#include <yvals.h>              // warning numbers get enabled in yvals.h 

#pragma warning(disable: 4018)  // signed/unsigned mismatch
#pragma warning(disable: 4100)  // unreferenced formal parameter
#pragma warning(disable: 4245)  // conversion from 'type1' to 'type2', 
                                // signed/unsigned mismatch
#pragma warning(disable: 4512)  // 'class' : assignment operator could not be generated
#pragma warning(disable: 4663)  // C++ language change: to explicitly specialize 
                                // class template 'vector'
#pragma warning(disable: 4710)  // 'function' : function not inlined
#pragma warning(disable: 4786)  // identifier was truncated to 'number' characters in 
                                // the debug information

// BUG: C4786 Warning Is Not Disabled with #pragma Warning
// STATUS: Microsoft has confirmed this to be a bug in the Microsoft product. 
// This warning can be ignored. This occured only in the <map> container.

#include <map>
#include <memory>

#pragma warning(pop)

#endif  // #if defined(_MSC_VER) && _MSC_VER > 1200

#ifndef PtrToUlong
#define PtrToUlong( p ) ((ULONG)(ULONG_PTR) (p) )
#endif

namespace fd
{

namespace detail
{

// ================================================================================
//
// assembly thunk implementation
// (to swap the first input argument of the callback with member variable p1_)
//
// ================================================================================
template<typename T1>
class callback_thunk_impl
{
  // --------------------------------------------------------------------------------
  // Standard C-style callback adapter (assembly thunk) works if and only if the size of
  // the first parameter (T1) of callback function is same as the size of a pointer
  // --------------------------------------------------------------------------------
  FD_STATIC_ASSERT(sizeof(T1) == sizeof(void *));

  // --------------------------------------------------------------------------------

protected:

  // temporary location to back up the original 'p1' which will be replaced with 'pthis' in the thunk
  T1 p1_;

  // --------------------------------------------------------------------------------

  //
  // Assembly Thunk
  //
  // the first parameter (T1 p1) will be replaced with 'pthis' in the thunk, but before
  // this replacement the original parameter 'p1' will be backed up in the temporary
  // location 'p1_' so that it can be retrieved later when it needs
  //
  struct CallbackThunk_
  {
    //  mov eax, dword ptr [esp+04h]              // 8B 44 24 04
    //  mov [12345678h], eax                      // A3 78 56 34 12
    //  mov dword ptr [esp+04h], [34567890h]      // C7 44 24 04 90 78 56 34
    //  jmp [56789012h]                           // E9 12 90 78 56

#if defined(_M_IX86)

#pragma pack(push, 1)

    struct Thunk_
    {
      DWORD m_movEAXESP;    // mov eax, dword ptr [esp+04h] (esp+04h is the first parameter 'T1 p1')
      BYTE  m_movEAX;       // mov [A], eax
      DWORD m_p1;           // [A] = [pthis->p1_]
      DWORD m_movESPthis;   // mov dword ptr [esp+04h], [B]
      DWORD m_this;         // [B] = [pthis]
      BYTE  m_jmp;          // jmp [C]
      DWORD m_relproc;      // [C] = [Callback]

    };  // struct Thunk_

#pragma pack(pop)

#else // #if defined(_M_IX86)

#error Only X86 supported

#endif  // #if defined(_M_IX86)

    Thunk_ thunk_;

    void init(DWORD_PTR proc, void * pthis)
    {

#if defined(_M_IX86)

      thunk_.m_movEAXESP  = 0x0424448B;               // 8B 44 24 04
      thunk_.m_movEAX     = 0xA3;                     // A3
      thunk_.m_p1         = (DWORD)callback_thunk_impl::offset_of_p1_(pthis);
      thunk_.m_movESPthis = 0x042444C7;               // C7 44 24 04
      thunk_.m_this       = PtrToUlong(pthis);
      thunk_.m_jmp        = 0xE9;                     // E9
      thunk_.m_relproc    = (DWORD)((INT_PTR)proc - ((INT_PTR)this + sizeof(Thunk_)));

#endif  // #if defined(_M_IX86)

      // write block from data cache and flush from instruction cache
      ::FlushInstructionCache(::GetCurrentProcess(), &thunk_, sizeof(thunk_));
    }

    void const * GetCodeAddress() const
    {
      return &(this->thunk_);
    }

  };  // struct CallbackThunk_

#if defined(_M_IX86)

  class DynamicCallbackThunk_
  {
    CallbackThunk_ * pthunk_;

  public:
    DynamicCallbackThunk_() : pthunk_(0)
    {
    }

    ~DynamicCallbackThunk_()
    {
      if(pthunk_)
        ::HeapFree(::GetProcessHeap(), 0, pthunk_);
    }

    void init(DWORD_PTR proc, void * pthis)
    {
      if(0 == pthunk_)
      {
        pthunk_ =
          static_cast<CallbackThunk_ *>(::HeapAlloc(::GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, sizeof(CallbackThunk_)));
        FD_ASSERT(pthunk_);
        pthunk_->init(proc, pthis);
      }
    }

    void const * GetCodeAddress() const
    {
      FD_ASSERT(pthunk_);
      return pthunk_->GetCodeAddress();
    }

  };  // class DynamicCallbackThunk_
  typedef DynamicCallbackThunk_ CallbackThunk;
#else // #if defined(_M_IX86)
  typedef CallbackThunk_ CallbackThunk;
#endif  // #if defined(_M_IX86)

  CallbackThunk cbthunk_;

  // --------------------------------------------------------------------------------

public:
  inline static DWORD offset_of_p1_(void * pthis)
  {
    return (DWORD)((char *)pthis + offsetof(callback_thunk_impl, p1_));
  }

};  // template<typename T1> class callback_thunk_impl

// ================================================================================
//
// garbage callback collector (singleton)
//
// ================================================================================
class async_callback_cleaner
{
  typedef delegate1<void, void *> deleterType;

  typedef std::map<void *, deleterType> GarbageCallbackMap;
  GarbageCallbackMap map_garbage_callback_;

public:
  // singleton access: check in the specified object into the cleanup list
  template<typename T> inline
    static void check_in(T * typed_obj)
  {
    instance_().check_in_(typed_obj);
  }

  // singleton access: revoke the specified object from the cleanup list
  template<typename T> inline
    static void revoke(T * typed_obj)
  {
    instance_().revoke_(typed_obj);
  }
  
  // d'tor
  ~async_callback_cleaner()
  {
    cleanup_();
  }

  // implementations
private:
  // singleton access
  static async_callback_cleaner & instance_()
  {
    static async_callback_cleaner instance_;
    return instance_;
  }

  template<typename T> inline
    void check_in_(T * typed_obj)
  {
    //TRACE(_T("async_callback_cleaner::check_in_(%p)\n"), typed_obj);
    void * untyped_obj = static_cast<void *>(typed_obj);
    GarbageCallbackMap::iterator it_f = map_garbage_callback_.find(untyped_obj);
    if(map_garbage_callback_.end() == it_f)
    { // not found
      map_garbage_callback_[untyped_obj] = deleterType(&deleter_t_<T>::delete_t_);
    }
  }

  template<typename T> inline
    void revoke_(T * typed_obj)
  {
    //TRACE(_T("async_callback_cleaner::revoke_(%p)\n"), typed_obj);
    void * untyped_obj = static_cast<void *>(typed_obj);
    GarbageCallbackMap::iterator it_f = map_garbage_callback_.find(untyped_obj);
    if(map_garbage_callback_.end() != it_f)
    { // found
      map_garbage_callback_.erase(it_f);
    }
  }

  // cleanup
  void cleanup_()
  {
    GarbageCallbackMap::iterator it = map_garbage_callback_.begin(), it_e = map_garbage_callback_.end();
    while(it != it_e)
    {
      void * untyped_obj = it->first;
      deleterType const & deleter = it->second;
      //TRACE(_T("async_callback_cleaner::cleanup_(%p)\n"), untyped_obj);
      deleter(untyped_obj);
      ++it;
    }
  }
  
  // typed deleter function
  template<typename T>
    struct deleter_t_
  {
    static void delete_t_(void * untyped_obj)
    {
      T * typed_obj = static_cast<T *>(untyped_obj);
      delete typed_obj;
    }
    
  };  // template<typename T> struct deleter_t_

  // do not allow
private:
  // default c'tor
  async_callback_cleaner() { }
  // copy c'tor
  async_callback_cleaner(async_callback_cleaner const & other);
  // assignment operator
  async_callback_cleaner & operator = (async_callback_cleaner const & other);
  
};  // class async_callback_cleaner

// ================================================================================
//
// scoped auto deleter
// (to delete the stored object when the life time of instance goes out of its scope)
//
// ================================================================================
template<typename T>
class auto_deleter_t_
{
  T * pt_;
public:
  // c'tor
  explicit auto_deleter_t_(T * pt) : pt_(pt)
  {
  }
  // d'tor
  ~auto_deleter_t_()
  {
    if(pt_)
    {
      delete pt_;
      // to revoke this from the garbage callback cleaner
      async_callback_cleaner::revoke(pt_);
    }
  }

};  // template<typename T> class auto_deleter_t_

// ================================================================================
//
// sta_callback with various numbers of input parameter implementations
//
// ================================================================================
template<typename R, typename T1>
class std_callback_adapter_impl1 : private callback_thunk_impl<T1>
{
  typedef std_callback_adapter_impl1<R, T1> thisClass;
  typedef delegate1<R, T1> dgClass;
  typedef R (CALLBACK * Callback)(T1);

  // --------------------------------------------------------------------------------

  // data members
  dgClass dg_;
  bool single_invoke_;

  // --------------------------------------------------------------------------------

public:

  explicit std_callback_adapter_impl1(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : dg_(dg), single_invoke_(single_invoke)
  {

#if defined(FD_NO_VOID_RETURN)

    cbthunk_.init(
      (DWORD_PTR)&util::If<util::Is_same_type<R, void>::value, select_void_, select_>::Result::std_callback_,
      this);

#else // #if defined(FD_NO_VOID_RETURN)

    cbthunk_.init((DWORD_PTR)&select_::std_callback_, this);

#endif  // #if defined(FD_NO_VOID_RETURN)

    if(auto_delete)
      async_callback_cleaner::check_in(this);
  }

  operator Callback () const
  {
    return reinterpret_cast<Callback>(cbthunk_.GetCodeAddress());
  }

  // implementations
private:

  struct select_
  {
    static R CALLBACK std_callback_(T1 p1)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      return pthis->dg_(pthis->p1_);
    }

  };  // struct select_
  friend struct select_;

#if defined(FD_NO_VOID_RETURN)

  struct select_void_
  {
    static R CALLBACK std_callback_(T1 p1)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      pthis->dg_(pthis->p1_);
    }

  };  // struct select_void_
  friend struct select_void_;

#endif  // #if defined(FD_NO_VOID_RETURN)

};  // template<typename R, typename T1> class std_callback_adapter_impl1

// ================================================================================

template<typename R, typename T1, typename T2>
class std_callback_adapter_impl2 : private callback_thunk_impl<T1>
{
  typedef std_callback_adapter_impl2<R, T1, T2> thisClass;
  typedef delegate2<R, T1, T2> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2);

  // --------------------------------------------------------------------------------

  // data members
  dgClass dg_;
  bool single_invoke_;

  // --------------------------------------------------------------------------------

public:

  explicit std_callback_adapter_impl2(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : dg_(dg), single_invoke_(single_invoke)
  {

#if defined(FD_NO_VOID_RETURN)

    cbthunk_.init(
      (DWORD_PTR)&util::If<util::Is_same_type<R, void>::value, select_void_, select_>::Result::std_callback_,
      this);

#else // #if defined(FD_NO_VOID_RETURN)

    cbthunk_.init((DWORD_PTR)&select_::std_callback_, this);

#endif  // #if defined(FD_NO_VOID_RETURN)

    if(auto_delete)
      async_callback_cleaner::check_in(this);
  }

  operator Callback () const
  {
    return reinterpret_cast<Callback>(cbthunk_.GetCodeAddress());
  }

  // implementations
private:

  struct select_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      return pthis->dg_(pthis->p1_, p2);
    }

  };  // struct select_
  friend struct select_;

#if defined(FD_NO_VOID_RETURN)

  struct select_void_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      pthis->dg_(pthis->p1_, p2);
    }

  };  // struct select_void_
  friend struct select_void_;

#endif  // #if defined(FD_NO_VOID_RETURN)

};  // template<typename R, typename T1, typename T2> class std_callback_adapter_impl2

// ================================================================================

template<typename R, typename T1, typename T2, typename T3>
class std_callback_adapter_impl3 : private callback_thunk_impl<T1>
{
  typedef std_callback_adapter_impl3<R, T1, T2, T3> thisClass;
  typedef delegate3<R, T1, T2, T3> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3);

  // --------------------------------------------------------------------------------

  // data members
  dgClass dg_;
  bool single_invoke_;

  // --------------------------------------------------------------------------------

public:

  explicit std_callback_adapter_impl3(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : dg_(dg), single_invoke_(single_invoke)
  {

#if defined(FD_NO_VOID_RETURN)

    cbthunk_.init(
      (DWORD_PTR)&util::If<util::Is_same_type<R, void>::value, select_void_, select_>::Result::std_callback_,
      this);

#else // #if defined(FD_NO_VOID_RETURN)

    cbthunk_.init((DWORD_PTR)&select_::std_callback_, this);

#endif  // #if defined(FD_NO_VOID_RETURN)

    if(auto_delete)
      async_callback_cleaner::check_in(this);
  }

  operator Callback () const
  {
    return reinterpret_cast<Callback>(cbthunk_.GetCodeAddress());
  }

  // implementations
private:

  struct select_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      return pthis->dg_(pthis->p1_, p2, p3);
    }

  };  // struct select_
  friend struct select_;

#if defined(FD_NO_VOID_RETURN)

  struct select_void_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      pthis->dg_(pthis->p1_, p2, p3);
    }

  };  // struct select_void_
  friend struct select_void_;

#endif  // #if defined(FD_NO_VOID_RETURN)

};  // template<typename R, typename T1, typename T2, typename T3> class std_callback_adapter_impl3

// ================================================================================

template<typename R, typename T1, typename T2, typename T3, typename T4>
class std_callback_adapter_impl4 : private callback_thunk_impl<T1>
{
  typedef std_callback_adapter_impl4<R, T1, T2, T3, T4> thisClass;
  typedef delegate4<R, T1, T2, T3, T4> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4);

  // --------------------------------------------------------------------------------

  // data members
  dgClass dg_;
  bool single_invoke_;

  // --------------------------------------------------------------------------------

public:

  explicit std_callback_adapter_impl4(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : dg_(dg), single_invoke_(single_invoke)
  {

#if defined(FD_NO_VOID_RETURN)

    cbthunk_.init(
      (DWORD_PTR)&util::If<util::Is_same_type<R, void>::value, select_void_, select_>::Result::std_callback_,
      this);

#else // #if defined(FD_NO_VOID_RETURN)

    cbthunk_.init((DWORD_PTR)&select_::std_callback_, this);

#endif  // #if defined(FD_NO_VOID_RETURN)

    if(auto_delete)
      async_callback_cleaner::check_in(this);
  }

  operator Callback () const
  {
    return reinterpret_cast<Callback>(cbthunk_.GetCodeAddress());
  }

  // implementations
private:

  struct select_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      return pthis->dg_(pthis->p1_, p2, p3, p4);
    }

  };  // struct select_
  friend struct select_;

#if defined(FD_NO_VOID_RETURN)

  struct select_void_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      pthis->dg_(pthis->p1_, p2, p3, p4);
    }

  };  // struct select_void_
  friend struct select_void_;

#endif  // #if defined(FD_NO_VOID_RETURN)

};  // template<typename R, typename T1, typename T2, typename T3, typename T4> class std_callback_adapter_impl4

// ================================================================================

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
class std_callback_adapter_impl5 : private callback_thunk_impl<T1>
{
  typedef std_callback_adapter_impl5<R, T1, T2, T3, T4, T5> thisClass;
  typedef delegate5<R, T1, T2, T3, T4, T5> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5);

  // --------------------------------------------------------------------------------

  // data members
  dgClass dg_;
  bool single_invoke_;

  // --------------------------------------------------------------------------------

public:

  explicit std_callback_adapter_impl5(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : dg_(dg), single_invoke_(single_invoke)
  {

#if defined(FD_NO_VOID_RETURN)

    cbthunk_.init(
      (DWORD_PTR)&util::If<util::Is_same_type<R, void>::value, select_void_, select_>::Result::std_callback_,
      this);

#else // #if defined(FD_NO_VOID_RETURN)

    cbthunk_.init((DWORD_PTR)&select_::std_callback_, this);

#endif  // #if defined(FD_NO_VOID_RETURN)

    if(auto_delete)
      async_callback_cleaner::check_in(this);
  }

  operator Callback () const
  {
    return reinterpret_cast<Callback>(cbthunk_.GetCodeAddress());
  }

  // implementations
private:

  struct select_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      return pthis->dg_(pthis->p1_, p2, p3, p4, p5);
    }

  };  // struct select_
  friend struct select_;

#if defined(FD_NO_VOID_RETURN)

  struct select_void_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      pthis->dg_(pthis->p1_, p2, p3, p4, p5);
    }

  };  // struct select_void_
  friend struct select_void_;

#endif  // #if defined(FD_NO_VOID_RETURN)

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5> class std_callback_adapter_impl5

// ================================================================================

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
class std_callback_adapter_impl6 : private callback_thunk_impl<T1>
{
  typedef std_callback_adapter_impl6<R, T1, T2, T3, T4, T5, T6> thisClass;
  typedef delegate6<R, T1, T2, T3, T4, T5, T6> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6);

  // --------------------------------------------------------------------------------

  // data members
  dgClass dg_;
  bool single_invoke_;

  // --------------------------------------------------------------------------------

public:

  explicit std_callback_adapter_impl6(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : dg_(dg), single_invoke_(single_invoke)
  {

#if defined(FD_NO_VOID_RETURN)

    cbthunk_.init(
      (DWORD_PTR)&util::If<util::Is_same_type<R, void>::value, select_void_, select_>::Result::std_callback_,
      this);

#else // #if defined(FD_NO_VOID_RETURN)

    cbthunk_.init((DWORD_PTR)&select_::std_callback_, this);

#endif  // #if defined(FD_NO_VOID_RETURN)

    if(auto_delete)
      async_callback_cleaner::check_in(this);
  }

  operator Callback () const
  {
    return reinterpret_cast<Callback>(cbthunk_.GetCodeAddress());
  }

  // implementations
private:

  struct select_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      return pthis->dg_(pthis->p1_, p2, p3, p4, p5, p6);
    }

  };  // struct select_
  friend struct select_;

#if defined(FD_NO_VOID_RETURN)

  struct select_void_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      pthis->dg_(pthis->p1_, p2, p3, p4, p5, p6);
    }

  };  // struct select_void_
  friend struct select_void_;

#endif  // #if defined(FD_NO_VOID_RETURN)

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> class std_callback_adapter_impl6

// ================================================================================

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
class std_callback_adapter_impl7 : private callback_thunk_impl<T1>
{
  typedef std_callback_adapter_impl7<R, T1, T2, T3, T4, T5, T6, T7> thisClass;
  typedef delegate7<R, T1, T2, T3, T4, T5, T6, T7> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6, T7);

  // --------------------------------------------------------------------------------

  // data members
  dgClass dg_;
  bool single_invoke_;

  // --------------------------------------------------------------------------------

public:

  explicit std_callback_adapter_impl7(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : dg_(dg), single_invoke_(single_invoke)
  {

#if defined(FD_NO_VOID_RETURN)

    cbthunk_.init(
      (DWORD_PTR)&util::If<util::Is_same_type<R, void>::value, select_void_, select_>::Result::std_callback_,
      this);

#else // #if defined(FD_NO_VOID_RETURN)

    cbthunk_.init((DWORD_PTR)&select_::std_callback_, this);

#endif  // #if defined(FD_NO_VOID_RETURN)

    if(auto_delete)
      async_callback_cleaner::check_in(this);
  }

  operator Callback () const
  {
    return reinterpret_cast<Callback>(cbthunk_.GetCodeAddress());
  }

  // implementations
private:

  struct select_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      return pthis->dg_(pthis->p1_, p2, p3, p4, p5, p6, p7);
    }

  };  // struct select_
  friend struct select_;

#if defined(FD_NO_VOID_RETURN)

  struct select_void_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      pthis->dg_(pthis->p1_, p2, p3, p4, p5, p6, p7);
    }

  };  // struct select_void_
  friend struct select_void_;

#endif  // #if defined(FD_NO_VOID_RETURN)

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7> class std_callback_adapter_impl7

// ================================================================================

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
class std_callback_adapter_impl8 : private callback_thunk_impl<T1>
{
  typedef std_callback_adapter_impl8<R, T1, T2, T3, T4, T5, T6, T7, T8> thisClass;
  typedef delegate8<R, T1, T2, T3, T4, T5, T6, T7, T8> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6, T7, T8);

  // --------------------------------------------------------------------------------

  // data members
  dgClass dg_;
  bool single_invoke_;

  // --------------------------------------------------------------------------------

public:

  explicit std_callback_adapter_impl8(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : dg_(dg), single_invoke_(single_invoke)
  {

#if defined(FD_NO_VOID_RETURN)

    cbthunk_.init(
      (DWORD_PTR)&util::If<util::Is_same_type<R, void>::value, select_void_, select_>::Result::std_callback_,
      this);

#else // #if defined(FD_NO_VOID_RETURN)

    cbthunk_.init((DWORD_PTR)&select_::std_callback_, this);

#endif  // #if defined(FD_NO_VOID_RETURN)

    if(auto_delete)
      async_callback_cleaner::check_in(this);
  }

  operator Callback () const
  {
    return reinterpret_cast<Callback>(cbthunk_.GetCodeAddress());
  }

  // implementations
private:

  struct select_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      return pthis->dg_(pthis->p1_, p2, p3, p4, p5, p6, p7, p8);
    }

  };  // struct select_
  friend struct select_;

#if defined(FD_NO_VOID_RETURN)

  struct select_void_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      pthis->dg_(pthis->p1_, p2, p3, p4, p5, p6, p7, p8);
    }

  };  // struct select_void_
  friend struct select_void_;

#endif  // #if defined(FD_NO_VOID_RETURN)

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8> class std_callback_adapter_impl8

// ================================================================================

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
class std_callback_adapter_impl9 : private callback_thunk_impl<T1>
{
  typedef std_callback_adapter_impl9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9> thisClass;
  typedef delegate9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6, T7, T8, T9);

  // --------------------------------------------------------------------------------

  // data members
  dgClass dg_;
  bool single_invoke_;

  // --------------------------------------------------------------------------------

public:

  explicit std_callback_adapter_impl9(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : dg_(dg), single_invoke_(single_invoke)
  {

#if defined(FD_NO_VOID_RETURN)

    cbthunk_.init(
      (DWORD_PTR)&util::If<util::Is_same_type<R, void>::value, select_void_, select_>::Result::std_callback_,
      this);

#else // #if defined(FD_NO_VOID_RETURN)

    cbthunk_.init((DWORD_PTR)&select_::std_callback_, this);

#endif  // #if defined(FD_NO_VOID_RETURN)

    if(auto_delete)
      async_callback_cleaner::check_in(this);
  }

  operator Callback () const
  {
    return reinterpret_cast<Callback>(cbthunk_.GetCodeAddress());
  }

  // implementations
private:

  struct select_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      return pthis->dg_(pthis->p1_, p2, p3, p4, p5, p6, p7, p8, p9);
    }

  };  // struct select_
  friend struct select_;

#if defined(FD_NO_VOID_RETURN)

  struct select_void_
  {
    static R CALLBACK std_callback_(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      pthis->dg_(pthis->p1_, p2, p3, p4, p5, p6, p7, p8, p9);
    }

  };  // struct select_void_
  friend struct select_void_;

#endif  // #if defined(FD_NO_VOID_RETURN)

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9> class std_callback_adapter_impl9

// ================================================================================

#if defined(_MFC_VER) || defined(_INC_PROCESS)

template<typename R, typename T1>
class crt_callback_adapter_impl1 : private callback_thunk_impl<T1>
{
  typedef crt_callback_adapter_impl1<R, T1> thisClass;
  typedef delegate1<R, T1> dgClass;
  typedef R (__cdecl * Callback)(T1);

  // --------------------------------------------------------------------------------

  // data members
  dgClass dg_;
  bool single_invoke_;

  // --------------------------------------------------------------------------------

public:

  explicit crt_callback_adapter_impl1(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : dg_(dg), single_invoke_(single_invoke)
  {

#if defined(FD_NO_VOID_RETURN)

    cbthunk_.init(
      (DWORD_PTR)&util::If<util::Is_same_type<R, void>::value, select_void_, select_>::Result::crt_callback_,
      this);

#else // #if defined(FD_NO_VOID_RETURN)

    cbthunk_.init((DWORD_PTR)&select_::crt_callback_, this);

#endif  // #if defined(FD_NO_VOID_RETURN)

    if(auto_delete)
      async_callback_cleaner::check_in(this);
  }

  operator Callback () const
  {
    return reinterpret_cast<Callback>(cbthunk_.GetCodeAddress());
  }

  // implementations
private:

  struct select_
  {
    static R __cdecl crt_callback_(T1 p1)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      return pthis->dg_(pthis->p1_);
    }

  };  // struct select_
  friend struct select_;

#if defined(FD_NO_VOID_RETURN)

  struct select_void_
  {
    static R __cdecl crt_callback_(T1 p1)
    {
      thisClass * pthis = reinterpret_cast<thisClass *>(p1);
      auto_deleter_t_<thisClass> g_del_(pthis->single_invoke_ ? pthis : 0);

      pthis->dg_(pthis->p1_);
    }

  };  // struct select_void_
  friend struct select_void_;

#endif  // #if defined(FD_NO_VOID_RETURN)

};  // template<typename R, typename T1> class crt_callback_adapter_impl1

#endif  // #if defined(_MFC_VER) || defined(_INC_PROCESS)

// ================================================================================

template<typename R, typename T1, typename T2, typename T3, typename T4>
class msg_callback_adapter_impl : private callback_thunk_impl<T1>
{
  typedef msg_callback_adapter_impl<R, T1, T2, T3, T4> thisClass;
  typedef delegate4<R, T1, T2, T3, T4> dgClass;
  typedef R (__stdcall * Callback)(T1, T2, T3, T4);

  // --------------------------------------------------------------------------------

  // data members
  dgClass dg_;

  // --------------------------------------------------------------------------------

public:

  explicit msg_callback_adapter_impl(dgClass const & dg)
    : dg_(dg)
  {
    cbthunk_.init((DWORD_PTR)&msg_proc_, this);

    async_callback_cleaner::check_in(this);
  }

  operator Callback () const
  {
    return reinterpret_cast<Callback>(cbthunk_.GetCodeAddress());
  }

  // implementations
private:

  static LRESULT CALLBACK msg_proc_(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
  {
    thisClass * pthis = reinterpret_cast<thisClass *>(hwnd);
    auto_deleter_t_<thisClass> g_del_(WM_NCDESTROY == uMsg ? pthis : 0);

    return  pthis->dg_(pthis->p1_, uMsg, wParam, lParam);
  }

};  // class msg_callback_adapter_impl

// ================================================================================

} // namespace detail

// ================================================================================
//
// Preferred Syntax
//
// ================================================================================

#if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

// --------------------------------------------------------------------------------

template<typename T> class std_callback;

// --------------------------------------------------------------------------------

template<typename R, typename T1>
class std_callback<R (T1)>
{
  typedef std_callback<R (T1)> thisClass;
  typedef detail::std_callback_adapter_impl1<R, T1> implClass;
  typedef delegate1<R, T1> dgClass;
  typedef R (CALLBACK * Callback)(T1);

  implClass impl_;

public:

  explicit std_callback(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1> class std_callback<R (T1)>

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2>
class std_callback<R (T1, T2)>
{
  typedef std_callback<R (T1, T2)> thisClass;
  typedef detail::std_callback_adapter_impl2<R, T1, T2> implClass;
  typedef delegate2<R, T1, T2> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2);

  implClass impl_;

public:

  explicit std_callback(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2> class std_callback<R (T1, T2)>

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3>
class std_callback<R (T1, T2, T3)>
{
  typedef std_callback<R (T1, T2, T3)> thisClass;
  typedef detail::std_callback_adapter_impl3<R, T1, T2, T3> implClass;
  typedef delegate3<R, T1, T2, T3> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3);

  implClass impl_;

public:

  explicit std_callback(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3> class std_callback<R (T1, T2, T3)>

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4>
class std_callback<R (T1, T2, T3, T4)>
{
  typedef std_callback<R (T1, T2, T3, T4)> thisClass;
  typedef detail::std_callback_adapter_impl4<R, T1, T2, T3, T4> implClass;
  typedef delegate4<R, T1, T2, T3, T4> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4);

  implClass impl_;

public:

  explicit std_callback(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4> class std_callback<R (T1, T2, T3, T4)>

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
class std_callback<R (T1, T2, T3, T4, T5)>
{
  typedef std_callback<R (T1, T2, T3, T4, T5)> thisClass;
  typedef detail::std_callback_adapter_impl5<R, T1, T2, T3, T4, T5> implClass;
  typedef delegate5<R, T1, T2, T3, T4, T5> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5);

  implClass impl_;

public:

  explicit std_callback(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5> class std_callback<R (T1, T2, T3, T4, T5)>

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
class std_callback<R (T1, T2, T3, T4, T5, T6)>
{
  typedef std_callback<R (T1, T2, T3, T4, T5, T6)> thisClass;
  typedef detail::std_callback_adapter_impl6<R, T1, T2, T3, T4, T5, T6> implClass;
  typedef delegate6<R, T1, T2, T3, T4, T5, T6> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6);

  implClass impl_;

public:

  explicit std_callback(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> class std_callback<R (T1, T2, T3, T4, T5, T6)>

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
class std_callback<R (T1, T2, T3, T4, T5, T6, T7)>
{
  typedef std_callback<R (T1, T2, T3, T4, T5, T6, T7)> thisClass;
  typedef detail::std_callback_adapter_impl7<R, T1, T2, T3, T4, T5, T6, T7> implClass;
  typedef delegate7<R, T1, T2, T3, T4, T5, T6, T7> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6, T7);

  implClass impl_;

public:

  explicit std_callback(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7> class std_callback<R (T1, T2, T3, T4, T5, T6, T7)>

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
class std_callback<R (T1, T2, T3, T4, T5, T6, T7, T8)>
{
  typedef std_callback<R (T1, T2, T3, T4, T5, T6, T7, T8)> thisClass;
  typedef detail::std_callback_adapter_impl8<R, T1, T2, T3, T4, T5, T6, T7, T8> implClass;
  typedef delegate8<R, T1, T2, T3, T4, T5, T6, T7, T8> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6, T7, T8);

  implClass impl_;

public:

  explicit std_callback(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8> class std_callback<R (T1, T2, T3, T4, T5, T6, T7, T8)>

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
class std_callback<R (T1, T2, T3, T4, T5, T6, T7, T8, T9)>
{
  typedef std_callback<R (T1, T2, T3, T4, T5, T6, T7, T8, T9)> thisClass;
  typedef detail::std_callback_adapter_impl9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9> implClass;
  typedef delegate9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6, T7, T8, T9);

  implClass impl_;

public:

  explicit std_callback(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9> class std_callback<R (T1, T2, T3, T4, T5, T6, T7, T8, T9)>

// --------------------------------------------------------------------------------

#endif  // #if !defined(FD_NO_TEMPLATE_PARTIAL_SPECIALIZATION)

// ================================================================================
//
// Portable Syntax
//
// ================================================================================

template<typename R, typename T1>
class std_callback1
{
  typedef std_callback1<R, T1> thisClass;
  typedef detail::std_callback_adapter_impl1<R, T1> implClass;
  typedef delegate1<R, T1> dgClass;
  typedef R (CALLBACK * Callback)(T1);

  implClass impl_;

public:

  explicit std_callback1(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1> class std_callback1

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2>
class std_callback2
{
  typedef std_callback2<R, T1, T2> thisClass;
  typedef detail::std_callback_adapter_impl2<R, T1, T2> implClass;
  typedef delegate2<R, T1, T2> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2);

  implClass impl_;

public:

  explicit std_callback2(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2> class std_callback2

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3>
class std_callback3
{
  typedef std_callback3<R, T1, T2, T3> thisClass;
  typedef detail::std_callback_adapter_impl3<R, T1, T2, T3> implClass;
  typedef delegate3<R, T1, T2, T3> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3);

  implClass impl_;

public:

  explicit std_callback3(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3> class std_callback3

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4>
class std_callback4
{
  typedef std_callback4<R, T1, T2, T3, T4> thisClass;
  typedef detail::std_callback_adapter_impl4<R, T1, T2, T3, T4> implClass;
  typedef delegate4<R, T1, T2, T3, T4> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4);

  implClass impl_;

public:

  explicit std_callback4(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4> class std_callback4

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
class std_callback5
{
  typedef std_callback5<R, T1, T2, T3, T4, T5> thisClass;
  typedef detail::std_callback_adapter_impl5<R, T1, T2, T3, T4, T5> implClass;
  typedef delegate5<R, T1, T2, T3, T4, T5> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5);

  implClass impl_;

public:

  explicit std_callback5(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5> class std_callback5

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
class std_callback6
{
  typedef std_callback6<R, T1, T2, T3, T4, T5, T6> thisClass;
  typedef detail::std_callback_adapter_impl6<R, T1, T2, T3, T4, T5, T6> implClass;
  typedef delegate6<R, T1, T2, T3, T4, T5, T6> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6);

  implClass impl_;

public:

  explicit std_callback6(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> class std_callback6

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7>
class std_callback7
{
  typedef std_callback7<R, T1, T2, T3, T4, T5, T6, T7> thisClass;
  typedef detail::std_callback_adapter_impl7<R, T1, T2, T3, T4, T5, T6, T7> implClass;
  typedef delegate7<R, T1, T2, T3, T4, T5, T6, T7> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6, T7);

  implClass impl_;

public:

  explicit std_callback7(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7> class std_callback7

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8>
class std_callback8
{
  typedef std_callback8<R, T1, T2, T3, T4, T5, T6, T7, T8> thisClass;
  typedef detail::std_callback_adapter_impl8<R, T1, T2, T3, T4, T5, T6, T7, T8> implClass;
  typedef delegate8<R, T1, T2, T3, T4, T5, T6, T7, T8> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6, T7, T8);

  implClass impl_;

public:

  explicit std_callback8(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8> class std_callback8

// --------------------------------------------------------------------------------

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
class std_callback9
{
  typedef std_callback9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9> thisClass;
  typedef detail::std_callback_adapter_impl9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9> implClass;
  typedef delegate9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9> dgClass;
  typedef R (CALLBACK * Callback)(T1, T2, T3, T4, T5, T6, T7, T8, T9);

  implClass impl_;

public:

  explicit std_callback9(dgClass const & dg, bool auto_delete = false, bool single_invoke = false)
    : impl_(dg, auto_delete)
  {
  }

  operator Callback () const
  {
    return impl_.operator Callback();
  }

};  // template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9> class std_callback9

// ================================================================================
//
// make_std_callback() helper function
//
// This helper function return temporary object of a std_callback_adapter_implX class
// therefore *DO NOT USE* this helper function for 'asynchronous callback'. Use
// make_async_std_callback() helper function instead.
//
// Internally cloned bound object of the callback member function get terminated
// when the line goes out of scope since it is a temporary object being returned
//
// ================================================================================

template<typename R, typename T1> inline
detail::std_callback_adapter_impl1<R, T1>
make_std_callback(delegate1<R, T1> const & dg)
{
  return detail::std_callback_adapter_impl1<R, T1>(dg);
}

template<typename R, typename T1, typename T2> inline
detail::std_callback_adapter_impl2<R, T1, T2>
make_std_callback(delegate2<R, T1, T2> const & dg)
{
  return detail::std_callback_adapter_impl2<R, T1, T2>(dg);
}

template<typename R, typename T1, typename T2, typename T3> inline
detail::std_callback_adapter_impl3<R, T1, T2, T3>
make_std_callback(delegate3<R, T1, T2, T3> const & dg)
{
  return detail::std_callback_adapter_impl3<R, T1, T2, T3>(dg);
}

template<typename R, typename T1, typename T2, typename T3, typename T4> inline
detail::std_callback_adapter_impl4<R, T1, T2, T3, T4>
make_std_callback(delegate4<R, T1, T2, T3, T4> const & dg)
{
  return detail::std_callback_adapter_impl4<R, T1, T2, T3, T4>(dg);
}

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5> inline
detail::std_callback_adapter_impl5<R, T1, T2, T3, T4, T5>
make_std_callback(delegate5<R, T1, T2, T3, T4, T5> const & dg)
{
  return detail::std_callback_adapter_impl5<R, T1, T2, T3, T4, T5>(dg);
}

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline
detail::std_callback_adapter_impl6<R, T1, T2, T3, T4, T5, T6>
make_std_callback(delegate6<R, T1, T2, T3, T4, T5, T6> const & dg)
{
  return detail::std_callback_adapter_impl6<R, T1, T2, T3, T4, T6>(dg);
}

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7> inline
detail::std_callback_adapter_impl7<R, T1, T2, T3, T4, T5, T6, T7>
make_std_callback(delegate7<R, T1, T2, T3, T4, T5, T6, T7> const & dg)
{
  return detail::std_callback_adapter_impl7<R, T1, T2, T3, T4, T5, T6, T7>(dg);
}

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8> inline
detail::std_callback_adapter_impl8<R, T1, T2, T3, T4, T5, T6, T7, T8>
make_std_callback(delegate8<R, T1, T2, T3, T4, T5, T6, T7, T8> const & dg)
{
  return detail::std_callback_adapter_impl8<R, T1, T2, T3, T4, T5, T6, T7, T8>(dg);
}

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9> inline
detail::std_callback_adapter_impl9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9>
make_std_callback(delegate9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9> const & dg)
{
  return detail::std_callback_adapter_impl9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9>(dg);
}

// ================================================================================
//
// make_async_std_callback() helper function
//
// This helper function return the reference to the heap allocated object of
// a std_callback_adapter_implX class
//
// the allocated heap object will be deallocated automatically after the first asynchronous
// callback invocation (IOW, the internally stored delegate will be terminated at the same time)
//
// ================================================================================

template<typename R, typename T1> inline
detail::std_callback_adapter_impl1<R, T1> const &
make_async_std_callback(delegate1<R, T1> const & dg, bool single_invoke = false)
{
  return *(new detail::std_callback_adapter_impl1<R, T1>(dg, true, single_invoke));
}

template<typename R, typename T1, typename T2> inline
detail::std_callback_adapter_impl2<R, T1, T2> const &
make_async_std_callback(delegate2<R, T1, T2> const & dg, bool single_invoke = false)
{
  return *(new detail::std_callback_adapter_impl2<R, T1, T2>(dg, true, single_invoke));
}

template<typename R, typename T1, typename T2, typename T3> inline
detail::std_callback_adapter_impl3<R, T1, T2, T3> const &
make_async_std_callback(delegate3<R, T1, T2, T3> const & dg, bool single_invoke = false)
{
  return *(new detail::std_callback_adapter_impl3<R, T1, T2, T3>(dg, true, single_invoke));
}

template<typename R, typename T1, typename T2, typename T3, typename T4> inline
detail::std_callback_adapter_impl4<R, T1, T2, T3, T4> const &
make_async_std_callback(delegate4<R, T1, T2, T3, T4> const & dg, bool single_invoke = false)
{
  return *(new detail::std_callback_adapter_impl4<R, T1, T2, T3, T4>(dg, true, single_invoke));
}

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5> inline
detail::std_callback_adapter_impl5<R, T1, T2, T3, T4, T5> const &
make_async_std_callback(delegate5<R, T1, T2, T3, T4, T5> const & dg, bool single_invoke = false)
{
  return *(new detail::std_callback_adapter_impl5<R, T1, T2, T3, T4, T5>(dg, true, single_invoke));
}

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline
detail::std_callback_adapter_impl6<R, T1, T2, T3, T4, T5, T6> const &
make_async_std_callback(delegate6<R, T1, T2, T3, T4, T5, T6> const & dg, bool single_invoke = false)
{
  return *(new detail::std_callback_adapter_impl6<R, T1, T2, T3, T4, T5, T6>(dg, true, single_invoke));
}

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7> inline
detail::std_callback_adapter_impl7<R, T1, T2, T3, T4, T5, T6, T7> const &
make_async_std_callback(delegate7<R, T1, T2, T3, T4, T5, T6, T7> const & dg, bool single_invoke = false)
{
  return *(new detail::std_callback_adapter_impl7<R, T1, T2, T3, T4, T5, T6, T7>(dg, true, single_invoke));
}

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8> inline
detail::std_callback_adapter_impl8<R, T1, T2, T3, T4, T5, T6, T7, T8> const &
make_async_std_callback(delegate8<R, T1, T2, T3, T4, T5, T6, T7, T8> const & dg, bool single_invoke = false)
{
  return *(new detail::std_callback_adapter_impl8<R, T1, T2, T3, T4, T5, T6, T7, T8>(dg, true, single_invoke));
}

template<typename R, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9> inline
detail::std_callback_adapter_impl9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9> const &
make_async_std_callback(delegate9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9> const & dg, bool single_invoke = false)
{
  return *(new detail::std_callback_adapter_impl9<R, T1, T2, T3, T4, T5, T6, T7, T8, T9>(dg, true, single_invoke));
}

#if defined(_MFC_VER) || defined(_INC_PROCESS)

// ================================================================================
//
// make_async_crt_callback() helper function
//
// Special version for MFC's AfxBeginThread() or CRT's _beginthread()
//
// ================================================================================

template<typename R, typename T1> inline
detail::crt_callback_adapter_impl1<R, T1> const &
make_async_crt_callback(delegate1<R, T1> const & dg, bool single_invoke = false)
{
  return *(new detail::crt_callback_adapter_impl1<R, T1>(dg, true, single_invoke));
}

// ================================================================================

#endif  // #if defined(_MFC_VER) || defined(_INC_PROCESS)

// ================================================================================
//
// make_async_msg_proc() helper function
//
// Special version for WNDPROC and DLGPROC
//
// ================================================================================

template<typename R, typename T1, typename T2, typename T3, typename T4> inline
detail::msg_callback_adapter_impl<R, T1, T2, T3, T4> const &
make_async_msg_proc(delegate4<R, T1, T2, T3, T4> const & dg)
{
  return *(new detail::msg_callback_adapter_impl<R, T1, T2, T3, T4>(dg));
}

// ================================================================================

} // namespace fd

#endif  // #if !defined(__STD_CALLBACK_ADAPTER_H__INCLUDED__)
