/** @file delegate.hpp
 *
 *  @brief  non-repeated deleate implementation detail
 *
 *  @author JaeWook Choi
 *  @version 1.22
 *
 * This software is provided "as is" without express or implied warranty, and with
 * no claim as to its suitability for any purpose.
 *
 **/
#if !defined(__DELEGATE_HPP__INCLUDED__)
#define __DELEGATE_HPP__INCLUDED__

#if !defined(FD_EXTRACT_SIMPLIFIED)

#include <memory>

#endif

// ================================================================================

//
// Repeater macro to generate template/function parameters/arguments
//
// AGM::LibReflection: A reflection library for C++.
// ( http://codeproject.com/library/libreflection.asp )
//

#define __REPEAT0(M, C)
#define __REPEAT1(M, C)         M(1)
#define __REPEAT2(M, C)         __REPEAT1(M, C)  _##C M(2)
#define __REPEAT3(M, C)         __REPEAT2(M, C)  _##C M(3)
#define __REPEAT4(M, C)         __REPEAT3(M, C)  _##C M(4)
#define __REPEAT5(M, C)         __REPEAT4(M, C)  _##C M(5)
#define __REPEAT6(M, C)         __REPEAT5(M, C)  _##C M(6)
#define __REPEAT7(M, C)         __REPEAT6(M, C)  _##C M(7)
#define __REPEAT8(M, C)         __REPEAT7(M, C)  _##C M(8)
#define __REPEAT9(M, C)         __REPEAT8(M, C)  _##C M(9)
#define __REPEAT10(M, C)        __REPEAT9(M, C)  _##C M(10)
#define __REPEAT11(M, C)        __REPEAT10(M, C) _##C M(11)
#define __REPEAT12(M, C)        __REPEAT11(M, C) _##C M(12)
#define __REPEAT13(M, C)        __REPEAT12(M, C) _##C M(13)
#define __REPEAT14(M, C)        __REPEAT13(M, C) _##C M(14)
#define __REPEAT15(M, C)        __REPEAT14(M, C) _##C M(15)
#define __REPEAT16(M, C)        __REPEAT15(M, C) _##C M(16)
#define __REPEAT17(M, C)        __REPEAT16(M, C) _##C M(17)
#define __REPEAT18(M, C)        __REPEAT17(M, C) _##C M(18)
#define __REPEAT19(M, C)        __REPEAT18(M, C) _##C M(19)
#define __REPEAT20(M, C)        __REPEAT19(M, C) _##C M(20)
#define __REPEAT(N, M, C)       __REPEAT##N(M, C)

//various defs needed for parameters
#define ___COMMA__               ,
#define __TEMPLATE_PARAM__(N)   typename T##N
#define __TEMPLATE_ARG__(N)     T##N
#define __FUNCTION_PARAM__(N)   T##N p##N
#define __FUNCTION_ARG__(N)     p##N

#define __TEMPLATE_PARAM2__(N)  typename U##N
#define __TEMPLATE_ARG2__(N)    U##N
#define __FUNCTION_PARAM2__(N)  U##N q##N
#define __FUNCTION_ARG2__(N)    q##N

#if !defined(FD_DISABLE_DELEGATES)

#define ___SEMI_COLON__             ;
#define __DECL_MEMBER_VARIABLE__(N) T##N q##N
#define __CTOR_INITIALIZER__(N)     q##N(p##N)

#endif  // #if !defined(FD_DISABLE_DELEGATES)

// ----------------------------------------------------------------------

//
// example
//
// macro expansion for " __REPEAT(2, __TEMPLATE_PARAM__, __COMMA__) "
//
// __REPEAT(3, M, C)
// => __REPEAT3(M, C)
// => __REPEAT2(M, C) _##C M(3)
// => __REPEAT1(M, C) _##C M(2) _##C M(3)
// => M(1) _##C M(2) _##C M(3)
// => __TEMPLATE_PARAM__(1) __COMMA__ __TEMPLATE_PARAM__(2) __COMMA__ __TEMPLATE_PARAM__(3)
// => typename T1 , typename T2, typename T3
//
// macro expansion for " __REPEAT(3, __FUNCTION_ARG__, __COMMA__) "
//
// => M(1) _##C M(2) _##C M(3)
// => __FUNCTION_ARG__(1) __COMMA__ __FUNCTION_ARG__(2) __COMMA__ __FUNCTION_ARG__(3)
// => p1, p2, p3
//

// ================================================================================

#define FD_TPARMS(N)        __REPEAT(N, __TEMPLATE_PARAM__, __COMMA__)
// typename T1, typename T2, ... , typename Tn
#define FD_TARGS(N)         __REPEAT(N, __TEMPLATE_ARG__, __COMMA__)
// T1, T2, ... , Tn
#define FD_FPARMS(N)        __REPEAT(N, __FUNCTION_PARAM__, __COMMA__)
// T1 p1, T2 p2, ... , Tn pn
#define FD_FARGS(N)         __REPEAT(N, __FUNCTION_ARG__, __COMMA__)
// p1, p2, ... , pn

#define FD_TPARMS2(N)       __REPEAT(N, __TEMPLATE_PARAM2__, __COMMA__)
// typename U1, typename U2, ... , typename Un
#define FD_TARGS2(N)        __REPEAT(N, __TEMPLATE_ARG2__, __COMMA__)
// U1, U2, ... , Un
#define FD_FPARMS2(N)       __REPEAT(N, __FUNCTION_PARAM2__, __COMMA__)
// U1 q1, U2 q2, ... , Un qn
#define FD_FARGS2(N)        __REPEAT(N, __FUNCTION_ARG2__, __COMMA__)
// q1, q2, ... , qn

#if !defined(FD_DISABLE_DELEGATES)

#define FD_DECLMV(N)        __REPEAT(N, __DECL_MEMBER_VARIABLE__, __SEMI_COLON__)
#define FD_CTORINIT(N)      __REPEAT(N, __CTOR_INITIALIZER__, __COMMA__)

#endif  // #if !defined(FD_DISABLE_DELEGATES)

// ----------------------------------------------------------------------

//
// for member function call adapter
//
#define __REPEAT_SP0(M, C)
#define __REPEAT_SP1(M, C)
#define __REPEAT_SP2(M, C)      M(2)
#define __REPEAT_SP3(M, C)      __REPEAT_SP2(M, C)  _##C M(3)
#define __REPEAT_SP4(M, C)      __REPEAT_SP3(M, C)  _##C M(4)
#define __REPEAT_SP5(M, C)      __REPEAT_SP4(M, C)  _##C M(5)
#define __REPEAT_SP6(M, C)      __REPEAT_SP5(M, C)  _##C M(6)
#define __REPEAT_SP7(M, C)      __REPEAT_SP6(M, C)  _##C M(7)
#define __REPEAT_SP8(M, C)      __REPEAT_SP7(M, C)  _##C M(8)
#define __REPEAT_SP9(M, C)      __REPEAT_SP8(M, C)  _##C M(9)
#define __REPEAT_SP10(M, C)     __REPEAT_SP9(M, C)  _##C M(10)
#define __REPEAT_SP11(M, C)     __REPEAT_SP10(M, C) _##C M(11)
#define __REPEAT_SP12(M, C)     __REPEAT_SP11(M, C) _##C M(12)
#define __REPEAT_SP13(M, C)     __REPEAT_SP12(M, C) _##C M(13)
#define __REPEAT_SP14(M, C)     __REPEAT_SP13(M, C) _##C M(14)
#define __REPEAT_SP15(M, C)     __REPEAT_SP14(M, C) _##C M(15)
#define __REPEAT_SP16(M, C)     __REPEAT_SP15(M, C) _##C M(16)
#define __REPEAT_SP17(M, C)     __REPEAT_SP16(M, C) _##C M(17)
#define __REPEAT_SP18(M, C)     __REPEAT_SP17(M, C) _##C M(18)
#define __REPEAT_SP19(M, C)     __REPEAT_SP18(M, C) _##C M(19)
#define __REPEAT_SP20(M, C)     __REPEAT_SP19(M, C) _##C M(20)
#define __REPEAT_SP(N, M, C)    __REPEAT_SP##N(M, C)

#define FD_TPARMS_SP(N)     __REPEAT_SP(N, __TEMPLATE_PARAM__, __COMMA__)
// typename T2, typename T3, ... , typename Tn
#define FD_TARGS_SP(N)      __REPEAT_SP(N, __TEMPLATE_ARG__, __COMMA__)
// T2, T3, ... , Tn
#define FD_FPARMS_SP(N)     __REPEAT_SP(N, __FUNCTION_PARAM__, __COMMA__)
// T2 p2, T3 p3, ... , Tn pn
#define FD_FARGS_SP(N)      __REPEAT_SP(N, __FUNCTION_ARG__, __COMMA__)
// p2, p3, ... , pn

#define FD_TPARMS_SP2(N)     __REPEAT_SP(N, __TEMPLATE_PARAM2__, __COMMA__)
// typename U2, typename U3, ... , typename Un
#define FD_TARGS_SP2(N)      __REPEAT_SP(N, __TEMPLATE_ARG2__, __COMMA__)
// U2, U3, ... , Un
#define FD_FPARMS_SP2(N)     __REPEAT_SP(N, __FUNCTION_PARAM2__, __COMMA__)
// U2 q2, U3 q3, ... , Un qn
#define FD_FARGS_SP2(N)      __REPEAT_SP(N, __FUNCTION_ARG2__, __COMMA__)
// q2, q3, ... , qn

// ================================================================================

#define FD_JOIN(a, b)        FD_DO_JOIN(a, b)
#define FD_DO_JOIN(a, b)     FD_DO_JOIN2(a, b)
#define FD_DO_JOIN2(a, b)    a##b

#define COUNTOF_PVOID_OF_FUNCTION(fn)   (sizeof(fn) / sizeof(void *))

#undef  FD_NDEBUG
#ifdef  NDEBUG
#define FD_NDEBUG
#endif

#define FD_STATIC_ASSERT(expr)  FD_JOIN(typedef typename fd::util::t_assert<(bool)(expr)>::assertion failed, __LINE__)

#ifdef FD_NDEBUG
#define FD_PARAM_TYPE_CHK(type1, type2) ((void)0)
#else
#define FD_PARAM_TYPE_CHK(type1, type2) type1 param_chk = (type2)0; param_chk
#endif

#ifdef FD_NDEBUG
#define FD_PARAM_TYPE_CHK_OBJ_PTR(type1, typed_obj) ((void)0)
#else
#define FD_PARAM_TYPE_CHK_OBJ_PTR(type1, typed_obj) type1 param_chk = typed_obj; param_chk
#endif

#ifdef FD_NDEBUG
#define FD_PARAM_TYPE_CHK_OBJ_REF(type1, typed_obj) ((void)0)
#else
#define FD_PARAM_TYPE_CHK_OBJ_REF(type1, typed_obj) type1 param_chk = detail::select_obj_type_<T, U>::type::get_pointer_(typed_obj); param_chk
#endif

#if defined(ASSERT)
#define FD_ASSERT               ASSERT
#elif defined(ATLASSERT)
#define FD_ASSERT               ATLASSERT
#else
#if !defined(FD_EXTRACT_SIMPLIFIED)
#include <cassert>
#endif
#define FD_ASSERT(expr)         assert(expr)
#endif

namespace fd
{

namespace detail
{

// ================================================================================

//
// DefaultVoid - a workaround for 'void' templates in VC6.
//
//  - VC6 and earlier do not allow you to return 'void' from a function
//    => return 'void const *' instead
//
// Member Function Pointers and the Fastest Possible C++ Delegates By Don Clugston
// ( http://codeproject.com/cpp/delegate.asp )
//
//
#if defined(FD_NO_VOID_RETURN)
// VC6 workaround
typedef void const * DefaultVoid;
#else // #if defined(FD_NO_VOID_RETURN)
// On any other compiler, just use a normal void.
typedef void DefaultVoid;
#endif  // #if defined(FD_NO_VOID_RETURN)

// Translate from 'void' into 'DefaultVoid'
// Everything else is unchanged
template <class T>
struct VoidToDefaultVoid { typedef T type; };

template <>
struct VoidToDefaultVoid<void> { typedef DefaultVoid type; };

// ================================================================================

//
// The Safe Bool Idiom by Bjorn Karlsson
// ( http://www.artima.com/cppsource/safebool.html )
//
class safe_bool_base
{
public:
  typedef void (safe_bool_base::*bool_type)() const;
  void this_type_does_not_support_comparisons() const {}

protected:
  safe_bool_base() {}
  safe_bool_base(const safe_bool_base &) {}
  safe_bool_base& operator=(const safe_bool_base &) {return *this;}
  ~safe_bool_base() {}

};  // class safe_bool_base

template<typename T>
class safe_bool : public safe_bool_base
{
public:
  operator bool_type() const
  {
    T const * pT = static_cast<T const *>(this); pT;
    return !pT->empty()
      ? &safe_bool_base::this_type_does_not_support_comparisons
      : 0;
  }
protected:
  ~safe_bool() {}

};  // template<typename T> class safe_bool : public safe_bool_base

template<typename T, typename U>
void operator == (const safe_bool<T> & lhs, const safe_bool<U> & rhs)
{
  lhs.this_type_does_not_support_comparisons();
//  return false;
}

template<typename T,typename U>
void operator != (const safe_bool<T> & lhs,const safe_bool<U> & rhs)
{
  lhs.this_type_does_not_support_comparisons();
//  return false;
}

// ================================================================================

//
// tag for template specialization for free function (non-member function)
//
struct free_fn_tag;

// ================================================================================

#define NO_OBJECT ((void *)-1)

// ================================================================================

template<typename TMFn, typename TMFn_const, typename TFtorOp> inline
void chk_functor(TMFn *, TMFn_const *, TFtorOp const & ftor_op)
{
  ftor_op;
  enum { value = util::Is_same_type<TMFn, TFtorOp>::value || util::Is_same_type<TMFn_const, TFtorOp>::value };
  FD_STATIC_ASSERT(value);
}

template<typename TMFn_const, typename TFtorOp> inline
void chk_functor_const(TMFn_const *, TFtorOp const & ftor_op)
{
  ftor_op;
  enum { value = util::Is_same_type<TMFn_const, TFtorOp>::value };

  FD_STATIC_ASSERT(value);  // functor do not have const version of operator ()
}

// ================================================================================

template<typename T>
struct self_ref_type_
{
  inline static T * get_pointer_(T const & obj)
  {
    return const_cast<T *>(&obj);
  }
};

template<typename T>
struct smart_ptr_type_
{
  inline static typename T::element_type * get_pointer_(T & obj)
  {
    return get_pointer(obj);
  }
};

template<typename T, typename U>
struct select_obj_type_
{
  enum { Condition = util::Is_reference<T, U>::value || util::Is_reference<U, T>::value };
  typedef self_ref_type_<T> Then;
  typedef smart_ptr_type_<T> Else;

  typedef typename util::If< Condition, Then, Else >::Result type;

};  // template<typename T, typename U> struct select_obj_type_

// ================================================================================

template<typename Allocator>
class allocator_base
{
#if !defined(FD_CANT_REBIND_TYPE)
  typedef typename Allocator::template rebind<unsigned char>::other Alloc_;
#else
  typedef Allocator Alloc_;
#endif

  static Alloc_ & get_static_allocator_()
  {
    static Alloc_ alloc;
    return alloc;
  }

public:
  inline static void * allocate_(size_t n, void const * pInitHint = 0)
  {
    Alloc_ & alloc = get_static_allocator_();
    return alloc.allocate(n, pInitHint);
  }

  inline static void deallocate_(void * p, size_t n)
  {
    Alloc_ & alloc = get_static_allocator_();
    alloc.deallocate(static_cast<typename Alloc_::value_type *>(p), n);
  }

};  // template<typename Allocator> class allocator_base

} // namespace detail

} // namespace fd

#endif  // #if !defined(__DELEGATE_HPP__INCLUDED__)
