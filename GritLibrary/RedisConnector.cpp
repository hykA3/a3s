/*
	file : RedisConnector.cpp
	creator : kwon sin hyeok ( aria2060@gmail.com )
	last modify : 17.02.23
*/

#include "RedisConnector.h"
#include "TextSupport.h"
#include "Log.h"

namespace aria
{
	CRedisConnector::CRedisConnector()
	{
		_redisContext = NULL;
		_redisReply = NULL;

		_isPending = false;
	}


	CRedisConnector::~CRedisConnector()
	{
		Destroy();
	}

	bool CRedisConnector::Initialize()
	{
		if (_redisContext)
		{
			return false;
		}

		_redisContext = redisConnect("127.0.0.1", 6379);
		if (_redisContext == NULL || _redisContext->err)
		{
			std::string error = "Redis Connect Error : ";
			error += _redisContext->err;
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, error);
			return false;
		}
		
		_redisReply = (redisReply*)redisCommand(_redisContext, "Server_A3");
		if (!_redisReply || (_redisReply->type == REDIS_REPLY_ERROR))
		{
			GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, _redisReply->str);
			return false;
		}
		freeReplyObject(_redisReply);

		_isPending = true;	

		std::string connect = "RedisConnect : " + IntToString(_redisContext->fd);
		GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, connect.c_str());
		return true;
	}

	void CRedisConnector::Destroy()
	{
		if (_redisContext != NULL)
		{
			redisFree(_redisContext);
		}		

		if (_redisReply != NULL)
		{
			freeReplyObject(_redisReply);
		}
	}

	int CRedisConnector::SendQuery(const char* query, std::vector < std::map<std::string, std::string>>& result, bool autoRelease /*= true*/, int stack /*= 0*/)
	{
		if (_redisContext == NULL)
		{
			_isPending = true;
			return -1;
		}

		if (query == NULL)
		{
			_isPending = true;
			return -1;
		}

		_redisReply = (redisReply*)redisCommand(_redisContext, query);
		if (!_redisReply || _redisReply->type == REDIS_REPLY_ERROR)
		{
			TenLog("Redis Reply Error : %s", _redisReply->str);

			Destroy();
			Initialize();

			if (stack == 0)
			{
				_redisReply->integer = SendQuery(query, result, autoRelease, stack + 1);

				if (autoRelease)
					_isPending = true;
			}
			return -1;
		}

		else if( _redisReply->type == REDIS_REPLY_ARRAY)
		{
			std::map<std::string, std::string > rowResult;
			for (int i = 0; i < _redisReply->elements; i++)
			{
				std::string code;
				std::string name;

				code = _redisReply->element[i]->integer;
				name = _redisReply->element[i]->str;
				
				rowResult.insert(make_pair(code, name));
			}

			result.push_back(rowResult);
			freeReplyObject(_redisReply);
		}
		
		else
		{
			//?
			freeReplyObject(_redisReply);
		}

		return true;

	}
}
