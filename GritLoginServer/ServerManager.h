//
// file : IOCP.cpp
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.16
//

#pragma once

class CServerBind;
namespace aria
{ 
	class CConnection;
	class CNetworkResponseIntoJson;
}

#define GLOBAL_SERVER_MANAGER	CServerManager::GetInstance()

class CServerManager : public aria::CSingleton<CServerManager>
{
public:
	CServerManager(void);
	virtual ~CServerManager(void);

	bool	Initialize() { return true; }
	void	Destroy() {}

	bool	CreateBindServer(InitConfig& config, int count);

	bool	AddBindServer(CServerBind* server);
	bool	RemoveBindServer(CServerBind* server);

	bool	SendServerListToUser(aria::CNetworkResponseIntoJson* response);
	bool	RequestServerConnectToUser(aria::CNetworkResponseIntoJson* response, const char* serverName);

	bool	SendDataTable();

protected:
	//바인딩된 메인 서버
	aria::SafeMap<DWORD, CServerBind*>	_serverMap;

	CServerBind*						_server;

	aria::CCriticalSection				_cs;
};

