/*
	file : User.h
	creator : kwon sin hyeok ( aria2060@gmail.com )
	last modify : 17.03.07
*/
#include "stdafx.h"
#include "UserManager.h"
#include "Packet.h"

#include "../GritLibrary/NetworkRequestIntoJson.h"
#include "../GritLibrary/Log.h"
#include "../GritLibrary/IOCPServer.h"

CUserManager::CUserManager()
{
	_users = NULL;
}


CUserManager::~CUserManager()
{
	SAFE_DELETE_ARRAY(_users);
}

bool CUserManager::CreateUser(InitConfig& config, int count)
{
	_users = new CUser[count];

	for (int i = 0; i < count; ++i)
	{
		config._index = i;

		if (_users[i].CreateConnection(config) == false)
		{
			return false;
		}
	}

	return true;
}

bool CUserManager::AddUser(CUser* user)
{
	_userMap.Lock();

	auto itr = _userMap.GetMap().find(user->GetUserIndex());

	if (itr != _userMap.GetMap().end())
	{
		_userMap.Unlock();
		return false;
	}

	_users->SetUserIndex(GLOBAL_IOCP_SERVER->GeneratePrivateKey());

	_userMap.GetMap().insert(std::make_pair(_users->GetAccountIndex(), user));
	_userMap.Unlock();

	return true;
}

bool CUserManager::DeleteUser(CUser* user)
{
	if (user == NULL)
	{
		return false;
	}

	_userMap.Erase(user->GetAccountIndex());

	GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, "User(%d) Connection Off", user->GetAccountIndex());


	return true;
}

void CUserManager::SendAccountCheckResult(CUser* user, int resultCode)
{
	aria::CNetworkRequestIntoJson request;

	request.AddParam("resultCode", resultCode);
	request.AddParam("accountIndex", user->GetAccountIndex());

	std::string jsonString = request.GetJsonString();
	int size = sizeof(PacketBase) + jsonString.length();
	char* data = user->PrepareSendPacket(size + 1);

	PacketBase* base = (PacketBase*)data;
	base->_packetLength = size + 1;
	base->_packetType = request.GetRequestType();

	memcpy(data + sizeof(PacketBase), jsonString.c_str(), jsonString.length());

	data[size] = '\0';

	user->SendPost(size + 1);
}
