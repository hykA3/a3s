/*
	file : ApiLog.h
	creator : kwon sin hyeok ( aria2060@gmail.com )
	last modify : 17.02.07
*/
#pragma once

#include "../GritLibrary/define.h"

class CApiLog
{
public:
	CApiLog();
	~CApiLog();

	void			SetApiLog(int logType, std::string apiLog);

protected:
	std::string		_log;

};

