/*
	file : User.h
	creator : kwon sin hyeok ( aria2060@gmail.com )
	last modify : 17.03.07
*/

#pragma once

#include "../GritLibrary/Library.h"
#include "../GritLibrary/Singleton.h"
#include "../GritLibrary/Queue.h"

#include "User.h"

#define GLOBAL_USER_MANAGER	CUserManager::GetInstance()

class CUserManager : public aria::CSingleton<CUserManager>
{
public:
	CUserManager();
	virtual	~CUserManager();

	virtual bool Initialize() { return true; }
	virtual void Destroy() {}

	bool	CreateUser(InitConfig& config, int count);
	bool	AddUser(CUser* user);
	bool	DeleteUser(CUser* user);

	void	SendAccountCheckResult(CUser* user, int resultCode);

protected:
	aria::SafeMap<DWORD, CUser*>	_userMap;
	aria::CCriticalSection			_cs;

	CUser*							_users;
};

