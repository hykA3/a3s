//
// file : Packet.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.14
//

#pragma once
#pragma pack(1)

enum PacketType
{
	CreateAccountPacketType = 10,
	CheckAccountPacketType = 11,
	RequestServerListPacketType = 12,
	RequestServerConnectPacketType = 13,
	ConnectLoginServerType = 41,
	UploadDataTablePacketType = 51
};

struct PacketBase
{
	unsigned int _packetLength;
	unsigned short _packetType;
};

struct LoginPacket : public PacketBase
{
	std::string _email;	//64 Byte, 로그인할 email 
	std::string _password;	//16 Byte, 로그인할 password
};

struct LoginResultPacket : public PacketBase
{
	enum Result {
		RESULT_SUCCESS = 0,
		RESULT_NOT_FOUND_ACCOUNT,
	};
	int _resultCode;
	int _accountIndex;
};

struct AccountCreatePacket : public PacketBase
{
	char _newAccount[64];
	char _newPassword[16];
};

struct AccountCreateResultPacket : public PacketBase
{
	enum Result {
		RESULT_SUCCESS = 0,
		RESULT_ALREADY_EXIST,
		RESULT_FAILED
	};
	int _resultCode;
};

struct ConnectLoginServerPacket : public PacketBase
{
	char _serverName[32];
};

struct RequestServerListPacket : public PacketBase
{

};

struct RequestServerListResultPacket : public PacketBase
{
	int _serverCount;
};

struct RequestServerConnectPacket : public PacketBase
{
	char _serverName[32];
};

struct RequestServerConnectResultPacket : public PacketBase
{
	char _serverIP[16];
	int _serverPort;
};
