//
// file : IOCP.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.16
//
#pragma once

#include "../GritLibrary/IOCPServer.h"
#include "../GritLibrary/NetworkRequestIntoJson.h"
#include "../GritLibrary/define.h"

using namespace aria;

class CIOCP : public aria::CIOCPServer
{
public:
	CIOCP();
	virtual ~CIOCP();

	union FuncProcess
	{
		void(*funcProcessPacket)(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data);

		FuncProcess()
		{
			funcProcessPacket = NULL;
		}
	};

	bool			Initialize();
	bool			StartServer();


	virtual bool OnConnect(CConnection* connection) override;


	virtual bool OnAccept(CConnection* connection) override;

	virtual bool OnRecv(CConnection* connection, DWORD size, char* recvedMsg) override;


	virtual bool OnRecvImmediately(CConnection* connection, DWORD size, char* recvedMsg) override;

	virtual void OnClose(CConnection* connection) override;


	virtual bool OnSystemMsg(CConnection* connection, DWORD msgType, LPARAM lParam) override;

protected:
	bool			CreateBindServerListenSocket(InitConfig& config);

	SOCKET			_bindServerListenSocket;
	FuncProcess		_funcProcess[100];
};

