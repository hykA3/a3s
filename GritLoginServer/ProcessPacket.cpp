#include "stdafx.h"
#include "ProcessPacket.h"
#include "ServerManager.h"
#include "ServerBind.h"
#include "User.h"
#include "UserManager.h"
#include "Packet.h"

#include "../GritLibrary/Log.h"
#include "../GritLibrary/NetworkRequestIntoJson.h"
#include "../GritLibrary/SqlConnectorPool.h"

CProcessPacket::CProcessPacket()
{
}


CProcessPacket::~CProcessPacket()
{
}

void CProcessPacket::CheckLoginAccount(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data)
{
	if (response == NULL || data == NULL)
		return;

	LoginPacket loginData;
	loginData._email = PARSE_STRING(*data, "email");
	loginData._password = PARSE_STRING(*data, "password");

	if (loginData._email.length() <= 0)
	{
		response->SendErrorPacket("emptyInformation");
		return;;
	}

	if (loginData._password.length() <= 0)
	{
		response->SendErrorPacket("emptyInformation");
		return;
	}

	char query[512];
	sprintf_s(query,
		"SELECT accountIndex from account where email = '%s' and password = PASSWORD('%s')",
		loginData._email.c_str(), loginData._password.c_str()
	);

	CSqlResult result;

	if (GLOBAL_SQL_CONNECTOR->NetworkRequest(query, result) == -1)
	{
		response->SendErrorPacket("serverQueryError");
		return;
	}

	if (result.empty())
	{
		response->SendErrorPacket("notFoundEmail");
		return;
	}

	CSqlRowResult &rowResult = result[0];

	CSqlRowResult::iterator itr;

	itr = rowResult.find("accountIndex");

	if (itr == rowResult.end())
	{
		response->SendErrorPacket("notFoundEmail");
		return;
	}

	int accountIndex = StringToInt(itr->second);

	//((CUser*)response->GetConnection())->SetAccountIndex(accountIndex);
	//GLOBAL_USER_MANAGER->SendAccountCheckResult((CUser*)response, accountIndex);
	//GLOBAL_USER_MANAGER->AddUser((CUser*)response->GetConnection());
	response->AddParam("accountIndex", "result", accountIndex);

	response->SendSuccessPacket();
}

void CProcessPacket::CreateAccount(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data)
{
	char query[512];

	std::string newEmail = PARSE_STRING(*data, "email");
	std::string newNickName = PARSE_STRING(*data, "nickName");
	std::string newPassword = PARSE_STRING(*data, "password");

	sprintf_s(query, "insert into aria.account(email,nickName,password) select '%s','%s', PASSWORD('%s') from dual where not exists (select email from aria.account where email = '%s')",
		newEmail.c_str(), newNickName.c_str(), newPassword.c_str(), newEmail.c_str());

	CSqlResult result;

	int row = GLOBAL_SQL_CONNECTOR->NetworkRequest(query, result);

	if (row == 0)
	{
		//이미 계정이 있음.
		response->SendErrorPacket("alreadyExist");
		return;
	}

	response->SendSuccessPacket();
}

void CProcessPacket::RequestServerList(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data)
{
	GLOBAL_SERVER_MANAGER->SendServerListToUser(response);
}

void CProcessPacket::RequestServerConnect(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data)
{
	if (PARSE_VALID(*data, "serverName") == false)
	{
		response->SendErrorPacket("invalidServerName");
		return;
	}

	GLOBAL_SERVER_MANAGER->RequestServerConnectToUser(response, PARSE_STRING(*data, "serverName"));
}

void CProcessPacket::ConnectServer(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data)
{
	if (PARSE_VALID(*data, "serverName") == false)
		return;

	CServerBind* bindServer = dynamic_cast<CServerBind*>(response->GetConnection());

	if (bindServer == NULL)
	{
		return;
	}

	bindServer->SetLoginServerName(PARSE_STRING(*data, "serverName"));

	if (GLOBAL_SERVER_MANAGER->AddBindServer(bindServer))
	{

	}
}

void CProcessPacket::UploadDataTable(char* data, DWORD size)
{
	char* dataTable = data;;
	char* query = new char[size + 100];
	memset(query, 0, size + 100);
	sprintf_s(query, size + 100, "insert into GameData (data) VALUE ('%s')", dataTable);

	CSqlResult result;
	int row = GLOBAL_SQL_CONNECTOR->NetworkRequest(query, result);

	delete[] query;

	if (row <= 0)
	{
		GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_HIGH, "DataTable Insert 실패");
	}

	GLOBAL_SERVER_MANAGER->SendDataTable();
}