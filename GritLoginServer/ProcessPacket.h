//
// file : ProcessPacket.h
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 17.03.07
//

#pragma once

#include "../GritLibrary/Rapidjson/document.h"
#include "../GritLibrary/NetworkRequestIntoJson.h"

class CProcessPacket
{
public:
	CProcessPacket();
	virtual ~CProcessPacket();

	static void CheckLoginAccount(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data);
	static void CreateAccount(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data);
	static void RequestServerList(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data);

	static void RequestServerConnect(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data);

	static void ConnectServer(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data);
	static void UploadDataTable(char* data, DWORD size);
};

