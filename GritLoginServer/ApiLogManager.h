/*
	file : ApiLogManager.h
	creator : kwon sin hyeok ( aria2060@gmail.com )
	last modify : 17.02.07
*/
#pragma once
#include "ApiLog.h"
#include "../GritLibrary/define.h"

#define APILOGMANAGER	CApiLogManager::GetInstance()

class CApiLogManager : public aria::CSingleton<CApiLogManager>
{
public:
	CApiLogManager();
	~CApiLogManager();

	void	SetLog(int logType, std::string apiLog);
	void	ShowLog();

	bool	Initialize() { return true; }
	void	Destroy() {}

protected:
	CApiLog*		_apiLog;
};

