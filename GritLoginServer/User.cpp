#include "stdafx.h"
#include "User.h"
#include "UserManager.h"

CUser::CUser()
{
	_accountIndex = -1;
}


CUser::~CUser()
{
}

bool CUser::CloseConnection(bool init /*= true*/, bool force /*= false*/)
{
	if (_accountIndex >= -1)
	{
		GLOBAL_USER_MANAGER->DeleteUser(this);
		_accountIndex = -1;
	}

	return CConnection::CloseConnection(init, force);
}
