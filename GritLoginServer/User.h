/*
	file : User.h
	creator : kwon sin hyeok ( aria2060@gmail.com )
	last modify : 17.03.07
*/

#pragma once

#include "../GritLibrary/Connection.h"

class CUser : public aria::CConnection
{
public:
	CUser();
	virtual ~CUser();

	virtual bool CloseConnection(bool init = true, bool force = false) override;

protected:
	DEFINE_GET_SET_ACCESS_WITH_VAR(int, UserIndex, _userIndex);
	DEFINE_GET_SET_ACCESS_WITH_VAR(int, AccountIndex, _accountIndex);
};

