//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// GritLoginServer.rc에서 사용되고 있습니다.
//
#define IDC_MYICON                      2
#define IDD_GRITLOGINSERVER_DIALOG      102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_GRITLOGINSERVER             107
#define IDI_SMALL                       108
#define IDC_GRITLOGINSERVER             109
#define IDR_MAINFRAME                   128
#define IDC_STATIC                      -1

#define BTN_START						201
#define BTN_STOP						202
#define EDIT_LOG						301

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
