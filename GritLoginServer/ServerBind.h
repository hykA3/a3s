//
// file : ServerBind.cpp
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.14
//
#pragma once

#include "../GritLibrary/Connection.h"

class CServerBind : public aria::CConnection
{
public:
	CServerBind(void);
	virtual ~CServerBind(void);

protected:
	DEFINE_GET_SET_ACCESS_WITH_VAR(int, ServerIndex, _serverIndex);
	DEFINE_GET_SET_ACCESS_WITH_VAR(std::string, LoginServerName, _serverName);
};

