//
// file : IOCP.cpp
// creator : kwon sin hyeok ( aria2060@gmail.com )
// last modify : 16.11.16
//

#include "stdafx.h"
#include "IOCP.h"
#include "ServerBind.h"
#include "Packet.h"
#include "ProcessPacket.h"
#include "ServerManager.h"
#include "UserManager.h"

#include "../GritLibrary/Connection.h"
#include "../GritLibrary/NetworkRequestIntoJson.h"


CIOCP::CIOCP()
{
	if (CIOCPServer::_instance == NULL)
	{
		CIOCPServer::_instance = this;
	}

	_bindServerListenSocket = INVALID_SOCKET;

	ZeroMemory(_funcProcess, sizeof(_funcProcess));
	Initialize();
}

CIOCP::~CIOCP()
{
 	GLOBAL_USER_MANAGER->DestroyInstance();
 	GLOBAL_SERVER_MANAGER->DestroyInstance();

	closesocket(_bindServerListenSocket);

	printf("Destroy Server\n");
}

bool CIOCP::OnConnect(CConnection* connection)
{
	throw std::logic_error("The method or operation is not implemented.");
}

bool CIOCP::OnAccept(CConnection* connection)
{
	SOCKET socket = connection->GetSocket();

	std::string temp = "LoginIOCPServer::OnAccept(), socketId : " + IntToString(socket);
	GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, temp.c_str());

	return true;
}

bool CIOCP::OnRecv(CConnection* connection, DWORD size, char* recvedMsg)
{
	return true;
}

bool CIOCP::OnRecvImmediately(CConnection* connection, DWORD size, char* recvedMsg)
{
	unsigned short type;
	CopyMemory(&type, recvedMsg + 4, 2);

// 	if (type == UploadDataTablePacketType)
// 	{
// 		CProcessPacket::UploadDataTable(recvedMsg + sizeof(PacketBase), size);
// 
// 		return true;
// 	}

	if (type >= 0 && type < 100 && NULL != _funcProcess[type].funcProcessPacket)
	{
		CNetworkResponseIntoJson json(connection);
		json.SetRequestType(type);

		CJsonObject* resultObject = new CJsonObject();
		json.AddParam("result", resultObject, false);

		rapidjson::Document document;

		if (document.Parse<0>(recvedMsg + sizeof(PacketBase)).HasParseError())
		{
			return true;
		}

		_funcProcess[type].funcProcessPacket(&json, &document);

		return true;
	}

	return false;
}

void CIOCP::OnClose(CConnection* connection)
{
	CServerBind* bindServer = dynamic_cast<CServerBind*>(connection);
	if (bindServer)
	{
		GLOBAL_SERVER_MANAGER->RemoveBindServer(bindServer);
	}
	std::string temp = "LoginIOCPServer::OnClose(), socketId : " + IntToString(connection->GetSocket());
	GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, temp.c_str());
}

bool CIOCP::OnSystemMsg(CConnection* connection, DWORD msgType, LPARAM lParam)
{
	return true;
}

bool CIOCP::StartServer()
{
	InitConfig initConfig;

	initConfig._processPacketCount = 1000;

	initConfig._sendBufferCount = 8;
	initConfig._recvBufferCount = 2;

	initConfig._sendBufferSize = 1024;
	initConfig._recvBufferSize = 1024;

	initConfig._serverPort = 9090;
	initConfig._maxConnectionCount = 100;

	initConfig._serverWorkerThreadCount = 4;
	initConfig._serverProcessThreadCount = 4;

	if (CIOCPServer::StartServer(initConfig) == false)
	{
		return false;
	}

	initConfig._serverPort = 9091;

	if (CreateBindServerListenSocket(initConfig) == false)
	{
		return false;
	}
 	//유저의 접속을 위한..
	GLOBAL_USER_MANAGER->CreateUser(initConfig, 100);

	//서버의 접속을 위한..
	initConfig._socketListener = _bindServerListenSocket;


	initConfig._sendBufferCount = 3;
	initConfig._recvBufferCount = 3;

	initConfig._sendBufferSize = 10485760;
	initConfig._recvBufferSize = 10485760;
	GLOBAL_SERVER_MANAGER->CreateBindServer(initConfig, 5);
	return true;
}

bool CIOCP::Initialize()
{
	_funcProcess[PacketType::CreateAccountPacketType].funcProcessPacket = CProcessPacket::CreateAccount;
	_funcProcess[PacketType::CheckAccountPacketType].funcProcessPacket = CProcessPacket::CheckLoginAccount;
	_funcProcess[PacketType::RequestServerListPacketType].funcProcessPacket = CProcessPacket::RequestServerList;
	_funcProcess[PacketType::ConnectLoginServerType].funcProcessPacket = CProcessPacket::ConnectServer;
	_funcProcess[PacketType::RequestServerConnectPacketType].funcProcessPacket = CProcessPacket::RequestServerConnect;
	return true;
}

bool CIOCP::CreateBindServerListenSocket(InitConfig& config)
{
	SOCKADDR_IN addr;

	_bindServerListenSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_IP, NULL, 0, WSA_FLAG_OVERLAPPED);

	if (_bindServerListenSocket == INVALID_SOCKET)
	{
		GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "LoginIOCPServer::CreateBindServerListenSocket(), 리슨 소켓 생성 실패..");

		return false;
	}

	addr.sin_family = AF_INET;
	addr.sin_port = htons(config._serverPort);
	addr.sin_addr.S_un.S_addr = htonl(INADDR_ANY); //아무 아이피나 받겟다.

	int result = bind(_bindServerListenSocket, (sockaddr*)& addr, sizeof(addr));

	if (result == SOCKET_ERROR)
	{
		GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "LoginIOCPServer::CreateBindServerListenSocket(), 바인드 실패");
		return false;
	}

	result = listen(_bindServerListenSocket, 50);

	if (result == SOCKET_ERROR)
	{
		GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "LoginIOCPServer::CreateBindServerListenSocket(), 리슨 실패");
		return false;
	}

	HANDLE iocpHandle = CreateIoCompletionPort((HANDLE)_bindServerListenSocket, _workerIOCPHandle, (DWORD)0, 0);

	if (iocpHandle == NULL || _workerIOCPHandle != iocpHandle)
	{
		GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_NORMAL, "LoginIOCPServer::CreateBindServerListenSocket(), CreateIoCompletionPort 실패");
		return false;
	}

	return true;
}
