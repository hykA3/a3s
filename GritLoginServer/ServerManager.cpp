#include "stdafx.h"
#include "ServerManager.h"
#include "ServerBind.h"
#include "Packet.h"
#include "../GritLibrary/NetworkRequestIntoJson.h"


CServerManager::CServerManager()
{
	_server = NULL;
}

CServerManager::~CServerManager()
{
	SAFE_DELETE_ARRAY(_server);
}

bool CServerManager::CreateBindServer(InitConfig& config, int count)
{
	_server = new CServerBind[count];

	for (int i = 0; i < count; ++i)
	{
		config._index = i;

		if (_server[i].CreateConnection(config) == false)
		{
			return false;
		}
	}

	return true;
}

bool CServerManager::AddBindServer(CServerBind* server)
{
	_serverMap.Lock();

	auto itr = _serverMap.GetMap().find(server->GetServerIndex());

	if (itr != _serverMap.GetMap().end())
	{
		_serverMap.Unlock();
		return false;
	}


	GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, "LoginServerManager::AddBindServer(), 로그인 서버 매니저에 등록");
	server->SetServerIndex(GLOBAL_IOCP_SERVER->GeneratePrivateKey());
	server->SetLoginServerName("gameServer");

	_serverMap.GetMap().insert(std::make_pair(server->GetServerIndex(), server));

	_serverMap.Unlock();

	return true;
}

bool CServerManager::RemoveBindServer(CServerBind* user)
{
	_serverMap.Erase(user->GetServerIndex());

	return true;
}

bool CServerManager::SendServerListToUser(aria::CNetworkResponseIntoJson* response)
{
	if (response == NULL)
		return false;

	_serverMap.Lock();

	auto itr = _serverMap.GetMap().begin();
	auto itrEnd = _serverMap.GetMap().end();

	for (; itr != itrEnd; ++itr)
	{
		std::string serverName = itr->second->GetLoginServerName();

		aria::CJsonObject* server = new aria::CJsonObject();

		server->AddParam("serverName", serverName.c_str());
		response->AddParam("serverList", "result", server, true);
	}

	_serverMap.Unlock();

	response->SendSuccessPacket();
	return true;
}

bool CServerManager::RequestServerConnectToUser(aria::CNetworkResponseIntoJson* response, const char* serverName)
{
	if (response == NULL || serverName == NULL)
		return false;

	_serverMap.Lock();

	auto itr = _serverMap.GetMap().begin();
	auto itrEnd = _serverMap.GetMap().end();


	for (; itr != itrEnd; ++itr)
	{
		if (itr->second->GetLoginServerName().compare(serverName) == 0)
		{
			//해당 서버로 이 유저를 인증 시켜야 하는데..귀찮으니 일단, 바로 서버의 정보를 내려 주자

			response->AddParam("ip", "result", itr->second->GetConnectionIP());
			response->AddParam("port", "result", 8080);

			response->SendSuccessPacket();
			break;
		}
	}

	_serverMap.Unlock();

	return true;
}

bool CServerManager::SendDataTable()
{
	_serverMap.Lock();

	auto itr = _serverMap.GetMap().begin();
	auto itrEnd = _serverMap.GetMap().end();


	for (; itr != itrEnd; ++itr)
	{
		aria::CNetworkResponseIntoJson json(itr->second);

		json.SetRequestType(UploadDataTablePacketType);

		std::string jsonString = json.GetJsonString();

		int size = sizeof(PacketBase) + jsonString.length();
		char* packet = itr->second->PrepareSendPacket(size + 1);

		packet[size] = '\0';

		PacketBase* base = (PacketBase*)packet;
		base->_packetLength = size + 1;
		base->_packetType = UploadDataTablePacketType;

		CopyMemory(packet + sizeof(PacketBase), jsonString.c_str(), jsonString.length());

		itr->second->SendPost(size + 1);
	}

	_serverMap.Unlock();

	return true;
}
