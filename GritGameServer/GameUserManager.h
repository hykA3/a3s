#pragma once

#include "GameUser.h"

#include "../GritLibrary/Library.h"
#include "../GritLibrary/Rapidjson/document.h"
#include "../GritLibrary/Queue.h"

#include "GameUser.h"

#define GLOBAL_GAME_USERMANAGER CGameUserManager::GetInstance()

#define BROADCAST_THREADMAX	4

class CGameUserManager : public aria::CSingleton<CGameUserManager>
{
public:
	CGameUserManager();
	virtual ~CGameUserManager();

	bool	Initialize() { return true; };
	void	Destroy(){};
	
	//note - user ����
	bool		CreateUser(InitConfig& config, int count);
	bool		AddUser(CGameUser* user, CharacterInfo* userInfo);
	bool		DeleteUser(CGameUser* user);

	bool		UpdateUser(CharacterInfo* characterInfo);

protected:
	CGameUser*								_users;
	HANDLE									_userDataSaveThread;
	aria::CCriticalSection					_cs;

	aria::SafeMap<int, CGameUser*>			_userList;
	aria::SafeMap<DWORD, int>				_convertorIndex;

	//note - user information broadcast thread
public:
	bool		CreateBroadCastThread();
	void		BroadCastCharacter(/*aria::CNetworkResponseIntoJson* response, int index*/);

protected:
	bool		_broadCastTheadRun;
	HANDLE		_broadThreadHandle;
};

