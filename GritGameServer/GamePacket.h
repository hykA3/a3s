#pragma once
#pragma pack(1)

#include <iostream>
#include "../GritLibrary//Rapidjson/document.h"
#include "../GritLibrary/NetworkRequestIntoJson.h"

enum PacketType
{
	NewPlayer = 11,
	LoginSuccessType = 12,
	LoginFailType = 13,

	LogoutPacketType = 14,

	CharacterUpdate = 21,
	ChararterBroadCast = 22,

	CreateRoomPacketType = 31,

	ConnectLoginServerType = 41,

	UploadDataTablePacketType = 51,

	ChattingPacketType = 82
};

struct PacketBase
{
	unsigned int _packetLength;
	unsigned short _packetType;
};

struct CharacterInfo
{
	short		_characterType;
	short		_weaponType;
	int			_state;
	int			_index;

	std::string	_nickName;

	float		_x;
	float		_y;
	float		_z;

	float		_roll;
	float		_pitch;
	float		_yaw;
};

