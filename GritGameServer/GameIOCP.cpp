#include "stdafx.h"
#include "GameIOCP.h"
#include "GameUserManager.h"
#include "GamePacket.h"
#include "GameProcessPacket.h"

#include "../GritLibrary/NetworkRequestIntoJson.h"
#include "../GritLibrary/Queue.h"
#include "../GritLibrary/Connection.h"

CGameIOCP::CGameIOCP()
{
	if (CIOCPServer::_instance == NULL)
	{
		CIOCPServer::_instance = this;
	}

	_serverName = "GameServer";

	Initialize();

	_loginServerSocket = INVALID_SOCKET;
	_loginServerConnection = NULL;
}


CGameIOCP::~CGameIOCP()
{
	for (int i = 0; i < 100; i++)
	{
		_funcProcess[i].funcProcessPacket = NULL;
	}

	SAFE_DELETE(_loginServerConnection);
	closesocket(_loginServerSocket);

	CGameUserManager::DestroyInstance();
}

bool CGameIOCP::StartServer(std::string serverName, std::string ip, int port)
{
	char outStr[1024] = { 0 };

	//srand((unsigned)time(NULL));

	_serverName = serverName;

	InitConfig initConfig;

	//임시 설정값..ini 파일을 따로 빼두는게 좋을듯 함.
	initConfig._processPacketCount = 1000;

	initConfig._sendBufferCount = 8;
	initConfig._recvBufferCount = 2;

	initConfig._sendBufferSize = 5120; //1kb
	initConfig._recvBufferSize = 5120;

	initConfig._serverPort = 8081;
	initConfig._maxConnectionCount = 100;

	initConfig._serverWorkerThreadCount = 4;
	initConfig._serverProcessThreadCount = 4;

	if (aria::CIOCPServer::StartServer(initConfig, false) == false)
	{
		return false;
	}

	// 	GameDataManager::GetInstance();
	// 	NpcManager::GetInstance()->Initialize();

	initConfig._serverPort = port;

	//로그인서버와의 연결이 필요 없다. 
	//추후 레디스 서버로 연결요청
// 	if (ConnectLoginServer(ip, port) == false)
// 
// 	{
// 		return false;
// 	}

	GLOBAL_GAME_USERMANAGER->CreateUser(initConfig, 100);

	return true;
}

bool CGameIOCP::OnConnect(aria::CConnection* connection)
{
	if (connection == _loginServerConnection)
	{
		aria::CNetworkRequestIntoJson json;
		json.AddParam("serverName", _serverName.c_str());
		json.SetRequestType(ConnectLoginServerType);

		std::string jsonString = json.GetJsonString();

		int size = sizeof(PacketBase) + jsonString.length();

		char* buffer = connection->PrepareSendPacket(size + 1);

		if (buffer == NULL)
		{
			return false;
		}

		buffer[size] = '\0';

		PacketBase* base = (PacketBase*)buffer;

		base->_packetLength = size + 1;
		base->_packetType = ConnectLoginServerType;


		CopyMemory(buffer + sizeof(PacketBase), jsonString.c_str(), jsonString.length());

		connection->SendPost(size + 1);

		GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, "GritIOCPServer::OnConnect(), Connection 패킷 전송");
	}

	return true;
}

bool CGameIOCP::OnAccept(aria::CConnection* connection)
{
	SOCKET socket = connection->GetSocket();

	std::string temp = "gameIOCPServer::OnAccept(), socketId : " + IntToString(socket);
	GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, temp.c_str());

	return true;
}

bool CGameIOCP::OnRecv(aria::CConnection* connection, DWORD size, char* recvedMsg)
{
	return true;
}

bool CGameIOCP::OnRecvImmediately(aria::CConnection* connection, DWORD size, char* recvedMsg)
{
	unsigned short type;
	CopyMemory(&type, recvedMsg + 4, 2);

	if (type >= 0 && type < 100 && NULL != _funcProcess[type].funcProcessPacket)
	{
		aria::CNetworkResponseIntoJson json(connection);
		json.SetRequestType(type);

		aria::CJsonObject* resultObject = new aria::CJsonObject();
		json.AddParam("result", resultObject, false);

		rapidjson::Document document;

		if (document.Parse<0>(recvedMsg + sizeof(PacketBase)).HasParseError())
		{
			return true;
		}

		_funcProcess[type].funcProcessPacket(&json, &document);
		return true;
	}

	return true;
}

void CGameIOCP::OnClose(aria::CConnection* connection)
{
	SOCKET socket = connection->GetSocket();
	CGameUser* user = dynamic_cast<CGameUser*>(connection);
	if (user)
	{
		GLOBAL_GAME_USERMANAGER->DeleteUser(user);
	}
	std::string temp = "gameIOCPServer::OnClose(), socketId : " + IntToString(socket);
	GLOBAL_LOG_HELPER->Normal(LOG_INFO_TYPE_NORMAL, temp.c_str());
}

bool CGameIOCP::OnSystemMsg(aria::CConnection* connection, DWORD msgType, LPARAM lParam)
{
	return true;
}

void CGameIOCP::SetProcessFunc()
{
	_funcProcess[PacketType::NewPlayer].funcProcessPacket = CGameProcessPacket::NewUser;
	_funcProcess[PacketType::CharacterUpdate].funcProcessPacket = CGameProcessPacket::UpdateUser;
}

bool CGameIOCP::ConnectLoginServer(std::string ip, int port)
{
	SAFE_DELETE(_loginServerConnection);

	_loginServerConnection = new aria::CConnection();

	if (_loginServerConnection)
	{
		InitConfig initConfig;

		initConfig._sendBufferCount = 2;
		initConfig._recvBufferCount = 2;

		initConfig._sendBufferSize = 10485760;
		initConfig._recvBufferSize = 10485760;

		initConfig._serverPort = 9091;
		initConfig._socketListener = GetListenSocket();

		if (_loginServerConnection->CreateConnection(initConfig, false) == false)
		{
			return false;
		}

		if (_loginServerConnection->BindConnectSock(ip.c_str(), port, _workerIOCPHandle, false) == false)
		{
			return false;
		}
	}

	return true;
}

bool CGameIOCP::Initialize()
{
	SetProcessFunc();
	return true;
}

void CGameIOCP::Destroy()
{

}
