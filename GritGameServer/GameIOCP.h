#pragma once

#include "../GritLibrary/IOCPServer.h"
#include "../GritLibrary/Rapidjson/document.h"

namespace aria
{
	class CConnection;
	class CNetworkResponseIntoJson;
}

class CGameIOCP : public aria::CIOCPServer
{
public:
	CGameIOCP();
	virtual ~CGameIOCP();

	bool				Initialize();
	void				Destroy();

	virtual bool		StartServer(std::string serverName, std::string ip, int port);
	virtual bool		OnConnect(aria::CConnection* connection);
	virtual bool		OnAccept(aria::CConnection* connection);
	virtual bool		OnRecv(aria::CConnection* connection, DWORD size, char* recvedMsg);
	virtual bool		OnRecvImmediately(aria::CConnection* connection, DWORD size, char* recvedMsg);
	virtual void		OnClose(aria::CConnection* connection);
	virtual bool		OnSystemMsg(aria::CConnection* connection, DWORD msgType, LPARAM lParam);

public:
	union FuncProcess
	{
		void(*funcProcessPacket)(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data);

		FuncProcess()
		{
			funcProcessPacket = NULL;
		}
	};
	void				SetProcessFunc();

protected:
	bool				ConnectLoginServer(std::string ip, int port);

	std::string			_serverName;

	//로그인서버 전용 접속 소켓
	SOCKET				_loginServerSocket;
	aria::CConnection*	_loginServerConnection;
	FuncProcess			_funcProcess[100];
	std::string			_logFileName;
};

