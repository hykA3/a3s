#include "stdafx.h"
#include "GameUser.h"


CGameUser::CGameUser()
{
// 	_characterInfo->_characterType = 0;
// 	_characterInfo->_index = 0;
// 	_characterInfo->_nickName = nullptr;
// 	_characterInfo->_state = 0;
// 	
// 	_characterInfo->_x = 0;
// 	_characterInfo->_y = 0;
// 	_characterInfo->_z = 0;
// 
// 	_characterInfo->_roll = 0;
// 	_characterInfo->_pitch = 0;
// 	_characterInfo->_yaw = 0;
}

CGameUser::~CGameUser()
{
	SAFE_DELETE_ARRAY(_characterInfo);
}

const CharacterInfo* CGameUser::GetCharacterInformation()
{
	return _characterInfo;
}

// bool CGameUser::CloseConnection(bool init /*= true*/, bool force /*= false*/)
// {
// 	return true;
// }

void CGameUser::SetCharacterInformation(CharacterInfo* characterInfo)
{
	_characterInfo = characterInfo;
}
