#pragma once

#include "../GritLibrary/Rapidjson/document.h"
#include "../GritLibrary/NetworkRequestIntoJson.h"

class CGameProcessPacket
{
public:
	CGameProcessPacket();
	virtual ~CGameProcessPacket();

	static void NewUser(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data);
	static void UpdateUser(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data);
};

