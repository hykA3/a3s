#pragma once

#include "../GritLibrary/Connection.h"
#include "../GritLibrary/NetworkRequestIntoJson.h"
#include "../GritLibrary/Queue.h"

#include "GamePacket.h"

class CGameUser : public aria::CConnection
{
public:
	CGameUser();
	virtual ~CGameUser();

	const CharacterInfo*	GetCharacterInformation();
//	virtual bool			CloseConnection(bool init = true, bool force = false);

	void					SetCharacterInformation(CharacterInfo* characterInfo);

protected:
	CharacterInfo*			_characterInfo;
	bool					_needSaveCharacterInfo;

	DEFINE_GET_SET_ACCESS_WITH_VAR(int, ServerIndex, _serverIndex);
};

