#include "stdafx.h"
#include "GameUserManager.h"

#include "../GritLibrary/Queue.h"
#include "../GritLibrary/NetworkRequestIntoJson.h"

unsigned int WINAPI CallBroadCastThread(void* object)
{
	CGameUserManager* gameUserManager = (CGameUserManager*)object;
	gameUserManager->BroadCastCharacter();
	return 1;
}

CGameUserManager::CGameUserManager()
{
	if (!CreateBroadCastThread())
	{
		return;
	}

	_users = NULL;
}

CGameUserManager::~CGameUserManager()
{
	_broadCastTheadRun = false;

	if (_broadThreadHandle)
	{
		_broadThreadHandle = NULL;
		delete _broadThreadHandle;
	}
	
	SAFE_DELETE_ARRAY(_users);
}

bool CGameUserManager::CreateUser(InitConfig& config, int count)
{
	_users = new CGameUser[count];

	for (int i = 0; i < count; ++i)
	{
		config._index = i;

		if (_users[i].CreateConnection(config) == false)
		{
			return false;
		}

	}
	return true;
}

bool CGameUserManager::AddUser(CGameUser* user, CharacterInfo* userInfo)
{
	_userList.Lock();

	auto itr = _userList.GetMap().find(userInfo->_index);

	if (itr != _userList.GetMap().end())
	{
		_userList.Unlock();
		return false;
	}

	user->SetServerIndex(GLOBAL_IOCP_SERVER->GeneratePrivateKey());
	_convertorIndex.GetMap().insert(std::make_pair(user->GetServerIndex(), userInfo->_index));

	_userList.GetMap().insert(std::make_pair(userInfo->_index, user));
	user->SetCharacterInformation(userInfo);

	_userList.Unlock();
	return true;
}

bool CGameUserManager::DeleteUser(CGameUser* user)
{
	_userList.Lock();

	if (_convertorIndex.GetMap().begin() == _convertorIndex.GetMap().end())
	{
		_userList.Unlock();
		return false;
	}
	auto convertor = _convertorIndex.GetMap().find(user->GetServerIndex());
	if (convertor->second < 0)
	{
		return false;
	}

	int userIndex = convertor->second;

	_convertorIndex.GetMap().erase(user->GetServerIndex());
	_userList.GetMap().erase(userIndex);

	_userList.Unlock();

	return true;
}

bool CGameUserManager::UpdateUser(CharacterInfo* characterInfo)
{
	_userList.Lock();

	//user check
	auto itr = _userList.GetMap().find(characterInfo->_index);
	if (itr == _userList.GetMap().end())
	{
		_userList.Unlock();
		return false;
	}	

	itr->second->SetCharacterInformation(characterInfo);
	_userList.Unlock();

	return true;
}

void CGameUserManager::BroadCastCharacter(/*aria::CNetworkResponseIntoJson* response, int index*/)
{
	while (_broadCastTheadRun)
	{
		_userList.Lock();

		// summary
		// user가 없거나 1명 이하일 경우 BroadCast를 하지 않는다.
		if (_userList.GetMap().begin() == _userList.GetMap().end() || 1 >= _userList.GetMap().size())
		{
			_userList.Unlock();
			continue;
		}

		aria::CJsonObject* resultObject = new aria::CJsonObject();
// 		aria::CNetworkResponseIntoJson json(response->GetConnection());
// 		json.SetRequestType(ChararterBroadCast);

		// summary
		// 유저 모두의 정보로 Json Array 생성한다
		auto itr = _userList.GetMap().begin();
		while (itr != _userList.GetMap().end())
		{
			aria::CJsonObject* jsonObject = new aria::CJsonObject();
			// 		if (itr->first == index)
			// 		{
			// 			itr++;
			// 			continue;
			// 		}

			jsonObject->AddParam("Index", itr->first);
			jsonObject->AddParam("Type", itr->second->GetCharacterInformation()->_characterType);
			jsonObject->AddParam("State", itr->second->GetCharacterInformation()->_state);

			jsonObject->AddParam("X", itr->second->GetCharacterInformation()->_x);
			jsonObject->AddParam("Y", itr->second->GetCharacterInformation()->_y);
			jsonObject->AddParam("Z", itr->second->GetCharacterInformation()->_z);

			jsonObject->AddParam("Roll", itr->second->GetCharacterInformation()->_roll);
			jsonObject->AddParam("Pitch", itr->second->GetCharacterInformation()->_pitch);
			jsonObject->AddParam("Yaw", itr->second->GetCharacterInformation()->_yaw);

			resultObject->AddParam("UserInfo", jsonObject, true);

			itr++;
		}
		
		// summary
		// 유저 각자에게 Json Array를 BroadCast 한다.
		auto userItr = _userList.GetMap().begin();
		while (userItr != _userList.GetMap().end())
		{
			aria::CNetworkResponseIntoJson* json = new aria::CNetworkResponseIntoJson(userItr->second);
			//유저정보 확인
			if (userItr->second->GetCharacterInformation()->_index < 0)
			{
				userItr++;
				continue;
			}
			json->SetRequestType(ChararterBroadCast);
			json->AddParam("result", resultObject, false);

			json->SendSuccessPacket();
			free(json);

			userItr++;		
		}

		_userList.Unlock();

		Sleep(15);
	}
}

bool CGameUserManager::CreateBroadCastThread()
{
	HANDLE	threadHandle = INVALID_HANDLE_VALUE;
	UINT	threadId = ChararterBroadCast;

	threadHandle = (HANDLE)_beginthreadex(NULL, 0, &CallBroadCastThread,
		this, CREATE_SUSPENDED, &threadId);

	if (threadHandle == NULL)
	{
		GLOBAL_LOG_HELPER->Error(LOG_INFO_TYPE_ERROR_CRITICAL, "BroadCastThread 생성 실패");
		return false;
	}

	_broadThreadHandle = threadHandle;
	ResumeThread(threadHandle);

	_broadCastTheadRun = true;

	return true;
}
