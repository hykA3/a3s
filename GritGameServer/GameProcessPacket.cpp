#include "stdafx.h"
#include "GameProcessPacket.h"
#include "GamePacket.h"
#include "GameUserManager.h"
#include "GameUser.h"



CGameProcessPacket::CGameProcessPacket()
{
}


CGameProcessPacket::~CGameProcessPacket()
{
}

void CGameProcessPacket::NewUser(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data)
{
	if (response == NULL || data == NULL)
	{
		response->SendErrorPacket("Packet Error");
		return;
	}

	CharacterInfo	newCharacter;
	newCharacter._characterType = PARSE_INT(*data, "Type");
	newCharacter._index = PARSE_INT(*data, "Index");

	newCharacter._x = PARSE_DOUBLE(*data, "X");
	newCharacter._y = PARSE_DOUBLE(*data, "Y");
	newCharacter._z = PARSE_DOUBLE(*data, "Z");

	//new character create
	//((CGameUser*)response->GetConnection())->SetCharacterInformation(&newCharacter);
	if (GLOBAL_GAME_USERMANAGER->AddUser((CGameUser*)response->GetConnection(), &newCharacter))
	{
		response->SendSuccessPacket();
	}
	else
	{
		response->SendErrorPacket("Invalid User");
	}
}

void CGameProcessPacket::UpdateUser(aria::CNetworkResponseIntoJson* response, rapidjson::Value* data)
{
	if (response == NULL || data == NULL)
	{
		response->SendErrorPacket("Packet Error");
		return;
	}

	//character info parse
	CharacterInfo	*characterInfo = new CharacterInfo();
	
	characterInfo->_characterType = PARSE_INT(*data, "Type");
	characterInfo->_index = PARSE_INT(*data, "Index");
	characterInfo->_state = PARSE_INT(*data, "State");
				 
	characterInfo->_x = PARSE_DOUBLE(*data, "X");
	characterInfo->_y = PARSE_DOUBLE(*data, "Y");
	characterInfo->_z = PARSE_DOUBLE(*data, "Z");
				 
	characterInfo->_roll = PARSE_DOUBLE(*data, "Roll");
	characterInfo->_pitch = PARSE_DOUBLE(*data, "Pitch");
	characterInfo->_yaw = PARSE_DOUBLE(*data, "Yaw");

	GLOBAL_GAME_USERMANAGER->UpdateUser(characterInfo);
//	GLOBAL_GAME_USERMANAGER->BroadCastCharacter(response, characterInfo->_index);

	response->SendSuccessPacket();
}
